{
	"basePath": "/v1",
	"consumes": [
		"application/json"
	],
	"definitions": {
		"User": {
			"description": "User objects allow you to associate actions performed in the system with the user that performed them.\nThe User object contains common information across every user in the system regardless of status and role.",
			"properties": {
				"id": {
					"type": "number",
					"format": "double",
					"description": "User id"
				},
				"token": {
					"type": "string",
					"description": "Authorization token"
				},
				"email": {
					"type": "string"
				},
				"username": {
					"type": "string"
				},
				"password": {
					"type": "string"
				},
				"resources": {
					"items": {
						"additionalProperties": true
					},
					"type": "array"
				},
				"roles": {
					"items": {
						"type": "string"
					},
					"type": "array"
				}
			},
			"type": "object",
			"additionalProperties": false,
			"example": {
				"id": 1,
				"token": "52907745-7672-470e-a803-a2f8feb52944"
			}
		},
		"Credentials": {
			"properties": {
				"username": {
					"type": "string"
				},
				"password": {
					"type": "string"
				}
			},
			"required": [
				"username",
				"password"
			],
			"type": "object",
			"additionalProperties": false
		},
		"Registration": {
			"description": "Value object for register user",
			"properties": {
				"username": {
					"type": "string",
					"description": "Username"
				},
				"email": {
					"type": "string",
					"description": "User primary email"
				},
				"password": {
					"type": "string",
					"description": "Password"
				},
				"passwordAgain": {
					"type": "string",
					"description": "Password again."
				}
			},
			"required": [
				"username",
				"email",
				"password",
				"passwordAgain"
			],
			"type": "object",
			"additionalProperties": false
		},
		"AccountModel": {
			"properties": {
				"username": {
					"type": "string"
				},
				"email": {
					"type": "string"
				}
			},
			"required": [
				"username",
				"email"
			],
			"type": "object",
			"additionalProperties": false
		},
		"ErrorResponseModel": {
			"properties": {
				"message": {
					"type": "string",
					"enum": [
						"Unkown error"
					],
					"x-nullable": false
				}
			},
			"required": [
				"message"
			],
			"type": "object",
			"additionalProperties": false
		},
		"ValidateErrorJSON": {
			"properties": {
				"message": {
					"type": "string",
					"enum": [
						"Validation failed"
					],
					"x-nullable": false
				},
				"details": {
					"properties": {},
					"additionalProperties": {
						"additionalProperties": true
					},
					"type": "object"
				}
			},
			"required": [
				"message",
				"details"
			],
			"type": "object",
			"additionalProperties": false
		}
	},
	"info": {
		"title": "@lordoftheflies/paspartu-application",
		"version": "0.0.2",
		"description": "Paspartu all-in-one application layer",
		"license": {
			"name": "MIT"
		},
		"contact": {}
	},
	"paths": {
		"/account/login": {
			"post": {
				"operationId": "login",
				"produces": [
					"application/json"
				],
				"responses": {
					"200": {
						"description": "User logged in",
						"schema": {
							"$ref": "#/definitions/User"
						}
					}
				},
				"consumes": [
					"application/json"
				],
				"description": "Login user",
				"summary": "Login",
				"tags": [
					"UAAC"
				],
				"security": [],
				"parameters": [
					{
						"description": "Id of user",
						"in": "body",
						"name": "credentials",
						"required": true,
						"schema": {
							"$ref": "#/definitions/Credentials"
						}
					}
				]
			}
		},
		"/account/register": {
			"post": {
				"operationId": "register",
				"produces": [
					"application/json"
				],
				"responses": {
					"200": {
						"description": "User registered",
						"schema": {
							"$ref": "#/definitions/User"
						}
					}
				},
				"consumes": [
					"application/json"
				],
				"description": "Register user",
				"summary": "Register",
				"tags": [
					"UAAC"
				],
				"security": [],
				"parameters": [
					{
						"description": "Id of user",
						"in": "body",
						"name": "registration",
						"required": true,
						"schema": {
							"$ref": "#/definitions/Registration"
						}
					}
				]
			}
		},
		"/account/info": {
			"get": {
				"operationId": "userInfo",
				"produces": [
					"application/json"
				],
				"responses": {
					"200": {
						"description": "sf",
						"schema": {
							"$ref": "#/definitions/AccountModel"
						}
					},
					"422": {
						"description": "Validation failed",
						"schema": {
							"$ref": "#/definitions/ValidateErrorJSON"
						}
					},
					"500": {
						"description": "Unexpected error",
						"schema": {
							"$ref": "#/definitions/ErrorResponseModel"
						}
					}
				},
				"description": "Get user info",
				"tags": [
					"UAAC"
				],
				"security": [
					{
						"jwt": [
							"account:profile-read"
						]
					}
				],
				"parameters": []
			}
		}
	},
	"produces": [
		"application/json"
	],
	"swagger": "2.0",
	"securityDefinitions": {
		"api_key": {
			"type": "apiKey",
			"name": "access_token",
			"in": "query"
		},
		"jwt": {
			"type": "apiKey",
			"name": "access_token",
			"in": "query"
		},
		"tsoa_auth": {
			"type": "oauth2",
			"authorizationUrl": "http://swagger.io/api/oauth/dialog",
			"flow": "implicit",
			"scopes": {
				"write:pets": "modify things",
				"read:pets": "read things"
			}
		}
	},
	"host": "localhost:3000",
	"tags": [
		{
			"name": "UAAC",
			"description": "Paspartu UAAC module API",
			"externalDocs": {
				"description": "Find out more about User Access and Authentication Control",
				"url": "http://swagger.io"
			}
		}
	]
}