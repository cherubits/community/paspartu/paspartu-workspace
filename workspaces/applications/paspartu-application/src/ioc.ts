import "reflect-metadata";
import { Container, decorate, injectable } from "inversify";
import $logger from './logger';
import { Controller } from 'tsoa';
import { buildProviderModule } from "inversify-binding-decorators";
import './loader';
import {
    AuthenticationModule,
    PersistenceContainerModule,
} from "@lordoftheflies/paspartu-infrastructure/src";



const iocContainer = new Container({
    autoBindInjectable: true,
    skipBaseClassChecks: true,
});

(async () => {

    $logger.info('loading container modules', { component: 'container' });

    await iocContainer.loadAsync(
        new PersistenceContainerModule(),
        new AuthenticationModule(),
    );

    decorate(injectable(), Controller); // Makes tsoa's Controller injectable
    iocContainer.load(buildProviderModule());
    $logger.warn('initialize controllers', { component: 'container' });

    $logger.info('container modules initialized', { component: 'container' });

    return iocContainer;
})();

// export according to convention
export { iocContainer };
