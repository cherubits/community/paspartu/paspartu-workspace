
import * as express from "express";
import { inject } from "inversify";
import {
    Body,
    Controller,
    Get,
    Path,
    Post,
    Query,
    Route,
    SuccessResponse,
    OperationId,
    Response,
    Request,
    Tags,
    Security,
} from "tsoa";
import { provideSingleton } from "@lordoftheflies/paspartu-core/src/decorators";
import * as jwt from "jsonwebtoken";
import { httpPost } from "inversify-express-utils";
import {
    ErrorResponseModel,
    ValidateErrorJSON
} from "@lordoftheflies/paspartu-core/src/errors";
import {
    AccountModel,
    Credentials,
    Registration,
    User,
} from '@lordoftheflies/paspartu-domain/src/models';

@Route("account")
@Tags("UAAC")
@provideSingleton(AccountController)
export class AccountController extends Controller {

    // @inject($services.UserService)
    private userService!: any;

    /**
     * Login user
     * @summary Login
     * @param credentials Id of user
     * @param request Modified user
     * @returns
     */
    @SuccessResponse("200", "User logged in")
    @Post("login")
    @OperationId("login")
    public async login(
        @Body() credentials: Credentials,
        @Request() request: any,
        // @Response() response: any
    ): Promise<User> {
        const user = await this.userService.findOneByCredentials(credentials.username, credentials.password);
        const token = jwt.sign({
            id: 1,
            username: user.username,
            email: user.email,
            scopes: ["user:create", "account:profile-read"]
        }, 'secret');
        return { id: user.id, resources: ["account:profile-read"], roles: ["user.create"], token: token };
    }

    /**
     * Register user
     * @summary Register
     * @param registration Id of user
     * @returns
     */
    @SuccessResponse("200", "User registered")
    @Post("register")
    @OperationId("register")
    public async register(
        @Body() registration: Registration,
        // @Request() request: any,
        // @Response() response: any
    ): Promise<User> {
        if (!registration.password || !registration.passwordAgain || (registration.password !== registration.passwordAgain)) {
            throw new Error('invalid');
        } else {
            const password = registration.password;
            const user = await this.userService.create({
                email: registration.email,
                password: password,
                username: registration.username,
            });
            const token = jwt.sign({
                id: user.id,
                username: user.username,
                email: user.email,
                scopes: [
                    "user:create",
                    "account:profile-read"
                ]
            }, user.password);
            this.setHeader('x-access-token', `${token}`);
            this.setHeader('Set-Cookie', `x-access-token=${token}`);
            return {
                id: user.id,
                resources: ["account:profile-read"],
                roles: ["user.create"],
                token: token
            };

        }
    }

    /**
     * Get user info
     * @param request fre
     * @returns sf
     */
    @Response<ErrorResponseModel>(500, "Unexpected error")
    @Response<ValidateErrorJSON>(422, "Validation failed")
    @Security("jwt", ["account:profile-read"])
    @Get("info")
    @OperationId("userInfo")
    public async userInfo(@Request() request: any): Promise<AccountModel> {
        await this.userService.findOne(request.user.id);
        return Promise.resolve(request.user);
    }
}
