import "reflect-metadata";
import { Container } from "inversify";
import $logger from './logger';
import express, { Application, Router, Request, Response } from 'express';
import { AuthProvider, InversifyExpressServer } from "inversify-express-utils";
import { iocContainer as $container, iocContainer } from "./ioc";
import morgan from 'morgan';
import { AuthenticationProvider, $providers } from "@lordoftheflies/paspartu-infrastructure/src/authentication";
import swaggerUi from "swagger-ui-express";
import $app from './app';
// import { RegisterRoutes } from "./routes";
import { ValidateError } from "tsoa";


(async () => {

    const port = 3000;

    $logger.debug('initializing server', { component: 'server' });

    const router = Router({
        caseSensitive: false,
        mergeParams: false,
        strict: false
    });

    const server = new InversifyExpressServer(
        $container,
        router,
        // null,
        { rootPath: "/v1" },
        $app,
        AuthenticationProvider,
        false);
    server
        .setConfig(async (app: Application) => {
            // add body parser
            app.use(express.urlencoded({ extended: true }));
            app.use(express.json());
            app.use(express.text());
            app.use(express.json({ type: 'application/json' }));
            app.use(morgan('combined'));
            //app.use("/api-docs", swaggerUi.serve, async (_req: Request, res: Response) => {
            //    return res.send(swaggerUi.generateHTML(await import("../src/swagger.json")));
            //});
            //const RegisterRoutes = (await import('./routes')).default;
            //RegisterRoutes(app);
        })
        .setErrorConfig((app) => {
            app.use((err: Error, req: Request, res: Response, next: any) => {
                if (err instanceof ValidateError) {
                    $logger.warn(`Caught Validation Error for ${req.path}:`, err.fields);
                    return res.status(err.status).json({
                        message: "Validation Failed",
                        details: err?.fields,
                    });
                }
                if (err instanceof Error) {
                    return res.status(500).json({
                        message: "Internal Server Error",
                    });
                }

                next();
            });
        })
        .build()
        .listen(port, () => {
            $logger.info(`server running at http://127.0.0.1:${port}/`);
        });

})();
