

// import { SizingConstant } from './';
import Login from './Login';
import { LoginProps } from './Login.props';

export default {
  title: 'Views/Login',
  component: Login,
  argTypes: {
    color: {
      name: 'color',
      description: 'Foreground color',
      type: { name: 'string', required: false },
      defaultValue: 'red',
      control: {
        type: 'color'
      }
    },
    // size: {
    //   control: {
    //     // options: [SizingConstant.LARGE, SizingConstant.SMALL],
    //     type: 'enum'
    //   }
    // }
  },
};

export const Regular = (args: LoginProps) => <Login {...args}></Login>;

