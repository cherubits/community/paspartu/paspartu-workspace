import { WithStyles } from "@material-ui/core/styles";
import useStyles from "./Login.style";

// import { SizingConstant } from "../../core/SizingConstant";
// import { SemanticColor } from "../../core/SemanticColor";
// import { CardHeaderColor } from "../../core/CardHeaderColor";
export interface LoginProps extends WithStyles<typeof useStyles> {
}
