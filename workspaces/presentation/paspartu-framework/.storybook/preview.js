import { DocsContainer, DocsPage } from '@storybook/addon-docs';
import { themes, ThemeProvider } from '@storybook/theming';
import { addons } from '@storybook/addons';
import React from 'react';
import { MemoryRouter } from "react-router";


export const INITIAL_VIEWPORTS = {
  iphone5: {
    name: 'iPhone 5',
    styles: {
      height: '568px',
      width: '320px',
    },
    type: 'mobile',
  },
  iphone6: {
    name: 'iPhone 6',
    styles: {
      height: '667px',
      width: '375px',
    },
    type: 'mobile',
  },
  iphone6p: {
    name: 'iPhone 6 Plus',
    styles: {
      height: '736px',
      width: '414px',
    },
    type: 'mobile',
  },
  iphone8p: {
    name: 'iPhone 8 Plus',
    styles: {
      height: '736px',
      width: '414px',
    },
    type: 'mobile',
  },
  iphonex: {
    name: 'iPhone X',
    styles: {
      height: '812px',
      width: '375px',
    },
    type: 'mobile',
  },
  iphonexr: {
    name: 'iPhone XR',
    styles: {
      height: '896px',
      width: '414px',
    },
    type: 'mobile',
  },
  iphonexsmax: {
    name: 'iPhone XS Max',
    styles: {
      height: '896px',
      width: '414px',
    },
    type: 'mobile',
  },
  ipad: {
    name: 'iPad',
    styles: {
      height: '1024px',
      width: '768px',
    },
    type: 'tablet',
  },
  ipad10p: {
    name: 'iPad Pro 10.5-in',
    styles: {
      height: '1112px',
      width: '834px',
    },
    type: 'tablet',
  },
  ipad12p: {
    name: 'iPad Pro 12.9-in',
    styles: {
      height: '1366px',
      width: '1024px',
    },
    type: 'tablet',
  },
  galaxys5: {
    name: 'Galaxy S5',
    styles: {
      height: '640px',
      width: '360px',
    },
    type: 'mobile',
  },
  galaxys9: {
    name: 'Galaxy S9',
    styles: {
      height: '740px',
      width: '360px',
    },
    type: 'mobile',
  },
  nexus5x: {
    name: 'Nexus 5X',
    styles: {
      height: '660px',
      width: '412px',
    },
    type: 'mobile',
  },
  nexus6p: {
    name: 'Nexus 6P',
    styles: {
      height: '732px',
      width: '412px',
    },
    type: 'mobile',
  },
  pixel: {
    name: 'Pixel',
    styles: {
      height: '960px',
      width: '540px',
    },
    type: 'mobile',
  },
  pixelxl: {
    name: 'Pixel XL',
    styles: {
      height: '1280px',
      width: '720px',
    },
    type: 'mobile',
  },
};

export const MINIMAL_VIEWPORTS = {
  mobile1: {
    name: 'Small mobile',
    styles: { height: '568px', width: '320px' },
    type: 'mobile',
  },
  mobile2: {
    name: 'Large mobile',
    styles: { height: '896px', width: '414px' },
    type: 'mobile',
  },
  tablet: {
    name: 'Tablet',
    styles: { height: '1112px', width: '834px' },
    type: 'tablet',
  },
};

export const CUSTOM_VIEWPORTS = {
  kindleFire2: {
    name: 'Kindle Fire 2',
    styles: {
      width: '600px',
      height: '963px',
    },
  },
  kindleFireHD: {
    name: 'Kindle Fire HD',
    styles: {
      width: '533px',
      height: '801px',
    },
  },
};

// import { ThemeProvider } from '@material-ui/core';

import DarkTheme from './darkTheme';
import LightTheme from './lightTheme';

const ThemeSet = {
  'dark': themes.dark,
  'light': themes.light,
  'normal': themes.normal,
}

// // Function to obtain the intended theme
const getTheme = (themeName) => {
  return ThemeSet[themeName]
}


export const parameters = {
  a11y: {
    config: {},
    options: {
      checks: { 'color-contrast': { options: { noScroll: true } } },
      restoreScroll: true,
    },
  },
  actions: { argTypesRegex: '^on[A-Z].*' },
  argTypes: {
    width: {
      control: {
        type: 'range',
        min: 400,
        max: 1200,
        step: 50,
      },
    },
    label: {
      table: {
        disable: true,
      },
    },
  },
  docs: {
    container: DocsContainer,
    page: DocsPage,
    inlineStories: false,
    iframeHeight: '300px',
    theme: themes.normal,
  },
  layout: 'centered',
  viewport: {
    defaultViewport: 'default',
    viewports: {
      ...CUSTOM_VIEWPORTS,
      ...MINIMAL_VIEWPORTS,
      ...INITIAL_VIEWPORTS,
    },
  },
  controls: {
    hideNoControlsWarning: true,
    expanded: true,
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    }
  },
  backgrounds: {
    default: 'normal',
    values: [
      {
        name: 'light',
        value: 'yellow',
      },
      {
        name: 'dark',
        value: 'darkblue',
      },
      {
        name: 'normal',
        value: 'transparent',
      },
    ],
  },
};

export const globalTypes = {
  theme: {
    name: 'Theme',
    description: 'Global theme for components',
    defaultValue: 'light',
    toolbar: {
      icon: 'circlehollow',
      // array of plain string values or MenuItem shape (see below)
      items: [
        'light',
        'dark',
        'normal',
      ],
    },
  },
  routes: {
    name: 'Routes',
    description: 'Global routing for components',
    defaultValue: ['/'],
    toolbar: {
      icon: 'globe',
      items: [
        ['/'],
        ['/', 'home'],
      ]
    }
  },
  locale: {
    name: 'Locale',
    description: 'Internationalization locale',
    defaultValue: 'en',
    toolbar: {
      icon: 'globe',
      items: [
        { value: 'en', right: '🇺🇸', title: 'English' },
        { value: 'fr', right: '🇫🇷', title: 'Français' },
        { value: 'es', right: '🇪🇸', title: 'Español' },
        { value: 'zh', right: '🇨🇳', title: '中文' },
        { value: 'kr', right: '🇰🇷', title: '한국어' },
      ],
    },
  },
};
//
// const withThemeProvider = (Story, context) => {
//   const theme = getTheme(context.globals.theme);
//   return html`
//     <ThemeProvider theme="${theme}">
//         <Story {...context} />
//     </ThemeProvider>
//   `;
// }
//


const withThemeProvider=(Story,context)=>{
  const theme = getTheme(context.globals.theme);
  return (
    <ThemeProvider theme={theme}>
      <Story {...context} />
    </ThemeProvider>
  )
}

const withRoutingProvider=(Story,context)=>{
  return (
    <MemoryRouter initialEntries={context.globals.routes}>
      <Story {...context} />
    </MemoryRouter>
  )
}

const getCaptionForLocale = (locale) => {
  switch (locale) {
    case 'es': return 'Hola!';
    case 'fr': return 'Bonjour!';
    case 'kr': return '안녕하세요!';
    case 'zh': return '你好!';
    default:
      return 'Hello!'
  }
}

// export const storyWithLocale = (args, { globals: { locale } }) => {
//   const caption = getCaptionForLocale(locale);
//   return html`${caption}`;
// };

// export const decorators = [

// ];

// // export const decorators = [storyWithLocale];

export const decorators = [
  withThemeProvider,
  withRoutingProvider,
];
