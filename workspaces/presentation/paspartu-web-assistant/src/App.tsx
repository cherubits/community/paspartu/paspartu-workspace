import React from 'react';
import { Routes, Route, Navigate, useLocation } from "react-router-dom";
// import './App.css';
import Home from './pages/Home';
import About from './pages/About';


import { ThemeProvider } from '@material-ui/core';
import GlobalStyles from './GlobalStyles';
import theme from './theme/LightTheme';
import Privacy from './pages/Privacy';
import AdminLayout from './layouts/AdminLayout/AdminLayout';
import { AuthContextType } from './core/authentication/AuthContextType';
import { AuthenticationProvider } from './core/authentication/AuthenticationProvider';
import Login from './views/Login/Login';
import RtlLayout from './layouts/RtlLayout/RtlLayout';




let AuthContext = React.createContext<AuthContextType>(null!);


export function AuthProvider({ children }: { children: React.ReactNode }) {
  let [user, setUser] = React.useState<any>(null);

  let signin = (newUser: string, callback: VoidFunction) => {
    return new AuthenticationProvider().signin(() => {
      setUser(newUser);
      callback();
    });
  };

  let signout = (callback: VoidFunction) => {
    return new AuthenticationProvider().signout(() => {
      setUser(null);
      callback();
    });
  };

  let value = { user, signin, signout };

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}

export function useAuth() {
  return React.useContext(AuthContext);
}

export function RequireAuth({ children }: { children: JSX.Element }) {
  let auth = useAuth();
  let location = useLocation();

  if (!auth.user) {
    // Redirect them to the /login page, but save the current location they were
    // trying to go to when they were redirected. This allows us to send them
    // along to that page after they login, which is a nicer user experience
    // than dropping them off on the home page.
    return <Navigate to="/login" state={{ from: location }} />;
  }

  return children;
}


export interface AppProps {
  // extends RouteComponentProps<any>
}
export interface AppState {

}



export class App extends React.Component<AppProps, AppState> {

  render(): React.ReactNode {
    return (

        <AuthProvider>
      <div className="App">
          {/* <AppContext.Provider value={{
          authenticated: false,
          lang: 'en',
          theme: 'light',
          rtl: false,
        }}> */}
          <Routes>
            <Route path="/login" element={<Login />} />
            {/* <Route path="/home" element={<SplashLayout />} /> */}
            <Route
              path="/admin"
              element={
                <RequireAuth>
                  <AdminLayout />
                </RequireAuth>
              }
            />
            <Route
              path="/rtl"
              element={
                <RequireAuth>
                  <RtlLayout />
                </RequireAuth>
              }
            />



            {/* <AppContext.Consumer>{
              ({ authenticated }) => {
              //   if (!authenticated) {
              //     return <Navigate to={"login"} />
              //   } else return <Navigate to={'admin'} />
                return <span>mi</span>;
              }
            }</AppContext.Consumer> */}
          </Routes>
          {/* {routing} */}
          {/* </AppContext.Provider>C */}
asSS
      </div>
        </AuthProvider>
    );
  }
}

export default App;
