

export enum CardHeaderColor {
  WarningCardHeader = "warningCardHeader",
  SuccessCardHeader = "successCardHeader",
  DangerCardHeader = "dangerCardHeader",
  InfoCardHeader = "infoCardHeader",
  PrimaryCardHeader = "primaryCardHeader",
  RoseCardHeader = "roseCardHeader"
}
