
export enum BackgroundColor {
  Purple = "purple",
  Blue = "blue",
  Green = "green",
  Orange = "orange",
  Red = "red"
}
