
/**
 * This represents some generic auth provider API, like Firebase.
 */
export class AuthenticationProvider {

    private isAuthenticated = false;

    signin(callback: VoidFunction) {
        this.isAuthenticated = true;
        setTimeout(callback, 100); // fake async
    }

    signout(callback: VoidFunction) {
        this.isAuthenticated = false;
        setTimeout(callback, 100);
    }
};
