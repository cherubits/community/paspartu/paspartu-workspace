
export enum SemanticColor {
  Info = "info",
  Success = "success",
  Warning = "warning",
  Danger = "danger",
  Primary = "primary",
  Transparent = "transparent",
  White = "white"
}
