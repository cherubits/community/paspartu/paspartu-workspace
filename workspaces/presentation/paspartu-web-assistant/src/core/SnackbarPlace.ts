

export enum SnackbarPlace {
  TL = 'tl',
  TR = 'tr',
  TC = 'tc',
  BR = 'br',
  BL = 'bl',
  BC = 'bc'
}
