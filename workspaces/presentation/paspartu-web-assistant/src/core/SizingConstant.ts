
export enum SizingConstant {
  LARGE = 'lg',
  SMALL = 'sm'
}
