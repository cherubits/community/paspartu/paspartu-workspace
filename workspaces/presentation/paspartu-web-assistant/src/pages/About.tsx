import React from 'react';
import { Routes, Route, Link } from "react-router-dom";
// import { SidebarProps } from './SidebarProps';

/**
 * Application about page
 */
export class About extends React.Component {

  render() {
    return (
      <>
        <main>
          <h2>Who are we?</h2>
          <p>
            That feels like an existential question, don't you
            think?
          </p>
        </main>
        <nav>
          <Link to="/">Home</Link>
        </nav>
      </>
    );
  }
}

export default About;
