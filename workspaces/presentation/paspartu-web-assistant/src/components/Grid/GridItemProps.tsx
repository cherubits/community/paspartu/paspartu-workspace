import { GridSize } from "@material-ui/core";
import { WithStyles } from "@material-ui/core/styles";
import useStyles from "./GridItemStyle";

type CustomGridSize = boolean | GridSize | undefined;

export interface GridItemProps extends WithStyles<typeof useStyles> {
  xs?: CustomGridSize;
  sm?: CustomGridSize;
  md?: CustomGridSize;
  lg?: CustomGridSize;
  style?: any;
  children?: any[]|any;
}
