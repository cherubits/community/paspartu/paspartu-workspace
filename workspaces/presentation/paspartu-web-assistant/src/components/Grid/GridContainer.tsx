import React from "react";
// nodejs library to set properties for components
// @material-ui/core components
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import { GridContainerProps } from "./GridContainerProps";
import useStyles from './GridContainerStyle';

class GridContainer extends React.Component<GridContainerProps> {

  render() {
    const {
      classes,
      children, 
      ...rest
    } = this.props;

    return (
      <Grid container {...rest} className={classes.grid}>
        {children}
      </Grid>
    );
  }
}


export default withStyles(useStyles, {withTheme: true})(GridContainer);