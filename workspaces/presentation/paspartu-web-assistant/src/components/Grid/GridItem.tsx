import React from "react";
// nodejs library to set properties for components
// @material-ui/core components
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import { GridItemProps } from "./GridItemProps";
import useStyles from './GridItemStyle';

class GridItem extends React.Component<GridItemProps> {

  render() {
    const {
      classes,
      children, 
      ...rest
    } = this.props;

    return (
      <Grid item {...rest} className={classes.grid}>
        {children}
      </Grid>
    );
  }
}


export default withStyles(useStyles, {withTheme: true})(GridItem);