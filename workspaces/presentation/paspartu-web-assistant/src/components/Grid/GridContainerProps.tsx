import { GridJustification } from "@material-ui/core";
import { WithStyles } from "@material-ui/core/styles";
import useStyles from "./GridContainerStyle";

export interface GridContainerProps extends WithStyles<typeof useStyles> {
  children?: any[]|any;
  justify?: GridJustification;
}
