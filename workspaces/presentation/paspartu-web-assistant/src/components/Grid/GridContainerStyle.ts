
import { createStyles, Theme } from "@material-ui/core/styles";

const gridContainerStyles = (theme: Theme) => createStyles({
    grid: {
        padding: "0 15px !important",
    },
});

export default gridContainerStyles;