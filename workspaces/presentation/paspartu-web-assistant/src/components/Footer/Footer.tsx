/*eslint-disable*/
import React from "react";
// @material-ui/core components
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
// core components
import useStyles from "./Footer.style";
import { withStyles } from "@material-ui/core/styles";
import { FooterProps } from "./Footer.props";


/**
 * Application footer component
 */
class Footer extends React.Component<FooterProps> {
  constructor(props: FooterProps) {
    super(props);
  }

  render() {
    const {
      classes,
      author,
    } = this.props;

    return (
      <footer className={classes.footer}>
        <div className={classes.container}>
          <div className={classes.left}>
            <List className={classes.list}>
              <ListItem className={classes.inlineBlock}>
                <a href="#company" className={classes.block}>
                  Company
                </a>
              </ListItem>
              <ListItem className={classes.inlineBlock}>
                <a href="#portfolio" className={classes.block}>
                  Portfolio
                </a>
              </ListItem>
              <ListItem className={classes.inlineBlock}>
                <a href="#blog" className={classes.block}>
                  Blog
                </a>
              </ListItem>
            </List>
          </div>
          <p className={classes.right}>
            <span>
              &copy; {new Date().getFullYear()}{" "}
              <a
                href={author?.authorSiteLink}
                target="_blank"
                className={classes.a}
              >
                {this.props.author?.authorName}
              </a>
              {this.props.author?.motto}
            </span>
          </p>
        </div>
      </footer>
    );
  }
}
export default withStyles(useStyles, { withTheme: true })(Footer);
