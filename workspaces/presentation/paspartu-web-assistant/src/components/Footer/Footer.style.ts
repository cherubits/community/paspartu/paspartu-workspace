import {
  // defaultFont,
  container,
} from "../../App.style";
import { createStyles, Theme } from "@material-ui/core/styles";

const footerStyle = (theme: Theme) => createStyles({
  block: {
    color: "inherit",
    padding: "15px",
    textTransform: "uppercase",
    borderRadius: "3px",
    textDecoration: "none",
    position: "relative",
    display: "block",
    fontWeight: "bolder",
    fontSize: "12px",
    // FIXME: ...defaultFont,
    fontFamily: "Roboto",
    // fontWeight: "normal",
    lineHeight: "1.5em",
  },
  left: {
    float: "left",
    display: "block",
  },
  right: {
    padding: "15px 0",
    margin: "0",
    fontSize: "14px",
    float: "right",
  },
  footer: {
    bottom: "0",
    borderTop: "1px solid " + theme.palette.grey[500],
    padding: "15px 0",
    // FIXME: ...defaultFont,
    fontFamily: "Roboto",
    fontWeight: "normal",
    lineHeight: "1.5em",
  },
  container,
  a: {
    color: theme.palette.primary.main,
    textDecoration: "none",
    backgroundColor: "transparent",
  },
  list: {
    marginBottom: "0",
    padding: "0",
    marginTop: "0",
  },
  inlineBlock: {
    display: "inline-block",
    padding: "0px",
    width: "auto",
  },
});

export default footerStyle;
