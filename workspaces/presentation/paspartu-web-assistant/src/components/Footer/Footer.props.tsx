import useStyles from "./Footer.style";
import { WithStyles } from "@material-ui/core/styles";
import { PlutoniumMenu as PlutoniumLink } from "../../models/PlutoniumLink";
import { PlutoniumAuthor } from "../../models/PlutoniumAuthor";



/**
 * Application footer component properties
 */
export interface FooterProps extends WithStyles<typeof useStyles> {

  /**
   * Links for sitemap pages links
   */
  sitemap?: Array<PlutoniumLink>;

  /**
   * Author model
   */
  author?: PlutoniumAuthor;

  /**
   * When true inverted contrasts applied
   */
  whiteFont?: boolean;

  /**
   * Child React nodes
   */
  children?: any[];
}
