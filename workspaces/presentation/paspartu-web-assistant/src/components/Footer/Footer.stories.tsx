

import { SizingConstant } from '../../core/SizingConstant';
import Footer from './Footer';
import { FooterProps } from './Footer.props';

export default {
  title: 'Components/Footer',
  component: Footer,
  argTypes: {
    color: {
      name: 'color',
      description: 'Foreground color',
      type: { name: 'string', required: false },
      defaultValue: 'red',
      control: {
        type: 'color'
      }
    },
    // size: {
    //   control: {
    //     // options: [SizingConstant.LARGE, SizingConstant.SMALL],
    //     type: 'enum'
    //   }
    // }
  },
};

export const Primary = (args: FooterProps) => <Footer {...args}></Footer>;

