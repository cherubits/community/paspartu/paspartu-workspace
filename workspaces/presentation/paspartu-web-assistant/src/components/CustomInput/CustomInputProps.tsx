import { ReactNode } from "react";
import useStyles from "./CustomInputStyle";
import { WithStyles } from "@material-ui/core/styles";


export interface CustomInputProps extends WithStyles<typeof useStyles> {
  formControlProps?: any;
  labelText?: ReactNode;
  id?: string;
  labelProps?: Record<string, any>;
  inputProps?: Record<string, any>;
  error?: boolean;
  success?: boolean;
  rtlActive?: boolean;
}
