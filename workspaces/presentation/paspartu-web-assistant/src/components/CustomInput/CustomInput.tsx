import React from "react";
import classNames from "classnames";
// @material-ui/core components
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
// @material-ui/icons
import Clear from "@material-ui/icons/Clear";
import Check from "@material-ui/icons/Check";
// core components
import useStyles from "./CustomInputStyle";
import { withStyles } from "@material-ui/core/styles";
import { CustomInputProps } from "./CustomInputProps";

class CustomInput extends React.Component<CustomInputProps> {
  
  render() {

    // const classes = useStyles();
    const {
      formControlProps,
      labelText,
      id,
      labelProps,
      inputProps,
      error,
      success,
      rtlActive,
      classes
    } = this.props;

    const labelClasses = classNames({
      [" " + classes.labelRootError]: error,
      [" " + classes.labelRootSuccess]: success && !error,
      [" " + classes.labelRTL]: rtlActive,
    });
    const underlineClasses = classNames({
      [classes.underlineError]: error,
      [classes.underlineSuccess]: success && !error,
      [classes.underline]: true,
    });
    const marginTop = classNames({
      [classes.marginTop]: labelText === undefined,
    });
    let newInputProps = {
      maxLength:
        inputProps && inputProps.maxLength ? inputProps.maxLength : undefined,
      minLength:
        inputProps && inputProps.minLength ? inputProps.minLength : undefined,
      step: inputProps && inputProps.step ? inputProps.step : undefined,
    };
    return (
      <FormControl
        {...formControlProps}
        className={formControlProps ? formControlProps.className + " " + classes.formControl : classes.formControl}
      >
        {labelText !== undefined ? (
          <InputLabel
            className={classes.labelRoot + labelClasses}
            htmlFor={id}
            {...labelProps}
          >
            {labelText}
          </InputLabel>
        ) : null}
        <Input
          classes={{
            root: marginTop,
            disabled: classes.disabled,
            underline: underlineClasses,
          }}
          id={id}
          {...inputProps}
          inputProps={newInputProps}
        />
        {error ? (
          <Clear className={classes.feedback + " " + classes.labelRootError} />
        ) : success ? (
          <Check className={classes.feedback + " " + classes.labelRootSuccess} />
        ) : null}
      </FormControl>
    );
  }
}

export default withStyles(useStyles, {withTheme: true})(CustomInput);