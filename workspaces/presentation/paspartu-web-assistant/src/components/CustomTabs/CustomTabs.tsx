import React, { ChangeEvent } from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// nodejs library to set properties for components

// material-ui components
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
// core components
import Card from "../Card/Card";
import CardBody from "../Card/CardBody";
import CardHeader from "../Card/CardHeader";
import { withStyles, WithStyles } from "@material-ui/core/styles";

import useStyles from "./CustomTabsStyle";
import { SemanticColor } from "../../core/SemanticColor";
import { CardHeaderColor } from "../../core/CardHeaderColor";

export interface CustomTabsProps extends WithStyles<typeof useStyles> {
  headerColor?: SemanticColor;
  className?: string;
  title?: string;
  tabs?: Array<any>;
  rtlActive?: boolean;
  plainTabs?: boolean;
  initial?: number;
};

export interface CustomTabsState {
  value: any;
}

class CustomTabs extends React.Component<CustomTabsProps, CustomTabsState> {

  static defaultProps = {
    initial: 0
  };

  constructor(props: CustomTabsProps) {
    super(props);
    this.state = {
      value: props.initial
    }

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event: ChangeEvent<any>, value: any) {
    this.setState({value: value})
  }

  render() {
    const {
      headerColor, plainTabs, tabs, title, rtlActive, classes
    } = this.props;

    const cardTitle = classNames({
      [classes.cardTitle]: true,
      [classes.cardTitleRTL]: rtlActive,
    });
    const cardHeaderColor = headerColor as unknown as CardHeaderColor;
    return (
      <Card plain={plainTabs} className={this.props.className}>
        <CardHeader color={cardHeaderColor} plain={plainTabs} className={this.props.className}>
          {title !== undefined ? <div className={cardTitle}>{title}</div> : null}
          <Tabs
            value={this.state.value}
            onChange={this.handleChange}
            classes={{
              root: classes.tabsRoot,
              indicator: classes.displayNone,
              scrollButtons: classes.displayNone,
            }}
            variant="scrollable"
            scrollButtons="auto"
          >
            {tabs?.map((prop, key) => {
              var icon = {};
              if (prop.tabIcon) {
                icon = {
                  icon: <prop.tabIcon />,
                };
              }
              return (
                <Tab
                  classes={{
                    root: classes.tabRootButton,
                    selected: classes.tabSelected,
                    wrapper: classes.tabWrapper,
                  }}
                  key={key}
                  label={prop.tabName}
                  {...icon}
                />
              );
            })}
          </Tabs>
        </CardHeader>
        <CardBody className={this.props.className}>
          {tabs?.map((prop, key) => {
            if (key === prop.value) {
              return <div key={key}>{prop.tabContent}</div>;
            }
            return null;
          })}
        </CardBody>
      </Card>
    );
  }
}

export default withStyles(useStyles, {withTheme: true})(CustomTabs);
