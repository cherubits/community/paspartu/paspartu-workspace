import React, { ReactNode } from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// nodejs library to set properties for components
// @material-ui/core components
import { withStyles, WithStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import Hidden from "@material-ui/core/Hidden";
import Drawer from "@material-ui/core/Drawer";
// @material-ui/icons
import Menu from "@material-ui/icons/Menu";
// core components
import useStyles from "./CustomHeaderStyle";

export enum CustomHeaderColor {
  Primary = "primary",
  Info = "info",
  Success = "success",
  Warning = "warning",
  Danger = "danger",
  Transparent = "transparent",
  White = "white",
  Rose = "rose",
  Dark = "dark"
}
export interface CustomHeaderScrollingChangeProps {
  color: CustomHeaderColor;
  height: number;
}
export interface CustomHeaderProps extends WithStyles<typeof useStyles> {
  color: CustomHeaderColor;
  rightLinks?: ReactNode;
  leftLinks?: ReactNode;
  brand?: string;
  fixed?: boolean;
  absolute?: boolean;
  // this will cause the sidebar to change the color from
  // props.color (see above) to changeColorOnScroll.color
  // when the window.pageYOffset is heigher or equal to
  // changeColorOnScroll.height and then when it is smaller than
  // changeColorOnScroll.height change it back to
  // props.color (see above)
  changeColorOnScroll?: CustomHeaderScrollingChangeProps;
};

export interface CustomHeaderState {
  mobileOpen: boolean;
}


class CustomHeader extends React.Component<CustomHeaderProps, CustomHeaderState> {

  static defaultProps = {
    color: CustomHeaderColor.White,
  }

  constructor(props: CustomHeaderProps) {
    super(props);
    this.state = { mobileOpen: false};
    this.headerColorChange.bind(this);
    this.handleDrawerToggle.bind(this);
  }

  handleDrawerToggle() {
    this.setState({mobileOpen: !this.state.mobileOpen});
  }


  headerColorChange() {
    const { color, changeColorOnScroll, classes } = this.props;
    const windowsScrollTop = window.pageYOffset;
    if (changeColorOnScroll && windowsScrollTop > changeColorOnScroll.height) {
      document.body
        .getElementsByTagName("header")[0]
        .classList.remove(classes[color]);
      document.body
        .getElementsByTagName("header")[0]
        .classList.add(classes[changeColorOnScroll.color]);
    } else if (changeColorOnScroll) {
      document.body
        .getElementsByTagName("header")[0]
        .classList.add(classes[color]);
      document.body
        .getElementsByTagName("header")[0]
        .classList.remove(classes[changeColorOnScroll.color]);
    }
  }

  componentDidMount() {
    if (this.props.changeColorOnScroll) {
      window.addEventListener("scroll", this.headerColorChange);
    }
  }

  componentWillUnmount() {
    if (this.props.changeColorOnScroll) {
      window.removeEventListener("scroll", this.headerColorChange);
    }
  }

  render() {
    const {
      color, rightLinks, leftLinks, brand, fixed, absolute,
      classes,
    } = this.props;

    const appBarClasses = classNames({
      [classes.appBar]: true,
      [classes[color]]: color,
      [classes.absolute]: absolute,
      [classes.fixed]: fixed,
    });
    const brandComponent = <Button className={classes.title}>{brand}</Button>;
    return (
      <AppBar className={appBarClasses}>
        <Toolbar className={classes.container}>
          {leftLinks !== undefined ? brandComponent : null}
          <div className={classes.flex}>
            {leftLinks !== undefined ? (
              // <Hidden smDown implementation="css">
                leftLinks
              // </Hidden>
            ) : (
              brandComponent
            )}
          </div>
          {/* <Hidden smDown implementation="css"> */}
            {rightLinks}
          {/* </Hidden> */}
          {/* <Hidden mdUp> */}
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={this.handleDrawerToggle}
            >
              <Menu />
            </IconButton>
          {/* </Hidden> */}
        </Toolbar>
        {/* <Hidden mdUp implementation="js"> */}
          <Drawer
            variant="temporary"
            anchor={"right"}
            open={this.state.mobileOpen}
            classes={{
              paper: classes.drawerPaper,
            }}
            onClose={this.handleDrawerToggle}
          >
            <div className={classes.appResponsive}>
              {leftLinks}
              {rightLinks}
            </div>
          </Drawer>
        {/* </Hidden>C */}
      </AppBar>
    );
  }
}
export default withStyles(useStyles, {withTheme: true})(CustomHeader);
