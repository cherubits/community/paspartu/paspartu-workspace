

import { SizingConstant } from '../../core/SizingConstant';
import CustomButton from './CustomButton';
import { CustomButtonProps } from './CustomButtonProps';

export default {
  title: 'Components/Controls/Button',
  component: CustomButton,
  argTypes: {
    color: {
      name: 'color',
      description: 'Foreground color',
      type: { name: 'string', required: false },
      defaultValue: 'red',
      control: {
        type: 'color'
      }
    },
    // size: {
    //   control: {
    //     // options: [SizingConstant.LARGE, SizingConstant.SMALL],
    //     type: 'enum'
    //   }
    // }
  },
};

export const Primary = (args: CustomButtonProps) => <CustomButton color={args.color}>Button</CustomButton>;

