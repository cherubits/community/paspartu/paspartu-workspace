import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// nodejs library to set properties for components

// material-ui components
import Button from "@material-ui/core/Button";

import useStyles from "./CustomButtonStyle";
import { SemanticColor } from "../../core/SemanticColor";
import { SizingConstant } from "../../core/SizingConstant";
import { withStyles } from "@material-ui/core/styles";
import { CustomButtonProps } from "./CustomButtonProps";

/**
 * Custom react button component
 */
export class CustomButton extends React.Component<CustomButtonProps> {

  render() {

    const {
      color,
      round,
      children,
      disabled,
      simple,
      size,
      block,
      link,
      justIcon,
      muiClasses,
      classes,
      className,
      ...rest
    } = this.props;

    const btnClasses = classNames({
      [classes.button]: true,
      [classes[size ?? SizingConstant.LARGE]]: size,
      [classes[color ?? SemanticColor.Primary]]: color,
      [classes.round]: round,
      [classes.disabled]: disabled,
      [classes.simple]: simple,
      [classes.block]: block,
      [classes.link]: link,
      [classes.justIcon]: justIcon,
      [className]: className,
    });
    return (
      <Button {...rest} classes={muiClasses} className={btnClasses}>
        {children}
      </Button>
    );
  }
}


export default withStyles(useStyles, {withTheme: true})(CustomButton);
