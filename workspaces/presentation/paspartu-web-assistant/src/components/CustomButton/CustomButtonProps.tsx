import { ReactNode } from "react";
import useStyles from "./CustomButtonStyle";
import { SemanticColor } from "../../core/SemanticColor";
import { SizingConstant } from "../../core/SizingConstant";
import { WithStyles } from "@material-ui/core/styles";

/**
 * Custom button properties
 */

export interface CustomButtonProps extends WithStyles<typeof useStyles> {
  /**
   * Foreground color property
   */
  color?: SemanticColor;
  /**
   * Button sizing property
   */
  size?: SizingConstant;
  /**
   * Simple button flag
   */
  simple?: boolean;
  /**
   * Rounded corner flag
   */
  round?: boolean;
  /**
   * Disabled flag
   */
  disabled?: boolean;
  /**
   * Block flag
   */
  block?: boolean;
  /**
   * Link flag
   */
  link?: boolean;
  /**
   * Button is only an icon flag
   */
  justIcon?: boolean;
  /**
   * CSS class property
   */
  className?: any;
  /**
   * Link url property
   */
  href?: string;
  /**
   * Click event handler
   */
  onClick?: any;
  /**
   * Event target
   */
  target?: string;
  /**
   * Streched to container flag
   */
  fullWidth?: boolean;
  /**
   * Use this to pass the classes props from Material-UI
   */
  muiClasses?: any;
  /**
   * Child react components
   */
  children?: ReactNode;
}
