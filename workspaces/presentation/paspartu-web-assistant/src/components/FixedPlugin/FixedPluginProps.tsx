import { BackgroundColor } from "./../../core/BackgroundColor";


export interface FixedPluginProps {
  bgImage?: string;
  handleFixedClick?: Function;
  rtlActive?: boolean;
  fixedClasses?: string;
  bgColor?: BackgroundColor;
  handleColorClick?: Function;
  handleImageClick?: Function;
}
