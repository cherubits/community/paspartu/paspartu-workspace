/*eslint-disable*/
import React from "react";
// nodejs library to set properties for components
// nodejs library that concatenates classes
import classnames from "classnames";

import imagine1 from "./../../assets/img/sidebar-1.jpg";
import imagine2 from "./../../assets/img/sidebar-2.jpg";
import imagine3 from "./../../assets/img/sidebar-3.jpg";
import imagine4 from "./../../assets/img/sidebar-4.jpg";

import Button from "../CustomButton/CustomButton";
import { SemanticColor } from "../../core/SemanticColor";
import { BackgroundColor } from "../../core/BackgroundColor";
import { FixedPluginProps } from "./FixedPluginProps";
import { FixedPluginState } from "./FixedPluginState";



class FixedPlugin extends React.Component<FixedPluginProps, FixedPluginState> {
  constructor(props: FixedPluginProps) {
    super(props);
    this.state = {
      bgImage: props.bgImage,
      bg_checked: false
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props?.handleFixedClick?.call(this);
  };

  render() {
    const {
      bgImage,
    } = this.props;


    return (
      <div
        className={classnames("fixed-plugin", {
          "rtl-fixed-plugin": this.props.rtlActive,
        })}
      >
        <div id="fixedPluginClasses" className={this.props.fixedClasses}>
          <div onClick={this.handleClick}>
            <i className="fa fa-cog fa-2x" />
          </div>
          <ul className="dropdown-menu">
            <li className="header-title">SIDEBAR FILTERS</li>
            <li className="adjustments-line">
              <a className="switch-trigger">
                <div>
                  <span
                    className={
                      this.props.bgColor === "purple"
                        ? "badge filter badge-purple active"
                        : "badge filter badge-purple"
                    }
                    data-color="purple"
                    onClick={() => {
                      this.props?.handleColorClick?.call(this, "purple");
                    }}
                  />
                  <span
                    className={
                      this.props.bgColor === "blue"
                        ? "badge filter badge-blue active"
                        : "badge filter badge-blue"
                    }
                    data-color="blue"
                    onClick={() => {
                      this.props?.handleColorClick?.call(this, "blue");
                    }}
                  />
                  <span
                    className={
                      this.props.bgColor === "green"
                        ? "badge filter badge-green active"
                        : "badge filter badge-green"
                    }
                    data-color="green"
                    onClick={() => {
                      this.props?.handleColorClick?.call(this, "green");
                    }}
                  />
                  <span
                    className={
                      this.props.bgColor === "red"
                        ? "badge filter badge-red active"
                        : "badge filter badge-red"
                    }
                    data-color="red"
                    onClick={() => {
                      this.props?.handleColorClick?.call(this, "red");
                    }}
                  />
                  <span
                    className={
                      this.props.bgColor === "orange"
                        ? "badge filter badge-orange active"
                        : "badge filter badge-orange"
                    }
                    data-color="orange"
                    onClick={() => {
                      this.props?.handleColorClick?.call(this, "orange");
                    }}
                  />
                </div>
              </a>
            </li>
            <li className="header-title">Images</li>
            <li className={bgImage === imagine1 ? "active" : ""}>
              <a
                className="img-holder switch-trigger"
                onClick={() => {
                  this.setState({bgImage: imagine1});
                  this.props?.handleImageClick?.call(this, imagine1);
                }}
              >
                <img src={imagine1} alt="..." />
              </a>
            </li>
            <li className={bgImage === imagine2 ? "active" : ""}>
              <a
                className="img-holder switch-trigger"
                onClick={() => {
                  this.setState({bgImage: imagine2});
                  this.props?.handleImageClick?.call(this, imagine2);
                }}
              >
                <img src={imagine2} alt="..." />
              </a>
            </li>
            <li className={bgImage === imagine3 ? "active" : ""}>
              <a
                className="img-holder switch-trigger"
                onClick={() => {
                  this.setState({bgImage: imagine3});
                  this.props?.handleImageClick?.call(this, imagine3);
                }}
              >
                <img src={imagine3} alt="..." />
              </a>
            </li>
            <li className={bgImage === imagine4 ? "active" : ""}>
              <a
                className="img-holder switch-trigger"
                onClick={() => {
                  this.setState({bgImage: imagine4});
                  this.props?.handleImageClick?.call(this, imagine4);
                }}
              >
                <img src={imagine4} alt="..." />
              </a>
            </li>

            <li className="button-container">
              <div className="button-container">
                <Button
                  color={SemanticColor.Success}
                  href="https://www.creative-tim.com/product/material-dashboard-react?ref=mdr-fixed-plugin"
                  target="_blank"
                  fullWidth
                >
                  Download free!
                </Button>
              </div>
            </li>
            <li className="button-container">
              <div className="button-container">
                <Button
                  color={SemanticColor.Warning}
                  href="https://www.creative-tim.com/product/material-dashboard-pro-react?ref=mdr-fixed-plugin"
                  target="_blank"
                  fullWidth
                >
                  Get PRO version
                </Button>
              </div>
            </li>
            <li className="button-container">
              <Button
                color={SemanticColor.Info}
                fullWidth
                href="https://demos.creative-tim.com/material-dashboard-react/#/documentation/tutorial?ref=mdr-fixed-plugin"
                target="_blank"
              >
                Documentation
              </Button>
            </li>
            <li className="adjustments-line" />
          </ul>
        </div>
      </div>
    );
  }
}

export default FixedPlugin;
