import React from "react";
// @material-ui/core components
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
// core components
import useTableStyles from "./CustomTableStyle";
import { CustomTableProperties } from "./CustomTableProperties";
import { SemanticColor } from "../../core/SemanticColor";

class CustomTable extends React.Component<CustomTableProperties> {

  render() {
    const { tableHead, tableData, tableHeaderColor, classes } = this.props;
    const headerKey = (tableHeaderColor ?? SemanticColor.Info).toString() + "TableHeader";
    const tableHeadClassName = Reflect.get(classes, headerKey);
    return (
      <div className={classes.tableResponsive}>
        <Table className={classes.table}>
          {tableHead !== undefined ? (
            <TableHead className={tableHeadClassName}>
              <TableRow className={classes.tableHeadRow}>
                {tableHead.map((prop, key) => {
                  return (
                    <TableCell
                      className={classes.tableCell + " " + classes.tableHeadCell}
                      key={key}
                    >
                      {prop}
                    </TableCell>
                  );
                })}
              </TableRow>
            </TableHead>
          ) : null}
          <TableBody>
            {tableData?.map((rowProp, rowKey) => {
              return (
                <TableRow key={rowKey} className={classes.tableBodyRow}>
                  {rowProp.map((prop: boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null | undefined, key: React.Key | null | undefined) => {
                    return (
                      <TableCell className={classes.tableCell} key={key}>
                        {prop}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default withStyles(useTableStyles, {withTheme: true})(CustomTable);
