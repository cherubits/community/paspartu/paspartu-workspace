import { WithStyles } from "@material-ui/core/styles";
import useTableStyles from "./CustomTableStyle";
import { SemanticColor } from "../../core/SemanticColor";



export interface CustomTableProperties extends WithStyles<typeof useTableStyles> {
  tableHeaderColor?: SemanticColor;
  tableHead?: Array<string>;
  tableData?: Array<Array<string>>;
}
