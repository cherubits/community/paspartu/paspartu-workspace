import { WithStyles } from "@material-ui/core/styles";
import useStyles from "./SnackbarContentStyle";
import { SemanticColor } from "../../core/SemanticColor";
import { SnackbarPlace } from "../../core/SnackbarPlace";





export interface SnackbarProperties extends WithStyles<typeof useStyles> {
  message?: string;
  color?: SemanticColor;
  close?: boolean;
  icon?: any;
  place?: SnackbarPlace;
  open?: boolean;
  rtlActive?: boolean;
  closeNotification?: Function;
}
