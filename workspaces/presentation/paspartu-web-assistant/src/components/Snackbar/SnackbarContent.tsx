import React, { ReactNode } from "react";
import classNames from "classnames";
// @material-ui/core components
import { WithStyles, withStyles } from "@material-ui/core/styles";
import Snack from "@material-ui/core/SnackbarContent";
import IconButton from "@material-ui/core/IconButton";
// @material-ui/icons
import Close from "@material-ui/icons/Close";
// core components
import useSnackbarContentStyles from "./SnackbarContentStyle";
import { SemanticColor } from "../../core/SemanticColor";



export interface SnackbarContentProperties extends WithStyles<typeof useSnackbarContentStyles> {
  message?: string,
  color?: SemanticColor,
  close?: boolean,
  icon?: any,
  rtlActive?: boolean,
  action?: ReactNode[]
}

class SnackbarContent extends React.Component<SnackbarContentProperties> {


  render() {
    const { message, close, icon, rtlActive, classes } = this.props;
    let action: any[] = [];
    const messageClasses = classNames({
      [classes.iconMessage]: icon !== undefined,
    });
    if (close !== undefined) {
      action = [
        <IconButton
          className={classes.iconButton}
          key="close"
          aria-label="Close"
          color="inherit"
        >
          <Close className={classes.close} />
        </IconButton>,
      ];
    }
    return (
      <Snack
        message={
          <div>
            {icon !== undefined ? <this.props.icon className={classes.icon} /> : null}
            <span className={messageClasses}>{message}</span>
          </div>
        }
        classes={{
          root: classes.root + " " + classes[SemanticColor.Danger],
          message: classes.message,
          action: classNames({ [classes.actionRTL]: rtlActive }),
        }}
        action={action}
      />
    );
  }
}

export default withStyles(useSnackbarContentStyles, {withTheme: true})(SnackbarContent);
