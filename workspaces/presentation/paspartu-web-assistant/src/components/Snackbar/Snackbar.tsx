import React from "react";
import classNames from "classnames";
// @material-ui/core components
import { withStyles } from "@material-ui/core/styles";
import Snack from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
// @material-ui/icons
import Close from "@material-ui/icons/Close";
// core components
import useStyles from "./SnackbarContentStyle";
import { SnackbarProperties } from "./SnackbarProperties";
import {SemanticColor} from '../../core/SemanticColor';
import {SnackbarPlace} from '../../core/SnackbarPlace';


class Snackbar extends React.Component<SnackbarProperties> {

  render() {

    const { message, close, icon, place, open, rtlActive, classes } = this.props;
    const placeStr = place ?? SnackbarPlace.TC as string;
    // const colorStr = (color ?? SemanticColor.Transparent) as string;
    let action: any[] = [];
    const messageClasses = classNames({
      [classes.iconMessage]: icon !== undefined,
    });
    if (close !== undefined) {
      action = [
        <IconButton
          className={classes.iconButton}
          key="close"
          aria-label="Close"
          color="inherit"
          onClick={() => this.props.closeNotification?.call(this)}
        >
          <Close className={classes.close} />
        </IconButton>,
      ];
    }
    return (
      <Snack
        anchorOrigin={{
          vertical: placeStr.indexOf("t") === -1 ? "bottom" : "top",
          horizontal:
          placeStr.indexOf("l") !== -1
              ? "left"
              : placeStr.indexOf("c") !== -1
              ? "center"
              : "right",
        }}
        open={open}
        message={
          <div>
            {icon !== undefined ? <this.props.icon className={classes.icon} /> : null}
            <span className={messageClasses}>{message}</span>
          </div>
        }
        action={action}
        ContentProps={{
          classes: {
            root: classes.root + " " + classes[SemanticColor.Info],
            message: classes.message,
            action: classNames({ [classes.actionRTL]: rtlActive }),
          },
        }}
      />
    );
  }
}

export default withStyles(useStyles, {withTheme: true})(Snackbar);
