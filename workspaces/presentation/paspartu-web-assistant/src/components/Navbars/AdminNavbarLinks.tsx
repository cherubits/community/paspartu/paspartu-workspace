import React from "react";
import classNames from "classnames";
// @material-ui/core components
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Hidden from "@material-ui/core/Hidden";
import Poppers from "@material-ui/core/Popper";
import Divider from "@material-ui/core/Divider";
// @material-ui/icons
import Person from "@material-ui/icons/Person";
import Notifications from "@material-ui/icons/Notifications";
import Dashboard from "@material-ui/icons/Dashboard";
import Search from "@material-ui/icons/Search";
// core components
import CustomInput from "../CustomInput/CustomInput";
import CustomButton from "../CustomButton/CustomButton";

import useStyles from "./HeaderLinksStyle";
import { withStyles } from "@material-ui/core/styles";
import { SemanticColor } from "../../core/SemanticColor";
import { HeaderLinksProps } from "./HeaderLinksProps";
import { AdminNavbarLinksState } from "./AdminNavbarLinksState";


class AdminNavbarLinks extends React.Component<HeaderLinksProps, AdminNavbarLinksState> {

  constructor(props: HeaderLinksProps) {
    super(props);
    this.state = {
      openNotification: null,
      openProfile: null
    };

    this.handleClickNotification = this.handleClickNotification.bind(this);
    this.handleClickProfile = this.handleClickProfile.bind(this);
    this.handleCloseNotification = this.handleCloseNotification.bind(this);
    this.handleCloseProfile = this.handleCloseProfile.bind(this);
  }

  handleClickNotification(event: Event) {
    if (this.state.openNotification && this.state.openNotification === event.target) {
      this.setState({openNotification: null});
    } else {
      this.setState({openNotification: event.currentTarget});
    }
  }
  handleCloseNotification() {
    this.setState({openNotification: null});
  }
  handleClickProfile(event: Event) {
    if (this.state.openProfile && this.state.openProfile === event.target) {
      this.setState({openProfile: null});
    } else {
      this.setState({openProfile: event.currentTarget});
    }
  }
  handleCloseProfile() {
    this.setState({openProfile: null});
  }

  render() {
    const {classes} = this.props;


    return (
      <div>
        <div className={classes.searchWrapper}>
          <CustomInput
            formControlProps={{
              className: classes.margin + " " + classes.search,
            }}
            inputProps={{
              placeholder: "Search",
              inputProps: {
                "aria-label": "Search",
              },
            }}
          />
          <CustomButton color={SemanticColor.White} aria-label="edit" justIcon round>
            <Search />
          </CustomButton>
        </div>
        <CustomButton
          color={window.innerWidth > 959 ? SemanticColor.Primary : SemanticColor.Info}
          justIcon={window.innerWidth > 959}
          simple={!(window.innerWidth > 959)}
          aria-label="Dashboard"
          round
          className={classes.buttonLink}
        >
          <Dashboard className={this.props.icons} />
          {/* <Hidden mdUp implementation="css"> */}
            <p className={classes.linkText}>Dashboard</p>
          {/* </Hidden> */}
        </CustomButton>
        <div className={classes.manager}>
          <CustomButton
            color={window.innerWidth > 959 ? SemanticColor.Primary : SemanticColor.Info}
            justIcon={window.innerWidth > 959}
            simple={!(window.innerWidth > 959)}
            aria-owns={this.state.openNotification ? "notification-menu-list-grow" : null}
            aria-haspopup="true"
            onClick={this.handleClickNotification}
            className={classes.buttonLink}
            round
          >
            <Notifications className={this.props.icons} />
            <span className={classes.notifications}>5</span>
            {/* <Hidden mdUp implementation="css"> */}
              <p onClick={this.handleCloseNotification} className={classes.linkText}>
                Notification
              </p>
            {/* </Hidden> */}
          </CustomButton>
          <Poppers
            open={Boolean(this.state.openNotification)}
            anchorEl={this.state.openNotification}
            transition
            disablePortal
            className={
              classNames({ [classes.popperClose]: !this.state.openNotification }) +
              " " +
              classes.popperNav
            }
          >
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                style={{
                  transformOrigin:
                    placement === "bottom" ? "center top" : "center bottom",
                }}
              >
                <Paper>
                  <ClickAwayListener onClickAway={this.handleCloseNotification}>
                    <MenuList role="menu">
                      <MenuItem
                        onClick={this.handleCloseNotification}
                        className={classes.dropdownItem}
                      >
                        Mike John responded to your email
                      </MenuItem>
                      <MenuItem
                        onClick={this.handleCloseNotification}
                        className={classes.dropdownItem}
                      >
                        You have 5 new tasks
                      </MenuItem>
                      <MenuItem
                        onClick={this.handleCloseNotification}
                        className={classes.dropdownItem}
                      >
                        You{"'"}re now friend with Andrew
                      </MenuItem>
                      <MenuItem
                        onClick={this.handleCloseNotification}
                        className={classes.dropdownItem}
                      >
                        Another Notification
                      </MenuItem>
                      <MenuItem
                        onClick={this.handleCloseNotification}
                        className={classes.dropdownItem}
                      >
                        Another One
                      </MenuItem>
                    </MenuList>
                  </ClickAwayListener>
                </Paper>
              </Grow>
            )}
          </Poppers>
        </div>
        <div className={classes.manager}>
          <CustomButton
            color={window.innerWidth > 959 ? SemanticColor.Primary : SemanticColor.Info}
            justIcon={window.innerWidth > 959}
            simple={!(window.innerWidth > 959)}
            aria-owns={this.state.openProfile ? "profile-menu-list-grow" : null}
            aria-haspopup="true"
            onClick={this.handleClickProfile}
            className={classes.buttonLink}
            round
          >
            <Person className={this.props.icons} />
            {/* <Hidden mdUp implementation="css"> */}
              <p className={classes.linkText}>Profile</p>
            {/* </Hidden> */}
          </CustomButton>
          <Poppers
            open={Boolean(this.state.openProfile)}
            anchorEl={this.state.openProfile}
            transition
            disablePortal
            className={
              classNames({ [classes.popperClose]: !this.state.openProfile }) +
              " " +
              classes.popperNav
            }
          >
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                style={{
                  transformOrigin:
                    placement === "bottom" ? "center top" : "center bottom",
                }}
              >
                <Paper>
                  <ClickAwayListener onClickAway={this.handleCloseProfile}>
                    <MenuList role="menu">
                      <MenuItem
                        onClick={this.handleCloseProfile}
                        className={classes.dropdownItem}
                      >
                        Profile
                      </MenuItem>
                      <MenuItem
                        onClick={this.handleCloseProfile}
                        className={classes.dropdownItem}
                      >
                        Settings
                      </MenuItem>
                      <Divider light />
                      <MenuItem
                        onClick={this.handleCloseProfile}
                        className={classes.dropdownItem}
                      >
                        Logout
                      </MenuItem>
                    </MenuList>
                  </ClickAwayListener>
                </Paper>
              </Grow>
            )}
          </Poppers>
        </div>
      </div>
    );
  }
}

export default withStyles(useStyles, {withTheme: true})(AdminNavbarLinks);
