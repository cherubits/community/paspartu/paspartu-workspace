import React, { MouseEventHandler } from "react";
import classNames from "classnames";
// @material-ui/core components
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
// @material-ui/icons
import Menu from "@material-ui/icons/Menu";
// core components
import AdminNavbarLinks from "./AdminNavbarLinks";
import RTLNavbarLinks from "./RTLNavbarLinks";
import Button from "../CustomButton/CustomButton";

//hooks
import { withStyles, WithStyles } from "@material-ui/core/styles";

import useStyles from "./HeaderStyle";
import { SemanticColor } from "../../core/SemanticColor";


export interface HeaderProps extends WithStyles<typeof useStyles> {
  color?: SemanticColor;
  rtlActive?: boolean;
  handleDrawerToggle: MouseEventHandler<HTMLButtonElement>;
  routes?: Array<any>,
};

export enum HeaderClasses {
  AppBar = "appBar",
  Flex = "flex",
  Container = "container",
  Title = "title",
  AppResponsive = "appResponsive",
  Primary = "primary",
  Info = "info",
  Success = "success",
  Warning = "warning",
  Danger = "danger"
}


// export class ButtonClasses {
//   Color = "color",
//   Round = "round",
//   Size = "size",
//   Block = "block",
//   Disabled = "disabled",
//   Children = "children",
//   Link = "link",
//   ClassName = "className",
//   Simple = "simple",
//   JustIcon = "justIcon",
//   MuiClasses = "muiClasses"
// }

class Header extends React.Component<HeaderProps> {

  static defaultProps = {
    routes: [],
  };

  get routeName() {
    let name = "";
    this.props?.routes?.forEach((route) => {
      if (window.location.href.indexOf(route.layout + route.path) !== -1) {
        name = this.props.rtlActive ? route.rtlName : route.name;
      }
    });
    return name;
  }

  render() {
    const {
      classes,
    } = this.props;
    const routeName = this.routeName;

    //const routeName = useRouteName();
    const { color } = this.props;
    const colorStr = color as unknown as HeaderClasses;
    const appBarClasses = classNames({
      [` ${classes[colorStr]}`]: color,
    });
    const buttonClassName = classes.title;
    return (
      <AppBar className={classes.appBar + appBarClasses} position="static">
        <Toolbar className={classes.container}>
          <div className={classes.flex}>
            <Button color={SemanticColor.Primary} href="#" className={buttonClassName}>
              {routeName}
            </Button>
          </div>
          {/* <Hidden smDown implementation="css"> */}
            {this.props.rtlActive ? <RTLNavbarLinks /> : <AdminNavbarLinks />}
          {/* </Hidden> */}
          {/* <Hidden mdUp implementation="css"> */}
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={this.props.handleDrawerToggle}
            >
              <Menu />
            </IconButton>
          {/* </Hidden> */}
        </Toolbar>
      </AppBar>
    );
  }
}

export default withStyles(useStyles, {withTheme: true})(Header);
