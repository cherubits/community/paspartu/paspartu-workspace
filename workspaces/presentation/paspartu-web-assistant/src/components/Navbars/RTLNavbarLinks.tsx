import React from "react";
import classNames from "classnames";
// @material-ui/core components
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Poppers from "@material-ui/core/Popper";
// @material-ui/icons
import Person from "@material-ui/icons/Person";
import Notifications from "@material-ui/icons/Notifications";
import Dashboard from "@material-ui/icons/Dashboard";
import Search from "@material-ui/icons/Search";
// core components
import CustomInput from "../CustomInput/CustomInput";
import Button from "../CustomButton/CustomButton";
import { withStyles } from "@material-ui/core/styles";

import useStyles from "./HeaderLinksStyle";
import { SemanticColor } from "../../core/SemanticColor";
import { HeaderLinksProps } from "./HeaderLinksProps";
import { RTLNavbarLinksState } from "./RTLNavbarLinksState";


class RTLNavbarLinks extends React.Component<HeaderLinksProps, RTLNavbarLinksState> {
  constructor(props: HeaderLinksProps) {
    super(props);
    this.state = {
      open: false
    };

    this.handleClose = this.handleClose.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
  }


  handleToggle(event: React.MouseEvent<HTMLParagraphElement, MouseEvent>) {
    if (this.state.open && this.state.open.contains(event.target)) {
      this.setState({open: null});
    } else {
      this.setState({open: event.currentTarget});
    }
  }

  handleClose() {
    this.setState({open: null});
  }


  render() {
    const {classes} = this.props;

    return (
      <div>
        <div className={classes.searchWrapper}>
          <CustomInput
            formControlProps={{
              className: classes.margin + " " + classes.search,
            }}
            inputProps={{
              placeholder: "جستجو...",
              inputProps: {
                "aria-label": "Search",
              },
            }}  />
          <Button color={SemanticColor.Primary} aria-label="edit" justIcon round>
            <Search />
          </Button>
        </div>
        <Button
          color={window.innerWidth > 959 ? SemanticColor.Primary : SemanticColor.Info}
          justIcon={window.innerWidth > 959}
          simple={!(window.innerWidth > 959)}
          aria-label="Dashboard"
          className={classes.buttonLink}
        >
          <Dashboard className={this.props.icons} />
          {/* <Hidden mdUp implementation="css"> */}
            <p className={classes.linkText}>آمارها</p>
          {/* </Hidden> */}
        </Button>
        <div className={classes.manager}>
          <Button
            color={window.innerWidth > 959 ? SemanticColor.Primary : SemanticColor.Info}
            justIcon={window.innerWidth > 959}
            simple={!(window.innerWidth > 959)}
            aria-owns={this.state.open ? "menu-list-grow" : null}
            aria-haspopup="true"
            onClick={this.handleToggle}
            className={classes.buttonLink}
          >
            <Notifications className={this.props.icons} />
            <span className={classes.notifications}>۵</span>
            {/* <Hidden mdUp implementation="css"> */}
              <p onClick={this.handleToggle} className={classes.linkText}>
                اعلان‌ها
              </p>
            {/* </Hidden> */}
          </Button>
          <Poppers
            open={Boolean(this.state.open)}
            anchorEl={this.state.open}
            transition
            disablePortal
            className={
              classNames({ [classes.popperClose]: !this.state.open }) +
              " " +
              classes.popperNav
            }
          >
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                style={{
                  transformOrigin:
                    placement === "bottom" ? "center top" : "center bottom",
                }}
              >
                <Paper>
                  <ClickAwayListener onClickAway={this.handleClose}>
                    <MenuList role="menu">
                      <MenuItem
                        onClick={this.handleClose}
                        className={classes.dropdownItem}
                      >
                        محمدرضا به ایمیل شما پاسخ داد
                      </MenuItem>
                      <MenuItem
                        onClick={this.handleClose}
                        className={classes.dropdownItem}
                      >
                        شما ۵ وظیفه جدید دارید
                      </MenuItem>
                      <MenuItem
                        onClick={this.handleClose}
                        className={classes.dropdownItem}
                      >
                        از حالا شما با علیرضا دوست هستید
                      </MenuItem>
                      <MenuItem
                        onClick={this.handleClose}
                        className={classes.dropdownItem}
                      >
                        اعلان دیگر
                      </MenuItem>
                      <MenuItem
                        onClick={this.handleClose}
                        className={classes.dropdownItem}
                      >
                        اعلان دیگر
                      </MenuItem>
                    </MenuList>
                  </ClickAwayListener>
                </Paper>
              </Grow>
            )}
          </Poppers>
        </div>
        <Button
          color={window.innerWidth > 959 ? SemanticColor.Primary : SemanticColor.Info}
          justIcon={window.innerWidth > 959}
          simple={!(window.innerWidth > 959)}
          aria-label="Person"
          className={!!classes.buttonLink}
        >
          <Person className={this.props.icons} />
          {/* <Hidden mdUp implementation="css"> */}
            <p className={classes.linkText}>حساب کاربری</p>
          {/* </Hidden> */}
        </Button>
      </div>
    );
  }
}

export default withStyles(useStyles, {withTheme: true})(RTLNavbarLinks);
