import { WithStyles } from "@material-ui/core/styles";
import useStyles from "./HeaderLinksStyle";


export interface HeaderLinksProps extends WithStyles<typeof useStyles> {
  icons?: any;

}
