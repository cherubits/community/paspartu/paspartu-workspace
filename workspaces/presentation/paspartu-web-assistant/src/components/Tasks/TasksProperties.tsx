import { WithStyles } from "@material-ui/core/styles";
import useTasksStyles from "./TasksStyle";


export interface TasksProperties extends WithStyles<typeof useTasksStyles> {
  tasksIndexes?: Array<number>;
  tasks?: any[];
  rtlActive?: boolean;
  checkedIndexes?: Array<number>;
}
