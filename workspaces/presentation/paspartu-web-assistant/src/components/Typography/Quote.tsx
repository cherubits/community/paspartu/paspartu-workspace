import React from "react";
// @material-ui/core components
import { withStyles } from "@material-ui/core/styles";
// core components
import useStyles from "./TypographyStyle";
import { QuoteProperties } from "./QuoteProperties";


class Quote extends React.Component<QuoteProperties> {

  render() {
    const { text, author, classes } = this.props;
    return (
      <blockquote className={classes.defaultFontStyle + " " + classes.quote}>
        <p className={classes.quoteText}>{text}</p>
        <small className={classes.quoteAuthor}>{author}</small>
      </blockquote>
    );
  }
}

export default withStyles(useStyles, {withTheme: true})(Quote);