import { WithStyles } from "@material-ui/core/styles";
import useStyles from "./TypographyStyle";

export interface QuoteProperties extends WithStyles<typeof useStyles> {
  text: string;
  author: string;
}
