import { WithStyles } from "@material-ui/core/styles";
import useStyles from "./TypographyStyle";

export interface TyphographyProperties extends WithStyles<typeof useStyles> {
  /**
   * Content children in the DOM.
   */
  children: any;
}
