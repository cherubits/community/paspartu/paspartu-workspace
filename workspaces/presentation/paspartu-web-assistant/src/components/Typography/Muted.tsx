import React from "react";
// @material-ui/core components
import { withStyles } from "@material-ui/core/styles";
// core components
import useStyles from "./TypographyStyle";
import { TyphographyProperties } from "./TyphographyProperties";



class Muted extends React.Component<TyphographyProperties> {

  render() {

    const { children, classes } = this.props;
    return (
      <div className={classes.defaultFontStyle + " " + classes.mutedText}>
        {children}
      </div>
    );
  }
}

export default withStyles(useStyles, {withTheme: true})(Muted);