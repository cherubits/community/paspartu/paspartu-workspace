import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// nodejs library to set properties for components
// @material-ui/core components
// @material-ui/icons

// core components
import useStyles from "./CardIconStyle";
import { withStyles } from "@material-ui/core/styles";
import { CardHeaderColor } from "../../core/CardHeaderColor";
import { CardIconProps } from "./CardIconProps";



class CardIcon extends React.Component<CardIconProps> {


  render() {

    const { className, children, color, classes, ...rest } = this.props;
    const cardHeaderColor = color as unknown as CardHeaderColor;
    const cardIconClasses = classNames({
      [classes.cardIcon]: true,
      [classes[cardHeaderColor]]: color,
      className: className,
    });
    return (
      <div className={cardIconClasses} {...rest}>
        {children}
      </div>
    );
  }
}

export default withStyles(useStyles, {withTheme: true})(CardIcon);
