import { ReactNode } from "react";
import useStyles from "./CardBodyStyle";
import { WithStyles } from "@material-ui/core/styles";



export interface CardBodyProps extends WithStyles<typeof useStyles> {
  children?: ReactNode;
  className?: string;
  profile?: boolean;
  plain?: boolean;
}
