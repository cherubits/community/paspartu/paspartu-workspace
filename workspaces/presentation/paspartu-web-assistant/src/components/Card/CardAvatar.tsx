import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// nodejs library to set properties for components
// @material-ui/core components
// @material-ui/icons
// core components
import { withStyles } from "@material-ui/core/styles";

import useStyles from "./CardAvatarStyle";
import { CardAvatarProps } from "./CardAvatarProps";




class CardAvatar extends React.Component<CardAvatarProps> {

  render() {
   
    const { children, className, plain, profile, classes, ...rest } = this.props;
    const cardAvatarClasses = classNames({
      [classes.cardAvatar]: true,
      [classes.cardAvatarProfile]: profile,
      [classes.cardAvatarPlain]: plain,
      [className]: className,
    });
    return (
      <div className={cardAvatarClasses} {...rest}>
        {children}
      </div>
    );
  }
}

export default withStyles(useStyles, {withTheme: true})(CardAvatar);