import { ReactNode } from "react";
import useStyles from "./CardIconStyle";
import { WithStyles } from "@material-ui/core/styles";
import { CardHeaderColor } from "../../core/CardHeaderColor";


export interface CardIconProps extends WithStyles<typeof useStyles> {
  children?: ReactNode;
  className?: string;
  color?: CardHeaderColor;
}
