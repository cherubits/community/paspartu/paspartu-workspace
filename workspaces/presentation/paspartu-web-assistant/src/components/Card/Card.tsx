import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// nodejs library to set properties for components
// @material-ui/core components
// @material-ui/icons

// core components
import useStyles from "./CardStyle";
import { withStyles } from "@material-ui/core/styles";
import { CardProps } from "./CardProps";


class Card extends React.Component<CardProps> {
  
  render() {
    const { className, children, plain, profile, chart, classes, ...rest } = this.props;
    const cardClasses = classNames({
      [classes.card]: true,
      [classes.cardPlain]: plain,
      [classes.cardProfile]: profile,
      [classes.cardChart]: chart,
      className: className,
    });
    return (
      <div className={cardClasses} {...rest}>
        {children}
      </div>
    );
  }
}

export default withStyles(useStyles, {withTheme: true})(Card);