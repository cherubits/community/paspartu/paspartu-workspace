import { ReactNode } from "react";
import useStyles from "./CardStyle";
import { WithStyles } from "@material-ui/core/styles";




export interface CardProps extends WithStyles<typeof useStyles> {
  className?: string;
  plain?: boolean;
  profile?: boolean;
  chart?: boolean;
  children?: ReactNode;
}
