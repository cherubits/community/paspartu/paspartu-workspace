import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// nodejs library to set properties for components
// @material-ui/core components
// @material-ui/icons

// core components
import useStyles from "./CardFooterStyle";
import { withStyles } from "@material-ui/core/styles";
import { CardFooterProps } from "./CardFooterProps";



class CardFooter extends React.Component<CardFooterProps> {

  render() {

    const { className, classes, children, plain, profile, stats, chart, ...rest } = this.props;
    const cardFooterClasses = classNames({
      [classes.cardFooter]: true,
      [classes.cardFooterPlain]: plain,
      [classes.cardFooterProfile]: profile,
      [classes.cardFooterStats]: stats,
      [classes.cardFooterChart]: chart,
      className: className,
    });
    return (
      <div className={cardFooterClasses} {...rest}>
        {children}
      </div>
    );
  }
}

export default withStyles(useStyles, {withTheme: true})(CardFooter);
