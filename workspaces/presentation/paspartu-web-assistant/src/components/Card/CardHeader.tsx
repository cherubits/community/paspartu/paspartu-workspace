import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// nodejs library to set properties for components
// @material-ui/core components
// @material-ui/icons

// core components
import useStyles from "./CardHeaderStyle";
import { withStyles } from "@material-ui/core/styles";
import { CardHeaderColor } from "../../core/CardHeaderColor";
import { CardHeaderProps } from "./CardHeaderProps";



class CardHeader extends React.Component<CardHeaderProps> {

  render() {
    const { className, children, color, plain, stats, icon, classes, ...rest } = this.props;
    const cardHeaderColor = color as unknown as CardHeaderColor;
    console.log(cardHeaderColor);
    const cardHeaderClasses = classNames({
      [classes.cardHeader]: true,
      [classes[cardHeaderColor]]: color,
      [classes.cardHeaderPlain]: plain,
      [classes.cardHeaderStats]: stats,
      [classes.cardHeaderIcon]: icon,
      className: className,
    });
    return (
      <div className={cardHeaderClasses} {...rest}>
        {children}
      </div>
    );
  }
}

export default withStyles(useStyles, {withTheme: true})(CardHeader);
