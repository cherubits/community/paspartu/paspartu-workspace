import { ReactNode } from "react";
import { WithStyles } from "@material-ui/core/styles";
import useStyles from "./CardAvatarStyle";



export interface CardAvatarProps extends WithStyles<typeof useStyles> {
  children?: ReactNode;
  className: string;
  profile?: boolean;
  plain?: boolean;
}
