import useStyles from "./CardFooterStyle";
import { WithStyles } from "@material-ui/core/styles";


export interface CardFooterProps extends WithStyles<typeof useStyles> {
  className?: string;
  profile?: boolean;
  plain?: boolean;
  stats?: boolean;
  chart?: boolean;
  children?: any;
}
