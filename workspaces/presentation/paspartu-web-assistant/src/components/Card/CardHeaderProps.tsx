import { ReactNode } from "react";
import useStyles from "./CardHeaderStyle";
import { WithStyles } from "@material-ui/core/styles";
import { CardHeaderColor } from "../../core/CardHeaderColor";


export interface CardHeaderProps extends WithStyles<typeof useStyles> {
  children?: ReactNode;
  className?: string;
  profile?: boolean;
  plain?: boolean;
  stats?: boolean;
  chart?: boolean;
  icon?: boolean;
  color?: CardHeaderColor;
}
