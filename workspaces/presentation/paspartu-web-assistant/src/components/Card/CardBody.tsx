import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// nodejs library to set properties for components
// @material-ui/core components
// @material-ui/icons

// core components
import useStyles from "./CardBodyStyle";
import { withStyles } from "@material-ui/core/styles";
import { CardBodyProps } from "./CardBodyProps";




class CardBody extends React.Component<CardBodyProps> {
  
  render() {

    const { className, children, plain, profile, classes, ...rest } = this.props;
    const cardBodyClasses = classNames({
      [classes.cardBody]: true,
      [classes.cardBodyPlain]: plain,
      [classes.cardBodyProfile]: profile,
      className: className,
    });
    return (
      <div className={cardBodyClasses} {...rest}>
        {children}
      </div>
    );
  }
}
export default withStyles(useStyles, {withTheme: true})(CardBody);
