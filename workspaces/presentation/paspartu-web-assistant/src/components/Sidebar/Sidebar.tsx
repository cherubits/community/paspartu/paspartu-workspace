/*eslint-disable*/
import React from "react";
import classNames from "classnames";
import { NavLink, useLocation } from "react-router-dom";
// @material-ui/core components
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Icon from "@material-ui/core/Icon";
// core components
import AdminNavbarLinks from "../Navbars/AdminNavbarLinks";
import RTLNavbarLinks from "../Navbars/RTLNavbarLinks";
import { withStyles } from "@material-ui/core/styles";
import useStyles from './SidebarStyle';
import {SidebarProps} from './SidebarProps';
import { BackgroundColor } from "../../core/BackgroundColor";

// import { withRouter } from 'react-router-dom';


class Sidebar extends React.Component<SidebarProps> {
  constructor(props: SidebarProps) {
    super(props);

    // this.activeRoute = this.activeRoute.bind(this);
  }

  componentDidUpdate(prevProps: SidebarProps) {
    // will be true
    // const locationChanged = this.props.location !== prevProps.location;
  }

  activeRoute(routeName?: string): boolean {
    return location.pathname === routeName;
  }

  render() {
    // verifies if routeName is the one active (in browser input)

    const { color, logo, image, logoText, routes, classes } = this.props;
    const c = color as unknown as BackgroundColor;
    var links = (
      <List className={classes.list}>
        {routes?.map((prop, key) => {
          var activePro = " ";
          var listItemClasses;
          if (prop.path === "/upgrade-to-pro") {
            activePro = classes.activePro + " ";
            listItemClasses = classNames({
              [" " + classes[c]]: true,
            });
          } else {
            listItemClasses = classNames({
              [" " + classes[c]]: this.activeRoute(prop.layout + prop.path),
            });
          }
          const whiteFontClasses = classNames({
            [" " + classes.whiteFont]: this.activeRoute(prop.layout + prop.path),
          });
          return (
            <NavLink
              to={prop.layout + prop.path}
              className={activePro + classes.item}
              // activeClassName="active"
              key={key}
            >
              <ListItem button className={classes.itemLink + listItemClasses}>
                {typeof prop.icon === "string" ? (
                  <Icon
                    className={classNames(classes.itemIcon, whiteFontClasses, {
                      [classes.itemIconRTL]: this.props.rtlActive,
                    })}
                  >
                    {prop.icon}
                  </Icon>
                ) : (
                  <prop.icon
                    className={classNames(classes.itemIcon, whiteFontClasses, {
                      [classes.itemIconRTL]: this.props.rtlActive,
                    })}
                  />
                )}
                <ListItemText
                  primary={this.props.rtlActive ? prop.rtlName : prop.name}
                  className={classNames(classes.itemText, whiteFontClasses, {
                    [classes.itemTextRTL]: this.props.rtlActive,
                  })}
                  disableTypography={true}
                />
              </ListItem>
            </NavLink>
          );
        })}
      </List>
    );
    var brand = (
      <div className={classes.logo}>
        <a
          href="https://www.creative-tim.com?ref=mdr-sidebar"
          className={classNames(classes.logoLink, {
            [classes.logoLinkRTL]: this.props.rtlActive,
          })}
          target="_blank"
        >
          <div className={classes.logoImage}>
            <img src={logo} alt="logo" className={classes.img} />
          </div>
          {logoText}
        </a>
      </div>
    );
    return (
      <div>
        {/* <Hidden mdUp implementation="css"> */}
          <Drawer
            variant="temporary"
            anchor={this.props.rtlActive ? "left" : "right"}
            open={this.props.open}
            classes={{
              paper: classNames(classes.drawerPaper, {
                [classes.drawerPaperRTL]: this.props.rtlActive,
              }),
            }}
            onClose={this.props.handleDrawerToggle}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {brand}
            <div className={classes.sidebarWrapper}>
              {this.props.rtlActive ? <RTLNavbarLinks /> : <AdminNavbarLinks />}
              {links}
            </div>
            {image !== undefined ? (
              <div
                className={classes.background}
                style={{ backgroundImage: "url(" + image + ")" }}
              />
            ) : null}
          </Drawer>
        {/* </Hidden> */}
        {/* <Hidden smDown implementation="css"> */}
          <Drawer
            anchor={this.props.rtlActive ? "right" : "left"}
            variant="permanent"
            open
            classes={{
              paper: classNames(classes.drawerPaper, {
                [classes.drawerPaperRTL]: this.props.rtlActive,
              }),
            }}
          >
            {brand}
            <div className={classes.sidebarWrapper}>{links}</div>
            {image !== undefined ? (
              <div
                className={classes.background}
                style={{ backgroundImage: "url(" + image + ")" }}
              />
            ) : null}
          </Drawer>
        {/* </Hidden> */}
      </div>
    );
  }
}


export default withStyles(useStyles, {withTheme: true})(Sidebar);
