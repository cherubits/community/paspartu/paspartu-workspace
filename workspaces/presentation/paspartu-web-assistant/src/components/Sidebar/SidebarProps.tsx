import { BackgroundColor } from '../../core/BackgroundColor';
import { WithStyles } from "@material-ui/core/styles";
import useStyles from "./SidebarStyle";

export interface SidebarProps extends WithStyles<typeof useStyles> {
  rtlActive?: boolean;
  handleDrawerToggle: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  color?: string;
  bgColor?: BackgroundColor;
  logo?: string;
  image?: string;
  logoText?: string;
  routes?: any[];
  open?: boolean;
}
