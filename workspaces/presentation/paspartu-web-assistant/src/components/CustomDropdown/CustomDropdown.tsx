import React, { ReactNode } from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// nodejs library to set properties for components

// @material-ui/core components
import { withStyles, WithStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Paper from "@material-ui/core/Paper";
import Grow from "@material-ui/core/Grow";
import Divider from "@material-ui/core/Divider";
import Icon from "@material-ui/core/Icon";
import Popper from "@material-ui/core/Popper";

// core components
import Button from "../../components/CustomButton/CustomButton";

import useStyles from "./CustomDropdownStyle";

export enum AccentColors {
  Black = "black",
  Primary =  "primary",
  Info =  "info",
  Success =  "success",
  Warning =  "warning",
  Danger =  "danger",
  Rose =  "rose"
}

export interface CustomDropdownProps extends WithStyles<typeof useStyles> {
  hoverColor: AccentColors;
  buttonText: ReactNode;
  buttonIcon: any;
  dropdownList: Array<any>;
  buttonProps: any;
  dropup?: boolean;
  dropdownHeader?: ReactNode;
  rtlActive?: boolean;
  caret?: boolean;
  left?: boolean;
  noLiPadding: boolean;
  // function that retuns the selected item
  onClick?: Function,
};

export interface CustomDropDownState {
    anchorEl: any;
}


class CustomDropdown extends React.Component<CustomDropdownProps, CustomDropDownState> {

  static defaultProps = {
    caret: true,
    hoverColor: AccentColors.Primary,
  }  

  constructor(props: CustomDropdownProps) {
    super(props);
    this.state = { anchorEl: null};
  }

  handleClick(event: Event) {
    if (this.state.anchorEl && this.state.anchorEl.contains(event.target)) {
      this.setState({anchorEl: null});
    } else {
      this.setState({anchorEl: event.target});;
    }
  }
  handleClose(param: string|boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null) {
    this.setState({anchorEl: null});
    if (this.props && this.props.onClick) {
      this.props.onClick(param);
    }
  }
  handleCloseAway(event: React.MouseEvent<Document, MouseEvent>) {
    if (this.state.anchorEl.contains(event.target)) {
      return;
    }
    this.setState({anchorEl: null});
  }

  render() {
    const {
      buttonText,
      buttonIcon,
      dropdownList,
      buttonProps,
      dropup,
      dropdownHeader,
      caret,
      hoverColor,
      left,
      rtlActive,
      noLiPadding,
      classes,
    } = this.props;
    const caretClasses = classNames({
      [classes.caret]: true,
      [classes.caretActive]: Boolean(this.state.anchorEl),
      [classes.caretRTL]: rtlActive,
    });
    const dropdownItem = classNames({
      [classes.dropdownItem]: true,
      [classes[`${hoverColor}Hover`]]: true,
      [classes.noLiPadding]: noLiPadding,
      [classes.dropdownItemRTL]: rtlActive,
    });
    let icon = null;
    switch (typeof buttonIcon) {
      case "object":
        icon = <this.props.buttonIcon className={classes.buttonIcon} />;
        break;
      case "string":
        icon = <Icon className={classes.buttonIcon}>{this.props.buttonIcon}</Icon>;
        break;
      default:
        icon = null;
        break;
    }
    return (
      <div>
        <div>
          <Button
            aria-label="Notifications"
            aria-owns={this.state.anchorEl ? "menu-list" : null}
            aria-haspopup="true"
            {...buttonProps}
            onClick={this.handleClick}
          >
            {icon}
            {buttonText !== undefined ? buttonText : null}
            {caret ? <b className={caretClasses} /> : null}
          </Button>
        </div>
        <Popper
          open={Boolean(this.state.anchorEl)}
          anchorEl={this.state.anchorEl}
          transition
          disablePortal
          placement={
            dropup
              ? left
                ? "top-start"
                : "top"
              : left
              ? "bottom-start"
              : "bottom"
          }
          className={classNames({
            [classes.popperClose]: !this.state.anchorEl,
            [classes.popperResponsive]: true,
          })}
        >
          {() => (
            <Grow
              in={Boolean(this.state.anchorEl)}
              style={
                dropup
                  ? { transformOrigin: "0 100% 0" }
                  : { transformOrigin: "0 0 0" }
              }
            >
              <Paper className={classes.dropdown}>
                <ClickAwayListener onClickAway={this.handleCloseAway}>
                  <MenuList role="menu" className={classes.menuList}>
                    {dropdownHeader !== undefined ? (
                      <MenuItem
                        onClick={() => this.handleClose(dropdownHeader)}
                        className={classes.dropdownHeader}
                      >
                        {dropdownHeader}
                      </MenuItem>
                    ) : null}
                    {dropdownList.map((prop, key) => {
                      if (prop.divider) {
                        return (
                          <Divider
                            key={key}
                            onClick={() => this.handleClose("divider")}
                            className={classes.dropdownDividerItem}
                          />
                        );
                      }
                      return (
                        <MenuItem
                          key={key}
                          onClick={() => this.handleClose(prop)}
                          className={dropdownItem}
                        >
                          {prop}
                        </MenuItem>
                      );
                    })}
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
      </div>
    );
  }
}

export default withStyles(useStyles, {withTheme: true})(CustomDropdown);