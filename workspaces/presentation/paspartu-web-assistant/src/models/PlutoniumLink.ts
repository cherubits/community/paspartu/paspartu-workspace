
export interface PlutoniumMenu {
  label: string;
  link: string;
  help?: string;
}
