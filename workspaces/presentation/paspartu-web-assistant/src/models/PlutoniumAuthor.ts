
export interface PlutoniumAuthor {

  /**
   * Copyright start from.
   */
  copyRightFrom: Date;

  /**
   * Website url of author
   */
  authorSiteLink: string;

  /**
   * Author organization or individual name
   */
  authorName: string;

  /**
   * Motto text
   */
  motto?: string;
}
