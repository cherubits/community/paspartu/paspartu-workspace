import React from "react";
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";
// @material-ui/core components
import { withStyles, WithStyles } from "@material-ui/core/styles";
// core components
import FixedPlugin from "../../components/FixedPlugin/FixedPlugin";


import useStyles from "../AdminLayout/AdminLayout.style";


export interface SplashLayoutProps extends WithStyles<typeof useStyles> {
  bgImage?: any;
}


// const switchRoutes = (
//   <Switch>
//     {routes.map((prop, key) => {
//       if (prop.layout === "/admin") {
//         return (
//           <Route
//             path={prop.layout + prop.path}
//             component={prop.component}
//             key={key}
//           />
//         );
//       }
//       return null;
//     })}
//     <Redirect from="/admin" to="/admin/dashboard" />
//   </Switch>
// );

export interface SplashLayoutState {
  image?: any;
  color?: any;
  fixedClasses?: string;
  mobileOpen?: boolean;

}

class SplashLayout extends React.Component<SplashLayoutProps, SplashLayoutState> {

  private ps: any;
  private mainPanel: any;

  constructor(props: SplashLayoutProps) {
    super(props);
    this.state = {
      image: props.bgImage,
      color: 'blue',
      fixedClasses: 'dropdown show',
      mobileOpen: false
    };

    this.mainPanel = React.createRef();

    this.handleColorClick = this.handleColorClick.bind(this);
    this.handleDrawerToggle = this.handleDrawerToggle.bind(this);
    this.handleFixedClick = this.handleFixedClick.bind(this);
    this.handleImageClick = this.handleImageClick.bind(this);
    this.resizeFunction = this.resizeFunction.bind(this);
  }

  handleImageClick(image: any) {
    this.setState({image: image});
  }

  handleColorClick(color: any) {
    this.setState({color: color});
  }
  handleFixedClick() {
    if (this.state.fixedClasses === "dropdown") {
      this.setState({fixedClasses: "dropdown show"});
    } else {
      this.setState({fixedClasses: "dropdown"});
    }
  }
  handleDrawerToggle() {
    this.setState({mobileOpen: !this.state.mobileOpen});
  }
  getRoute() {
    return window.location.pathname !== "/admin/maps";
  }
  resizeFunction() {
    if (window.innerWidth >= 960) {
      this.setState({mobileOpen: this.state.mobileOpen});
    }
  }

  componentDidMount() {
    if (navigator.platform.indexOf("Win") > -1) {
      this.ps = new PerfectScrollbar(this.mainPanel.current, {
        suppressScrollX: true,
        suppressScrollY: false,
      });
      document.body.style.overflow = "hidden";
    }
    window.addEventListener("resize", this.resizeFunction);
    // Specify how to clean up after this effect:
    return function cleanup() {

    };
  }
  componentWillUnmount() {
    if (navigator.platform.indexOf("Win") > -1) {
      this.ps.destroy();
    }
    window.removeEventListener("resize", this.resizeFunction);
  }

  render() {
    // styles
    const {classes} = this.props;
    // ref to help us initialize PerfectScrollbar on windows devices
    // states and functions


    return (
      <div className={classes.wrapper}>
        {/* <Sidebar
          routes={routes}
          logoText={"Creative Tim"}
          logo={logo}
          image={this.state.image}
          handleDrawerToggle={this.handleDrawerToggle}
          open={this.state.mobileOpen}
          color={this.state.color}
          {...rest}
        /> */}
        <div className={classes.mainPanel} ref={this.mainPanel}>
          {/* <Navbar
            routes={routes}
            handleDrawerToggle={this.handleDrawerToggle}
            {...rest}
          /> */}
          <div>Home</div>
          <FixedPlugin
            handleImageClick={this.handleImageClick}
            handleColorClick={this.handleColorClick}
            bgColor={this.state.color}
            bgImage={this.state.image}
            handleFixedClick={this.handleFixedClick}
            fixedClasses={this.state.fixedClasses}
          />
        </div>
      </div>
    );
  }
}
export default withStyles(useStyles, {withTheme: true})(SplashLayout);
