// const switchRoutes = (
//   <Switch>
//     {routes.map((prop, key) => {
//       if (prop.layout === "/admin") {
//         return (
//           <Route
//             path={prop.layout + prop.path}
//             component={prop.component}
//             key={key}
//           />
//         );
//       }
//       return null;
//     })}
//     <Redirect from="/admin" to="/admin/dashboard" />
//   </Switch>
// );

import { BackgroundColor } from "../../core/BackgroundColor";



export interface AdminLayoutState {
  image?: any;
  color?: BackgroundColor;
  fixedClasses?: string;
  mobileOpen?: boolean;

}
