import { WithStyles } from "@material-ui/core/styles";
import useStyles from "./AdminLayout.style";


export interface AdminLayoutProps extends WithStyles<typeof useStyles> {
  bgImage?: any;
}
