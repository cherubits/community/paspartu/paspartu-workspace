import AdminLayout from './AdminLayout';
import { AdminLayoutProps } from './AdminLayout.props';

export default {
  title: 'Layouts/AdminLayout',
  component: AdminLayout,
  argTypes: {
    bgImage: {
      name: 'Background image',
      description: 'Image displayed in the background.',
      type: { name: 'object', required: false },
      defaultValue: null,
      control: {
        type: 'object'
      }
    },
  },
};

export const Primary = (args: AdminLayoutProps) => <AdminLayout {...args}></AdminLayout>;

