import React from "react";
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";
// @material-ui/core components
import { withStyles } from "@material-ui/core/styles";
// core components
import Navbar from "../../components/Navbars/Navbar";
import Footer from "../../components/Footer/Footer";
import Sidebar from "../../components/Sidebar/Sidebar";
import FixedPlugin from "../../components/FixedPlugin/FixedPlugin";

import routes from "../../routes";

import useStyles from "./AdminLayout.style";

import logo from "../../assets/img/reactlogo.png";
import { AdminLayoutState } from "./AdminLayout.state";
import { AdminLayoutProps } from "./AdminLayout.props";
import LayoutOutlet from "../LayoutOutlet";
import { BackgroundColor } from "../../core/BackgroundColor";

/**
 * Layout for administration
 */
class AdminLayout extends React.Component<AdminLayoutProps, AdminLayoutState> {

  private ps: any;
  private mainPanel: any;

  constructor(props: AdminLayoutProps) {
    super(props);
    this.state = {
      image: props.bgImage,
      color: BackgroundColor.Blue,
      fixedClasses: 'dropdown show',
      mobileOpen: false
    };

    this.mainPanel = React.createRef();

    this.handleColorClick = this.handleColorClick.bind(this);
    this.handleDrawerToggle = this.handleDrawerToggle.bind(this);
    this.handleFixedClick = this.handleFixedClick.bind(this);
    this.handleImageClick = this.handleImageClick.bind(this);
    this.resizeFunction = this.resizeFunction.bind(this);
  }

  handleImageClick(image: any) {
    this.setState({ image: image });
  }

  handleColorClick(color: any) {
    this.setState({ color: color });
  }
  handleFixedClick() {
    if (this.state.fixedClasses === "dropdown") {
      this.setState({ fixedClasses: "dropdown show" });
    } else {
      this.setState({ fixedClasses: "dropdown" });
    }
  }
  handleDrawerToggle() {
    this.setState({ mobileOpen: !this.state.mobileOpen });
  }

  resizeFunction() {
    if (window.innerWidth >= 960) {
      this.setState({ mobileOpen: this.state.mobileOpen });
    }
  }

  componentDidMount() {
    if (navigator.platform.indexOf("Win") > -1) {
      this.ps = new PerfectScrollbar(this.mainPanel.current, {
        suppressScrollX: true,
        suppressScrollY: false,
      });
      document.body.style.overflow = "hidden";
    }
    window.addEventListener("resize", this.resizeFunction);

  }
  componentWillUnmount() {
    if (navigator.platform.indexOf("Win") > -1) {
      this.ps.destroy();
    }
    window.removeEventListener("resize", this.resizeFunction);
  }

  get route() {
    return window.location.pathname !== "/admin/maps";
  }

  render() {
    // styles
    const { classes, ...rest } = this.props;
    // ref to help us initialize PerfectScrollbar on windows devices
    // states and functions


    return (
      <div className={classes.wrapper}>
        {/* <Router> */}
        <Sidebar
          routes={routes}
          logoText={"Creative Tim"}
          logo={logo}
          image={this.state.image}
          handleDrawerToggle={this.handleDrawerToggle}
          open={this.state.mobileOpen}
          color={this.state.color}
          {...rest} />
        <div className={classes.mainPanel} ref={this.mainPanel}>
          <Navbar
            routes={routes}
            handleDrawerToggle={this.handleDrawerToggle}
            {...rest}
          />
          {this.route ? (
            <div className={classes.content}>
              <div className={classes.container}>
                <LayoutOutlet indexPath="/dashboard" rolePath="/admin" routes={routes} />
              </div>
            </div>
          ) : (
            <div className={classes.map}>
              <LayoutOutlet indexPath="/dashboard" rolePath="/admin" routes={routes} />
            </div>
          )}

          {this.route ? <Footer /> : null}

          <FixedPlugin
            handleImageClick={this.handleImageClick}
            handleColorClick={this.handleColorClick}
            bgColor={this.state.color}
            bgImage={this.state.image}
            handleFixedClick={this.handleFixedClick}
            fixedClasses={this.state.fixedClasses}
          />
        </div>
        {/* </Router> */}
      </div>
    );
  }
}
export default withStyles(useStyles, { withTheme: true })(AdminLayout);
