import React from "react";
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";
// @material-ui/core components
import { WithStyles, withStyles } from "@material-ui/core/styles";
// core components
import Navbar from "../../components/Navbars/Navbar";
import Footer from "../../components/Footer/Footer";
import Sidebar from "../../components/Sidebar/Sidebar";
import FixedPlugin from "../../components/FixedPlugin/FixedPlugin";

import routes from "../../routes";

import useStyles from "./RtlLayout.style";

import logo from "../../assets/img/reactlogo.png";
import LayoutOutlet from "../LayoutOutlet";

export interface RLTLayoutProps extends WithStyles<typeof useStyles> {
  bgImage?: any;
}


export interface RLTLayoutState {
  image?: any;
  color?: any;
  fixedClasses?: string;
  mobileOpen?: boolean;

}

// const switchRoutes = (
//   <Switch>
//     {routes.map((prop, key) => {
//       if (prop.layout === "/rtl") {
//         return (
//           <Route
//             path={prop.layout + prop.path}
//             component={prop.component}
//             key={key}
//           />
//         );
//       }
//       return null;
//     })}
//     <Redirect from="/rtl" to="/rtl/rtl-page" />
//   </Switch>
// );


class RTLLayout extends React.Component<RLTLayoutProps,RLTLayoutState> {

  private ps: any;
  private mainPanel: any;

  constructor(props: RLTLayoutProps) {
    super(props);
    this.state = {
      image: props.bgImage,
      color: 'blue',
      fixedClasses: 'dropdown show',
      mobileOpen: false
    };

    this.mainPanel = React.createRef();

    this.handleColorClick = this.handleColorClick.bind(this);
    this.handleDrawerToggle = this.handleDrawerToggle.bind(this);
    this.handleFixedClick = this.handleFixedClick.bind(this);
    this.handleImageClick = this.handleImageClick.bind(this);
    this.resizeFunction = this.resizeFunction.bind(this);
  }

  handleImageClick(image: any) {
    this.setState({image: image});
  }

  handleColorClick(color: any) {
    this.setState({color: color});
  }
  handleFixedClick() {
    if (this.state.fixedClasses === "dropdown") {
      this.setState({fixedClasses: "dropdown show"});
    } else {
      this.setState({fixedClasses: "dropdown"});
    }
  }
  handleDrawerToggle() {
    this.setState({mobileOpen: !this.state.mobileOpen});
  }
  get route() {
    return window.location.pathname !== "/admin/maps";
  }
  resizeFunction() {
    if (window.innerWidth >= 960) {
      this.setState({mobileOpen: this.state.mobileOpen});
    }
  }

  componentDidMount() {
    if (navigator.platform.indexOf("Win") > -1) {
      this.ps = new PerfectScrollbar(this.mainPanel.current, {
        suppressScrollX: true,
        suppressScrollY: false,
      });
      document.body.style.overflow = "hidden";
    }
    window.addEventListener("resize", this.resizeFunction);
    // Specify how to clean up after this effect:
    return function cleanup() {

    };
  }
  componentWillUnmount() {
    if (navigator.platform.indexOf("Win") > -1) {
      this.ps.destroy();
    }
    window.removeEventListener("resize", this.resizeFunction);
  }

  render() {
    const { classes, ...rest } = this.props;

    return (
      <div className={classes.wrapper}>
        <Sidebar
          logoText={"الإبداعية تيم"}
          logo={logo}
          image={this.state.image}
          handleDrawerToggle={this.handleDrawerToggle}
          open={this.state.mobileOpen}
          color={this.state.color}
          rtlActive
          {...rest}        />
        <div className={classes.mainPanel} ref={this.mainPanel}>
          <Navbar
            routes={routes}
            handleDrawerToggle={this.handleDrawerToggle}
            rtlActive
            {...rest}
          />
          {this.route ? (
            <div className={classes.content}>
              <div className={classes.container}>
                <LayoutOutlet indexPath="/dashboard" rolePath="/admin" routes={routes} />
              </div>
            </div>
          ) : (
            <div className={classes.map}>
              <LayoutOutlet indexPath="/dashboard" rolePath="/admin" routes={routes} />
            </div>
          )}

          {this.route ? <Footer /> : null}

          <FixedPlugin
            handleImageClick={this.handleImageClick}
            handleColorClick={this.handleColorClick}
            bgColor={this.state.color}
            bgImage={this.state.image}
            handleFixedClick={this.handleFixedClick}
            fixedClasses={this.state.fixedClasses}
            rtlActive
          />
        </div>
      </div>
    );
  }
}

export default withStyles(useStyles, {withTheme: true})(RTLLayout);
