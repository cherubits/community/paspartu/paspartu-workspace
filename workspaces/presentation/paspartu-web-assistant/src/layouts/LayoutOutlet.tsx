import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';

export interface LayoutOutletProps {
    routes: Array<any>;
    rolePath: string;
    indexPath: string;
}

class LayoutOutlet extends React.Component<LayoutOutletProps> {

    render() {
        const { routes, rolePath, indexPath } = this.props;
        return <Routes>
            {routes.map((prop, key) => {
                if (prop.layout === rolePath) {
                    return (
                        <Route
                            path={prop.layout + prop.path}
                            element={prop.component}
                            key={key}
                        />
                    );
                }
                return null;
            })}
            <Route path={rolePath} element={<Navigate replace to={`${rolePath}${indexPath}`} />} />
        </Routes>;
    }
}

export default LayoutOutlet;
