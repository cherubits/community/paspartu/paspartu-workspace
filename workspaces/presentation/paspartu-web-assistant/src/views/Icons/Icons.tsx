/*eslint-disable*/
import React from "react";
// @material-ui/core components
import { makeStyles, withStyles, WithStyles } from "@material-ui/core/styles";
import Hidden from "@material-ui/core/Hidden";
// core components
import GridItem from "../../components/Grid/GridItem";
import GridContainer from "../../components/Grid/GridContainer";
import Card from "../../components/Card/Card";
import CardHeader from "../../components/Card/CardHeader";
import CardBody from "../../components/Card/CardBody";

import useStyles from "./IconsStyle";
import { CardHeaderColor } from "../../core/CardHeaderColor";

export interface IconsProps extends WithStyles<typeof useStyles> {

}


 class Icons extends React.Component<IconsProps> {
   constructor(props: IconsProps) {
     super(props);
   }

   render() {
    const {classes} = this.props;
    return (
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Card plain>
            <CardHeader plain color={CardHeaderColor.PrimaryCardHeader}>
              <h4 className={classes.cardTitleWhite}>Material Design Icons</h4>
              <p className={classes.cardCategoryWhite}>
                Handcrafted by our friends from{" "}
                <a
                  href="https://design.google.com/icons/?ref=creativetime"
                  target="_blank"
                >
                  Google
                </a>
              </p>
            </CardHeader>
            <CardBody>
              {/* <Hidden only={["sm", "xs"]}> */}
                <iframe
                  className={classes.iframe}
                  src="https://material.io/icons/"
                  title="Icons iframe"
                >
                  <p>Your browser does not support iframes.</p>
                </iframe>
              {/* </Hidden> */}
              {/* <Hidden only={["lg", "md"]}> */}
                <GridItem xs={12} sm={12} md={6}>
                  <h5>
                    The icons are visible on Desktop mode inside an iframe. Since
                    the iframe is not working on Mobile and Tablets please visit
                    the icons on their original page on Google. Check the
                    <a
                      href="https://design.google.com/icons/?ref=creativetime"
                      target="_blank"
                    >
                      Material Icons
                    </a>
                  </h5>
                </GridItem>
              {/* </Hidden> */}
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    );
  }
 }
export default withStyles(useStyles, {withTheme: true})(Icons);
