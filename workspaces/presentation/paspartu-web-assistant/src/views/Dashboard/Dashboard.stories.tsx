import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import Dashboard from './Dashboard';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Views/Dashboard',
  component: Dashboard,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as ComponentMeta<typeof Dashboard>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof Dashboard> = (args) => <Dashboard {...args} />;

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
  // primary: true,
  // label: 'Dashboard',
};

export const Secondary = Template.bind({});
Secondary.args = {
  // label: 'Dashboard',
};

export const Large = Template.bind({});
Large.args = {
  // size: 'large',
  // label: 'Dashboard',
};

export const Small = Template.bind({});
Small.args = {
  // size: 'small',
  // label: 'Dashboard',
};
