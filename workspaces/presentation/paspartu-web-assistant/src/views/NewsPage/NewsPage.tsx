import React from 'react';
// import {Masonry} from '@material-ui/core/Maso'

export interface NewsPageProps {

}

export interface NewsPageState {
  someKey: string;
}

class NewsPage extends React.Component<NewsPageProps, NewsPageState> {
  constructor(props: NewsPageProps) {
    super(props);
    this.state = {
      someKey: 'someValue'
    };
  }

  // render() {
  //   return <Masonry columns={4} spacing={1}>
  //   {heights.map((height, index) => (
  //     <MasonryItem key={index}>
  //       <Item sx={{ height }}>{index + 1}</Item>
  //     </MasonryItem>
  //   ))}
  // </Masonry>;
  // }

  componentDidMount() {
    this.setState({
      someKey: 'otherValue'
    });
  }
}

export default NewsPage;
