/*eslint-disable*/
import React from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles, WithStyles, withStyles } from "@material-ui/core/styles";
// @material-ui/icons
import AddAlert from "@material-ui/icons/AddAlert";
// core components
import GridItem from "../../components/Grid/GridItem";
import GridContainer from "../../components/Grid/GridContainer";
import Button from "../../components/CustomButton/CustomButton";
import SnackbarContent from "../../components/Snackbar/SnackbarContent";
import Snackbar from "../../components/Snackbar/Snackbar";
import Card from "../../components/Card/Card";
import CardHeader from "../../components/Card/CardHeader";
import CardBody from "../../components/Card/CardBody";


import useStyles from './NotificationsStyle';
import { SemanticColor } from "../../core/SemanticColor";
import { CardHeaderColor } from "../../core/CardHeaderColor";
import { SnackbarPlace } from "../../core/SnackbarPlace";

export interface NotificationsProps extends WithStyles<typeof useStyles> {

}

export interface NotificationsState {
  tl?: boolean;
  tc?: boolean;
  tr?: boolean;
  bl?: boolean;
  bc?: boolean;
  br?: boolean;
}

class Notifications extends React.Component<NotificationsProps, NotificationsState> {
  constructor(props: NotificationsProps) {
    super(props);
    this.state = {
      tl: true
    }
  }

  // componentDidMount() {
  //   ChatAPI.subscribeToFriendStatus(
  //     this.props.friend.id,
  //     this.handleStatusChange
  //   );
  // }
  componentWillUnmount() {
    var id = window.setTimeout(() => {}, 0);
        while (id--) {
          window.clearTimeout(id);
        }
  }
  // handleStatusChange(status) {
  //   this.setState({
  //     isOnline: status.isOnline
  //   });
  // }


  showNotification(place: SnackbarPlace) {
    switch (place) {
      case SnackbarPlace.TL:
        if (!this.state.tl) {
          this.setState({tl: true});
          setTimeout( () => {
            this.setState({tl: false});
          }, 6000);
        }
        break;
      case SnackbarPlace.TC:
        if (!this.state.tc) {
          this.setState({tc: true});
          setTimeout( () => {
            this.setState({tc: false});
          }, 6000);
        }
        break;
      case SnackbarPlace.TR:
        if (!this.state.tr) {
          this.setState({tr: true});
          setTimeout( () => {
            this.setState({tr: false});
          }, 6000);
        }
        break;
      case SnackbarPlace.BL:
        if (!this.state.bl) {
          this.setState({bl: true});
          setTimeout( () => {
            this.setState({bl: false});
          }, 6000);
        }
        break;
      case SnackbarPlace.BC:
        if (!this.state.bc) {
          this.setState({bc: true});
          setTimeout( () => {
            this.setState({bc: false});
          }, 6000);
        }
        break;
      case SnackbarPlace.BR:
        if (!this.state.br) {
          this.setState({br: true});
          setTimeout( () => {
            this.setState({br: false});
          }, 6000);
        }
        break;
      default:
        break;
    }
  }
  render() {
    const {classes} = this.props;


    return (
      <Card>
        <CardHeader color={CardHeaderColor.PrimaryCardHeader}>
          <h4 className={classes.cardTitleWhite}>Notifications</h4>
          <p className={classes.cardCategoryWhite}>
            Handcrafted by our friends from{" "}
            <a
              target="_blank"
              href="https://material-ui-next.com/?ref=creativetime"
            >
              Material UI
            </a>{" "}
            and styled by{" "}
            <a
              target="_blank"
              href="https://www.creative-tim.com/?ref=mdr-notifications-page"
            >
              Creative Tim
            </a>
            . Please checkout the{" "}
            <a href="#pablo" target="_blank">
              full documentation
            </a>
            .
          </p>
        </CardHeader>
        <CardBody>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <h5>Notifications Style</h5>
              <br />
              <SnackbarContent message={"This is a plain notification"} />
              <SnackbarContent
                message={"This is a notification with close button."}
                close
              />
              <SnackbarContent
                message={"This is a notification with close button and icon."}
                close
                icon={AddAlert}
              />
              <SnackbarContent
                message={
                  "This is a notification with close button and icon and have many lines. You can see that the icon and the close button are always vertically aligned. This is a beautiful notification. So you don't have to worry about the style."
                }
                close
                icon={AddAlert}
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <h5>Notifications States</h5>
              <br />
              <SnackbarContent
                message={
                  'INFO - This is a regular notification made with color="info"'
                }
                close
                color={SemanticColor.Info}
              />
              <SnackbarContent
                message={
                  'SUCCESS - This is a regular notification made with color="success"'
                }
                close
                color={SemanticColor.Success}
              />
              <SnackbarContent
                message={
                  'WARNING - This is a regular notification made with color="warning"'
                }
                close
                color={SemanticColor.Warning}
              />
              <SnackbarContent
                message={
                  'DANGER - This is a regular notification made with color="danger"'
                }
                close
                color={SemanticColor.Danger}
              />
              <SnackbarContent
                message={
                  'PRIMARY - This is a regular notification made with color="primary"'
                }
                close
                color={SemanticColor.Primary}
              />
            </GridItem>
          </GridContainer>
          <br />
          <br />
          <GridContainer justify="center">
            <GridItem xs={12} sm={12} md={6} style={{ textAlign: "center" }}>
              <h5>
                Notifications Places
                <br />
                <small>Click to view notifications</small>
              </h5>
            </GridItem>
          </GridContainer>
          <GridContainer justify="center">
            <GridItem xs={12} sm={12} md={10} lg={8}>
              <GridContainer>
                <GridItem xs={12} sm={12} md={4}>
                  <Button
                    fullWidth
                    color={SemanticColor.Primary}
                    onClick={() => this.showNotification(SnackbarPlace.TL)}
                  >
                    Top Left
                  </Button>
                  <Snackbar
                    place={SnackbarPlace.TL}
                    color={SemanticColor.Info}
                    icon={AddAlert}
                    message="Welcome to MATERIAL DASHBOARD React - a beautiful freebie for every web developer."
                    open={this.state.tl}
                    closeNotification={() => this.setState({tl: false})}
                    close
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <Button
                    fullWidth
                    color={SemanticColor.Primary}
                    onClick={() => this.showNotification(SnackbarPlace.TC)}
                  >
                    Top Center
                  </Button>
                  <Snackbar
                    place={SnackbarPlace.TC}
                    color={SemanticColor.Info}
                    icon={AddAlert}
                    message="Welcome to MATERIAL DASHBOARD React - a beautiful freebie for every web developer."
                    open={this.state.tc}
                    closeNotification={() => this.setState({tc: false})}
                    close
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <Button
                    fullWidth
                    color={SemanticColor.Primary}
                    onClick={() => this.showNotification(SnackbarPlace.TR)}
                  >
                    Top Right
                  </Button>
                  <Snackbar
                    place={SnackbarPlace.TR}
                    color={SemanticColor.Info}
                    icon={AddAlert}
                    message="Welcome to MATERIAL DASHBOARD React - a beautiful freebie for every web developer."
                    open={this.state.tr}
                    closeNotification={() => this.setState({tr: false})}
                    close
                  />
                </GridItem>
              </GridContainer>
            </GridItem>
          </GridContainer>
          <GridContainer justify={"center"}>
            <GridItem xs={12} sm={12} md={10} lg={8}>
              <GridContainer>
                <GridItem xs={12} sm={12} md={4}>
                  <Button
                    fullWidth
                    color={SemanticColor.Primary}
                    onClick={() => this.showNotification(SnackbarPlace.TR)}
                  >
                    Bottom Left
                  </Button>
                  <Snackbar
                    place={SnackbarPlace.BL}
                    color={SemanticColor.Info}
                    icon={AddAlert}
                    message="Welcome to MATERIAL DASHBOARD React - a beautiful freebie for every web developer."
                    open={this.state.bl}
                    closeNotification={() => this.setState({bl: false})}
                    close
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <Button
                    fullWidth
                    color={SemanticColor.Primary}
                    onClick={() => this.showNotification(SnackbarPlace.BC)}
                  >
                    Bottom Center
                  </Button>
                  <Snackbar
                    place={SnackbarPlace.BC}
                    color={SemanticColor.Info}
                    icon={AddAlert}
                    message="Welcome to MATERIAL DASHBOARD React - a beautiful freebie for every web developer."
                    open={this.state.bc}
                    closeNotification={() => this.setState({bc: false})}
                    close
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <Button
                    fullWidth
                    color={SemanticColor.Primary}
                    onClick={() => this.showNotification(SnackbarPlace.BR)}
                  >
                    Bottom Right
                  </Button>
                  <Snackbar
                    place={SnackbarPlace.BR}
                    color={SemanticColor.Info}
                    icon={AddAlert}
                    message="Welcome to MATERIAL DASHBOARD React - a beautiful freebie for every web developer."
                    open={this.state.br}
                    closeNotification={() => this.setState({br: false})}
                    close
                  />
                </GridItem>
              </GridContainer>
            </GridItem>
          </GridContainer>
        </CardBody>
      </Card>
    );
  }
}

export default withStyles(useStyles, {withTheme: true})(Notifications);
