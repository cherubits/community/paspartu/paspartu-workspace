# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.1.2](https://gitlab.com/cherubits/community/paspartu/paspartu-workspace/compare/@lordoftheflies/paspartu-web-assistant@0.1.1...@lordoftheflies/paspartu-web-assistant@0.1.2) (2022-06-05)

**Note:** Version bump only for package @lordoftheflies/paspartu-web-assistant





## 0.1.1 (2022-06-05)

**Note:** Version bump only for package @lordoftheflies/paspartu-web-assistant
