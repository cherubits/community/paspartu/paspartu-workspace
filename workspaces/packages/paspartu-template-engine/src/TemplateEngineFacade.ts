import { inject, injectable, named } from "inversify";
import { DocumentData } from "./DocumentData";
import { DocumentRenderOptions } from "./DocumentRenderOptions";
import { DocumentTemplate } from "./DocumentTemplate";
import { renderPdf } from "./renderPdf";
import { PathLike, readFileSync, writeFileSync } from "fs";
import Handlebars from 'handlebars';
import { launch } from 'puppeteer';
import { capitalize, endsWith, upperCase } from "lodash";
import { format } from "date-fns";
import { InvoiceIntl } from "InvoiceIntl";
import $symbols from './symbols';
import { TemplateRenderingError } from "./errors";
import accounting from "accounting";
// import QrCode from 'qrcode';
// import AsyncHelpers from "async-helpers-typescript";
import qrcode, { encodeAsBase64, encodeAsImage } from 'tc-qrcode';
import logger from './logger';


@injectable()
export class TemplateEngineFacade {

    @inject($symbols.InvoiceIntl)
    @named('hu')
    private intl!: InvoiceIntl;

    constructor() {

    }

    async initializeTemplateFunction<T>(path: PathLike, encoding: BufferEncoding): Promise<HandlebarsTemplateDelegate<T>> {

        Handlebars.registerHelper({
            logo: (p: string) => {
                const result = `<img src="data:image/jpeg;base64,${readFileSync(p).toString('base64')}" alt="Company logo" class="logo" />`;
                return new Handlebars.SafeString(result);
            },
            formatDate: (p: Date) => {
                const result = `<span>${format(p, 'P')}</span>`;
                return new Handlebars.SafeString(result);
            },
            translate: (p: string) => {
                const result = (this.intl && p in this.intl) ? Reflect.get(this.intl, p) : p;
                return new Handlebars.SafeString(`<span>${result}</span>`)
            },
            capitalize: (p: string) => {
                return new Handlebars.SafeString(capitalize(p));
            },
            upperCase: (p: string) => {
                return new Handlebars.SafeString(upperCase(p));
            },
            formatMoney: (p: number) => {
                const result = accounting.formatMoney(p, { symbol: "HUF", format: "%v %s" }); // 5,318,008.00 GBP
                return new Handlebars.SafeString(result);
            },
            formatBankAccount: (p: string) => {
                const t1 = p.substring(0, 7);
                const t2 = p.substring(8, 15);
                const t3 = p.substring(16, 23);
                const result = `${t1}-${t2}-${t3}`;
                return new Handlebars.SafeString(result);
            },
            formatTaxNo: (p: string) => {
                const t1 = p.substring(0, 7);
                const t2 = p.substring(8);
                const t3 = p.substring(9, 10);
                const result = `${t1}-${t2}-${t3}`;
                return new Handlebars.SafeString(result);
            },
            qrCode: (p: string) => {
                try {
                    // const base64 = encodeAsBase64(p);
                    // const result = `<img id="qr" alt="QR code" class="qr" />`;
                    const imp = '<script src="https://cdn.jsdelivr.net/npm/tc-qrcode/tc-qrcode.min.js"></script>';
                    const script = `<script type="application/javascript">console.log("qr imported"); console.log("qr rendered"); document.getElementById("qrContainer").appendChild(TCQrcode.encodeAsImage('${p}')); </script>`
                    // return new Handlebars.parseWithoutProcessing(base64));
                    return new Handlebars.SafeString(imp + script);
                } catch (ex) {
                    console.error(ex);
                    return new Handlebars.SafeString('eeor');
                }
            }

        });
        // const hb = new AsyncHelpers({
        // });
        // hb.helper('qrCode', async (code: string, cb: any) => {
        //     try {
        //         const p = await QrCode.toBuffer(code)
        //         const result = `<img src="data:image/jpeg;base64,${p.toString('base64')}" alt="QR code" class="qr" />`;
        //         await cb(null, result);
        //         return result;
        //     } catch (err) {
        //         console.error(err);
        //         return code;
        //     }
        // });
        const templateHtml = readFileSync(path, encoding);
        return Handlebars.compile(templateHtml, { knownHelpers: { logo: true } });
    }

    async render<T>($template: DocumentTemplate<T>, $data: T, $options?: DocumentRenderOptions) {
        try {
            const html = (await this.initializeTemplateFunction($template.templatePath, $template.templateEncoding))($data);
            //let milis = new Date().getTime();
            //const pdfPath = $template.outputTitleTemplateString($data, { timestamp: milis });
            const options = Object.assign({}, $options, {
                width: '1230px',
                headerTemplate: "<p></p>",
                footerTemplate: "<p></p>",
                displayHeaderFooter: true,
                margin: {
                    top: "10px",
                    bottom: "30px"
                },
                printBackground: false,
                // path: pdfPath,
            });

            const browser = await launch({
                args: ['--no-sandbox'],
                headless: true,
            });

            const page = await browser.newPage();

            await page.goto(`data:text/html;charset=UTF-8,${html}`, {
                waitUntil: 'networkidle0',
                timeout: 10000
            });

            //writeFileSync('kacsa.html', await page.content());
            writeFileSync(options.path, await page.pdf(options));
            logger.info(`render ${process.cwd()}/${options.path}`);
            await browser.close();
        } catch (ex) {
            throw new TemplateRenderingError({
                templatePath: $template.templatePath,
                cause: ex as Error
            });
        } finally {
            logger.debug(`executed`);
        }
    }

}
