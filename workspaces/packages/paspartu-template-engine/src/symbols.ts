export default {
    TemplateEngineFacade: Symbol.for('TemplateEngineFacade'),
    InvoiceIntl: Symbol.for("InvoiceIntl"),
    CurrentLocale: Symbol.for("IntlLocale"),
}
