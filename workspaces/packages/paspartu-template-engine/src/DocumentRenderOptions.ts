import { PathLike } from "fs";
import { DocumentMargin } from "./DocumentMargin";


export interface DocumentRenderOptions {
    width: string;
    headerTemplate?: string;
    footerTemplate?: string;
    displayHeaderFooter: boolean;
    margin: DocumentMargin;
    printBackground: boolean;
    path: string;
}
