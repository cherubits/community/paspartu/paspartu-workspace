
export interface DocumentMargin {
    top?: string;
    bottom?: string;
    left?: string;
    right?: string;
}
