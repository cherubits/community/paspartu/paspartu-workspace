import { DocumentData } from "./DocumentData";


export enum PaymentMethodType {
    CASH = 'PaymentMethodTypeCash',
    TRANSFER = 'PaymentMethodTypeTransfer',
}

export enum UnitType {
    PIECES = 'UnitPcs',
    HOURS = 'UnitHours',
}

export enum VatType {
    VAT27 = 'Vat27',
    HO = 'VatHo'
}

export interface BankTransferData {
    name: string;
    accountNumber: string;
}

export interface InvoiceItem {
    description: string;
    amount: number;
    unitPrice: number;
    netPrice: number;
    vat: VatType;
    vatPrice: number;
    grossPrice: number;
    unit?: UnitType;
    note?: string;
}

export interface Legal {
    name: string;
    headQuarters: string;
    taxNo: string;
    registrationNo: string;
}

export interface Invoice extends DocumentData {
    invoiceNo: string;
    customer: Legal;
    issuer: Legal;
    subtitle: string;
    items: Array<InvoiceItem>;
    paymentMethodType: PaymentMethodType;
    bankTransferData?: BankTransferData;
    completionDate: Date;
    dueDate: Date;
    creationDate: Date;
}

const addFn = (p: number, c: number) => p + c;

export function price(invoice: Invoice) {
    return {
        net: invoice.items.map((i: InvoiceItem) => i.netPrice).reduce(addFn, 0),
        gross: invoice.items.map((i: InvoiceItem) => i.grossPrice).reduce(addFn, 0),
        vat: invoice.items.map((i: InvoiceItem) => i.vatPrice).reduce(addFn, 0),
    }
}
