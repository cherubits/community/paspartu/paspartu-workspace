import 'reflect-metadata';
import 'dotenv/config';

import { Container } from "inversify";
import { DocumentRenderOptions } from "./DocumentRenderOptions";
import { TemplateEngineContainerModule } from "./TemplateEngineModule";
import $symbols from './symbols';
import { TemplateEngineFacade } from "./TemplateEngineFacade";
import { DocumentTemplate } from "./DocumentTemplate";
import { Invoice, InvoiceItem, Legal, PaymentMethodType, price, UnitType, VatType } from "./Invoice";
import { BasicDocumentTemplate } from "./BasicDocumentTemplate";
import { faker } from '@faker-js/faker';
import { InvoiceIntl } from 'InvoiceIntl';
import { random, range } from 'lodash';
import { randomInt } from 'crypto';
import logger from './logger';

const options: DocumentRenderOptions = {
    width: '1230px',
    headerTemplate: "<p></p>",
    footerTemplate: "<p></p>",
    displayHeaderFooter: false,
    margin: {
        top: "10px",
        bottom: "30px",
    },
    printBackground: true,
    path: 'kiskacsa.pdf'
};

const template: DocumentTemplate<{invoice: Invoice, intl: InvoiceIntl}> = new BasicDocumentTemplate("invoice");

export function createRandomIssuer(): Legal {
    return {
        name: faker.company.companyName(),
        headQuarters: faker.address.buildingNumber(),
        taxNo: faker.random.alphaNumeric(10),
        registrationNo: faker.random.alphaNumeric(10),
    };
}

export function createRandomInvoiceItem(): InvoiceItem {
    return {
        amount: faker.datatype.number({min: 0, max: 10}),
        description: faker.lorem.words(3),
        grossPrice: faker.datatype.number({min: 0, max: 100000}),
        netPrice: faker.datatype.number({min: 0, max: 100000}),
        unitPrice: faker.datatype.number({min: 0, max: 100000}),
        vat: VatType.VAT27,
        vatPrice: faker.datatype.number({min: 0, max: 100000}),
        note: faker.lorem.sentences(2),
        unit: UnitType.PIECES,
    };
}
export function createRandomInvoice(): Invoice {
    return {
        name: faker.random.alphaNumeric(10),
        customer: createRandomIssuer(),
        invoiceNo: faker.random.alphaNumeric(10),
        issuer: createRandomIssuer(),
        items: range(0, randomInt(10)).map((i: number) => createRandomInvoiceItem()),
        // items: range(0, 10).map(i => createRandomInvoiceItem()),
        subtitle: faker.lorem.text(),
        paymentMethodType: PaymentMethodType.TRANSFER,
        completionDate: new Date(),
        creationDate: new Date(),
        dueDate: new Date(),
        bankTransferData: {
            accountNumber: faker.finance.account(24),
            name: faker.finance.accountName(),
        }
    };
}

const i = createRandomInvoice();
// export default async () => {
try {
    let container = new Container();
    container.load(new TemplateEngineContainerModule());
    const templateEngineFacade: TemplateEngineFacade = container.get($symbols.TemplateEngineFacade);
    const invoiceIntl: InvoiceIntl = container.getNamed($symbols.InvoiceIntl, 'hu');
    templateEngineFacade.render(template, {
        invoice: i,
        price: price(i),
        intl: invoiceIntl,
        lookAndFeel: {
            logo: 'assets/images/cherubits-logo-icon.png'
        }
    }, options);

// }
} catch (ex) {
    console.log((ex as Error).message);
}
