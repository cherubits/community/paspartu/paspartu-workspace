import { compile as handlebarsCompile } from "handlebars";
import fs from "fs";
import path from "path";
import puppeteer from 'puppeteer';
import { readFileSync } from "fs";
import { DocumentTemplate } from "./DocumentTemplate";
import { DocumentData } from "./DocumentData";
import { DocumentRenderOptions } from "./DocumentRenderOptions";

export async function renderPdf($template: DocumentTemplate<DocumentData>, $data: DocumentData, $options?: DocumentRenderOptions) {

    const templateHtml = readFileSync($template.templatePath, $template.templateEncoding);
    const template = handlebarsCompile(templateHtml);
    const html = template($data);

    let milis = new Date().getTime();

    const pdfPath = $template.outputTitleTemplateString($data, { timestamp: milis });

    var options = Object.assign({}, {
        width: '1230px',
        headerTemplate: "<p>---header ---</p>",
        footerTemplate: "<p>--- footer ----</p>",
        displayHeaderFooter: true,
        margin: {
            top: "10px",
            bottom: "30px"
        },
        printBackground: true,
        path: pdfPath
    }, $options);

    const browser = await puppeteer.launch({
        args: ['--no-sandbox'],
        headless: true
    });

    const page = await browser.newPage();

    await page.goto(`data:text/html;charset=UTF-8,${html}`, {
        waitUntil: 'networkidle0'
    });

    await page.pdf(options);
    await browser.close();
}
