import { ContainerModule, interfaces } from "inversify";
import { TemplateEngineFacade } from "./TemplateEngineFacade";
import $symbols from "./symbols";
import { InvoiceIntl } from "./InvoiceIntl";

import InvoiceItlEn from "../assets/locales/invoice-en.json";
import InvoiceItlHu from "../assets/locales/invoice-hu.json";

export class TemplateEngineContainerModule extends ContainerModule {
    constructor() {
        super(
            (
                bind: interfaces.Bind,
                unbind: interfaces.Unbind,
                isBound: interfaces.IsBound,
                rebind: interfaces.Rebind,
                unbindAsync: interfaces.UnbindAsync,
                onActivation: interfaces.Container["onActivation"],
                onDeactivation: interfaces.Container["onDeactivation"]
            ) => {
                bind<string>($symbols.CurrentLocale)
                    .toConstantValue(process.env.PASPARTU_LOCALE ?? 'en');

                bind<InvoiceIntl>($symbols.InvoiceIntl)
                    .toConstantValue(InvoiceItlEn)
                    .whenTargetNamed('en');
                bind<InvoiceIntl>($symbols.InvoiceIntl)
                    .toConstantValue(InvoiceItlHu)
                    .whenTargetNamed('hu');

                bind<TemplateEngineFacade>($symbols.TemplateEngineFacade)
                    .to(TemplateEngineFacade);
            }
        );
    }
}
