import { PathLike } from "fs";
import path from "path";
import { DocumentData } from "./DocumentData";
import { DocumentTemplate } from "./DocumentTemplate";



export class BasicDocumentTemplate<Data> implements DocumentTemplate<Data> {
    documentType: string;
    templatePath: PathLike;
    templateEncoding: BufferEncoding = 'utf8';

    constructor(name: string) {
        this.documentType = name;
        this.templatePath = path.join(process.cwd(), 'assets', 'templates', `${name}.handlebars`);
    }

    outputTitleTemplateString = (data: Data, options: any) => `${this.documentType}-${options.timestamp}.pdf`;


}
