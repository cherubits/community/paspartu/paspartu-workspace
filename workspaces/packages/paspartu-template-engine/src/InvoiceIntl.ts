
export interface InvoiceIntl {
    LabelBankAccountNumber?: string;
    LabelBankName?: string;
    LabelPaymentMethodType?: string;
    LabelCompletionDate?: string;
    LabelCreationDate?: string;
    LabelDueDate?: string;
    ColumnDescription?: string;
    ColumnAmount?: string;
    ColumnUnitPrice?: string;
    ColumnNetPrice?: string;
    ColumnVat?: string;
    ColumnVatPrice?: string;
    ColumnGrossPrice?: string;
    LabelInvoiceNumber?: string;
    LabelSummary?: string;
    TitleCreatedBy?: string;
    LabelPage?: string;
    LabelTaxNumber?: string;
    TitleCustomer?: string;
    TitleElectronicInvoice?: string;
    SubtitleElectronicInvoice?: string;
    PaymentMethodTypeCash?: string;
    PaymentMethodTypeTransfer?: string;
    UnitPcs?: string;
    UnitHour?: string;
    VatHo?: string;
    Vat27?: string;
}
