import { PathLike } from "fs";

export interface TemplateRenderingErrorParams {
    templatePath: PathLike;
    cause: Error;
}

export class TemplateRenderingError extends Error {
    constructor(args: TemplateRenderingErrorParams) {
        super(`Failed to rendering template ${args.templatePath}, cause ${args.cause?.message}`);

        // Set the prototype explicitly.
        Object.setPrototypeOf(this, TemplateRenderingError.prototype);
    }

    sayHello() {
        return "hello " + this.message;
    }
}
