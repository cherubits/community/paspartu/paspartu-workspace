import { PathLike } from "fs";
import { DocumentData } from "./DocumentData";



export interface DocumentTemplate<Data> {
    documentType: string;
    templatePath: PathLike;
    templateEncoding: BufferEncoding;
    outputTitleTemplateString: (data: Data, options?: any) => string;
}
