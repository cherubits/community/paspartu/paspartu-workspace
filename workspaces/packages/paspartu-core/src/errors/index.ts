import { ErrorResponseModel } from './ErrorResponseModel';
import { ValidateErrorJSON } from './ValidateErrorJSON';

export type {
    ErrorResponseModel,
    ValidateErrorJSON,
};
