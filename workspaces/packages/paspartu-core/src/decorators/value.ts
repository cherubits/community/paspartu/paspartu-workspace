/**
 * Decorator used at the field or method/constructor parameter level that
 * indicates a default value expression for the annotated element.
 * Typically used for expression-driven or property-driven dependency
 * injection. Also supported for dynamic resolution of handler method
 * arguments.
 *
 * A common use case is to inject values using #{environment variable name}
 * style expressions. Alternatively, values may be injected using
 * ${my.app.myProp} style property placeholders.
 *
 * Note that actual processing of the @value annotation is performed by a
 * property decorator which in turn means that you cannot use @value within
 * Inversify activation or Inversify factory types. Please consult the
 * javadoc for the AsyncContainerModule class (which, by default, checks
 * for the presence of this annotation).
 *
 * @param variable Environment variable name
 * @returns
 */
export const value = (variableName: string, defaultValue?: any, mandatory: boolean = false) => {
    return (target: any, memberName: string) => {
        if (mandatory && !(variableName in process.env)) {
            throw new Error(`Missing environmental variable "${variableName}"`);
        }
        let currentValue: any = process.env[variableName] ?? defaultValue;
        Object.defineProperty(target, memberName, {
            set: (newValue: any) => { currentValue = newValue; },
            get: () => currentValue
        });
    };
}
