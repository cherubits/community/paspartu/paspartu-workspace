import { fluentProvide } from "inversify-binding-decorators";
import { interfaces } from "inversify";

export const repository = function <T>(
  identifier: interfaces.ServiceIdentifier<T>
) {
  return fluentProvide(identifier).inTransientScope().done();
};
