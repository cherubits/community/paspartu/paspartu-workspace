import * as express from "express";
import { inject } from "inversify";
import $services from "../../core/constants/services";
import { User } from "../../domain/models/User";
import {
    Body,
    Controller,
    Get,
    Path,
    Post,
    Query,
    Route,
    SuccessResponse,
    OperationId,
    Response,
    Request,
    Tags,
    Security,
} from "tsoa";
import { ErrorResponseModel } from "../../core/errors/ErrorResponseModel";
import { ValidateErrorJSON } from "../../core/errors/ValidateErrorJSON";
import { UserResponseModel } from "../../domain/models/UserResponseModel";
import { provideSingleton } from "../../core/decorators/provideSingleton";
import { UserService } from "../../infrastructure/services/UserService";

@Route("users")
@Tags("UAAC")
@provideSingleton(UserController)
export class UserController extends Controller {

    @inject($services.UserService)
    private userService!: UserService;

    private reportError(res: express.Response, code: number, error: Error) {
        res.status(code);
        res.send(error.message);
    }

    // @httpGet("/")
    // public async get(
    //     @response() res: express.Response
    // ) {
    //     try {
    //         return await this.userRepository.find();
    //     } catch (error) {
    //         if (error instanceof Error) {
    //             this.reportError(res, 500, error);
    //         }
    //     }
    // }
    // @httpGet("/:year")
    // public async getByYear(
    //     @response() res: express.Response,
    //     @requestParam("year") yearParam: string
    // ) {
    //     try {
    //         const year = parseInt(yearParam);
    //         return await this.userRepository.find({

    //         });
    //     } catch (error) {
    //         if (error instanceof Error) {
    //             this.reportError(res, 500, error);
    //         }
    //     }
    // }
    // @httpPost("/")
    // public async post(
    //     @response() res: express.Response,
    //     @requestBody() newUser: User
    // ) {
    //     // if (
    //     //     !(typeof newUser.title === "string") || isNaN(newUser.year)
    //     // ) {
    //     //     res.status(400);
    //     //     res.send(`Invalid Movie!`);
    //     // }
    //     try {
    //         return await this.userRepository.save(newUser);
    //     } catch (error) {
    //         if (error instanceof Error) {
    //             this.reportError(res, 500, error);
    //         }
    //     }
    // }

    /**
     * Fetch users
     * @summary Fetch users
     * @param res User name
     * @returns User model
     */
    @Get()
    @OperationId("fetchUsers")
    public async fetch() {
        return this.userService.find();
    }

    /**
     * Fetch user
     * @summary Fetch user
     * @param userId User id
     * @param name User name
     * @returns User model
     */
    @Get("{userId}")
    @OperationId("getUser")
    public async get(
        @Path() userId: number,
        @Query() name?: string
    ): Promise<User> {
        const user = await this.userService.findOne(userId);
        if (user) {
            return {
                resources: [],
                roles: [],
                ...user
            };
        } else {
            throw new Error("User not found");
        }
    }

    /**
     * Crete new user
     * @summary New user
     * @param userCreationParams User dto
     * @returns Created user model
     */
    @SuccessResponse("201", "User created")
    @Response<ValidateErrorJSON>(422, "Validation Failed", {
        message: "Validation failed",
        details: {
            requestBody: {
                message: "id is an excess property and therefore not allowed",
                value: "52907745-7672-470e-a803-a2f8feb52944",
            },
        },
    })
    @Post()
    @OperationId("createUser")
    public async create(
        @Body() userCreationParams: User
    ): Promise<User> {
        return this.userService.create(userCreationParams);
    }

    /**
     * Modify user
     * @summary Modify user
     * @param userId Id of user
     * @param userCreationParams Modified user
     * @returns Modified user
     */
    @Security("jwt", ["user:create"])
    @SuccessResponse("200", "User modified")
    @Post("{userId}")
    @OperationId("modifyUser")
    public async modify(
        @Path("userId") userId: number,
        @Body() userCreationParams: User
    ): Promise<User> {
        return this.userService.modify(userId, userCreationParams);

    }

    @Response<ErrorResponseModel>(500, "Unexpected error")
    @Security("api_key")
    @Get("info")
    public async userInfo(@Request() request: any): Promise<UserResponseModel> {

        return Promise.resolve(request.user);
    }
}
