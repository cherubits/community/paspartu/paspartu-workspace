import "reflect-metadata";
import { Application, NextFunction, Request, Response, Router } from 'express';
import { Container } from 'inversify';
import { RoutingConfig, HttpResponseMessage, InversifyExpressServer } from 'inversify-express-utils';
import { use, should } from "chai";
import { spy, assert, mock } from 'sinon';

interface ServerWithTypes {
    _app: Application;
    _router: Router;
    _routingConfig: RoutingConfig;
    handleHttpResponseMessage: (
        message: HttpResponseMessage,
        res: Response
    ) => void;
}
import chaiHttp from 'chai-http';

// Configure chai
use(chaiHttp);
should();

import { REPOSITORY_SYMBOLS } from "../../core/constants/repositories";
const testContainer = new Container({
    autoBindInjectable: true,
    skipBaseClassChecks: true
});
const userRepositoryMock = mock({});
// userRepositoryMock.expects("method").once().throws();
testContainer.bind(REPOSITORY_SYMBOLS.UserRepository).toConstantValue(userRepositoryMock);
import('./UserController');


describe('Unit Test: InversifyExpressServer', () => {
    beforeEach(done => {
        // cleanUpMetadata();
        testContainer.snapshot();
        done();
    });

    afterEach(() => {

        // Restore to last snapshot so each unit test
        // takes a clean copy of the application container
        testContainer.restore();

    });

    it('should call the configFn before the errorConfigFn', done => {
        const middleware = (
            req: Request,
            res: Response,
            next: NextFunction,
        ) => {
            //
        };

        //

        const configFn = spy((app: Application) => {
            app.use(middleware);
        });

        const errorConfigFn = spy((app: Application) => {
            app.use(middleware);
        });

        const container = new Container();

        const server = new InversifyExpressServer(container, undefined, undefined, undefined, undefined, false);

        server.setConfig(configFn)
            .setErrorConfig(errorConfigFn);

        assert.notCalled(configFn);
        assert.notCalled(errorConfigFn);

        server.build();

        assert.calledOnce(configFn);
        assert.calledOnce(errorConfigFn);

        done();
    });

    //   it('Should allow to pass a custom Router instance as config', () => {
    //     const container = new Container();

    //     const customRouter = Router({
    //       caseSensitive: false,
    //       mergeParams: false,
    //       strict: false,
    //     });

    //     const serverWithDefaultRouter = new InversifyExpressServer(container);
    //     const serverWithCustomRouter = new InversifyExpressServer(
    //       container,
    //       customRouter
    //     );

    //     expect((serverWithDefaultRouter as unknown as ServerWithTypes)
    //       ._router === customRouter).toBe(false);
    //     expect((serverWithCustomRouter as unknown as ServerWithTypes)
    //       ._router === customRouter).toBe(true);
    //   });

    //   it('Should allow to provide custom routing configuration', () => {
    //     const container = new Container();

    //     const routingConfig = {
    //       rootPath: '/such/root/path',
    //     };

    //     const serverWithDefaultConfig = new InversifyExpressServer(container);
    //     const serverWithCustomConfig = new InversifyExpressServer(
    //       container,
    //       null,
    //       routingConfig
    //     );

    //     expect((serverWithCustomConfig as unknown as ServerWithTypes)
    //       ._routingConfig).toBe(routingConfig);

    //     expect((serverWithDefaultConfig as unknown as ServerWithTypes)
    //       ._routingConfig).not.toEqual(
    //         (serverWithCustomConfig as unknown as ServerWithTypes)._routingConfig,
    //       );
    //   });

    //   it('Should allow to provide a custom express application', () => {
    //     const container = new Container();
    //     const app = express();
    //     const serverWithDefaultApp = new InversifyExpressServer(container);
    //     const serverWithCustomApp = new InversifyExpressServer(
    //       container,
    //       null,
    //       null,
    //       app
    //     );

    //     expect((serverWithCustomApp as unknown as ServerWithTypes)._app).toBe(app);
    //     expect((serverWithDefaultApp as unknown as ServerWithTypes)._app).not
    //       .toEqual((serverWithCustomApp as unknown as ServerWithTypes)._app);
    //   });

    //   it('Should handle a HttpResponseMessage that has no content', () => {
    //     const container = new Container();
    //     const server = new InversifyExpressServer(container);

    //     const httpResponseMessageWithoutContent = new HttpResponseMessage(404);
    //     const mockResponse: Partial<Response> = {
    //       sendStatus: (code) => {code: code},
    //     };

    //     (server as unknown as ServerWithTypes).handleHttpResponseMessage(
    //       httpResponseMessageWithoutContent,
    //       mockResponse as unknown as Response,
    //     );

    //     expect(mockResponse.sendStatus).toHaveBeenCalledWith(404);
    //   });
});
