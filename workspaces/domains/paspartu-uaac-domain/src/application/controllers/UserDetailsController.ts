import { inject } from "inversify";
import { controller, BaseHttpController, httpGet } from "inversify-express-utils";
import { AuthenticationService } from "../../infrastructure/authentication/AuthenticationService";
import $services from '../../core/constants/services';
@controller("/")
export class UserDetailsController extends BaseHttpController {

    @inject($services.AuthenticationService)
    private authService!: AuthenticationService;

    @httpGet("/")
    public async getUserDetails() {
        if (await this.httpContext.user.isAuthenticated()) {
            return this.authService.getUserDetails(this.httpContext.user.details.id);
        } else {
            throw new Error();
        }
    }
}
