import {
    Entity,
    PrimaryGeneratedColumn,
    Column
} from "typeorm";

@Entity({
    name: 'uaac_permission'
})
export class PermissionEntity {

    @PrimaryGeneratedColumn("uuid")
    public id!: number;

    @Column()
    public code!: string;

    @Column()
    public description!: string;

}
