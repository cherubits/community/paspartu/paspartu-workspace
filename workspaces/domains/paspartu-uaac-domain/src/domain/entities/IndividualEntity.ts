import {
    Entity,
    PrimaryGeneratedColumn,
    Column
} from "typeorm";

@Entity({
    name: 'individuals'
})
export class IndividualEntity {

    @PrimaryGeneratedColumn()
    public id!: number;

    @Column()
    public lastName!: string;

    @Column()
    public firstName!: string;

}
