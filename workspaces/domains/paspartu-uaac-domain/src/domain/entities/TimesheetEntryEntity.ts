import {
    Entity,
    PrimaryGeneratedColumn,
    Column
} from "typeorm";

@Entity({
    name: 'timesheet_entries'
})
export class TimesheetEntryEntity {

    @PrimaryGeneratedColumn()
    public id!: number;

    @Column()
    public displayName!: string;

    @Column()
    public description!: string;

}
