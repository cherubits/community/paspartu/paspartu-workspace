import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    JoinTable,
    ManyToMany
} from "typeorm";
import { PermissionEntity } from "./PermissionEntity";

@Entity({
    name: 'uaac_groups'
})
export class GroupEntity {

    @PrimaryGeneratedColumn()
    public id!: number;

    @Column()
    public name!: string;

    @Column()
    public description!: string;

    @ManyToMany((type) => PermissionEntity)
    @JoinTable({
        name: 'uaac_group_permissions',
        joinColumn: {
            name: "group",
            referencedColumnName: "id",
        },
        inverseJoinColumn: {
            name: "permission",
            referencedColumnName: "id",
        },
    })
    permissions?: PermissionEntity[]

}
