
export interface UserResponseModel {
    username: string;
    email: string;
}
