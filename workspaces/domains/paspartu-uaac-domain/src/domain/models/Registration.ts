
/**
 * Value object for register user
 */
export interface Registration {

    /**
     * Username
     */
    username: string;

    /**
     * User primary email
     */
    email: string;

    /**
     * Password
     */
    password: string;

    /**
     * Password again.
     */
    passwordAgain: string;
}
