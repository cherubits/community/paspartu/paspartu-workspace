
export interface ErrorIntl {
    unknownErrorMessage?: string;
    missingEnvironmentVariableErrorMessage?: string;
    missingLocaleErrorMessage?: string;
}

export type TemplateFunction = (template: string, ...params: any[]) => string;

export interface Intl {
    [key: string]: string | TemplateFunction;
}
