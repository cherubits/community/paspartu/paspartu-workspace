import { injectable, inject } from "inversify";
import { BaseMiddleware } from "inversify-express-utils";
import $logger from "./logger";
import {Request, Response, NextFunction} from 'express';
import { MIDDLEWARE_SYMBOLS as $middlewares } from "./symbols";

@injectable()
export class AuthenticationMiddleware extends BaseMiddleware {
    // @inject(TYPES.Logger) private readonly logger: Logger;

    public async handler(
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        if (await this.httpContext.user.isAuthenticated()) {
            $logger.info(`${this.httpContext.user.details.email} => ${req.url}`, { component: $middlewares.AuthenticationMiddleware});
        } else {
            $logger.info(`Anonymous => ${req.url}`, { component: $middlewares.AuthenticationMiddleware});
        }
        // res.setHeader("token", this.httpContext.user.details.token);
        next();
    }
}
