import { describe, it } from "mocha";
import { expect } from 'chai';
import { UserPrincipal } from './UserPrincipal';
import { Container } from "inversify";



describe('Unit Test: UserPrincipal', () => {

    // application container is shared by all unit tests
    const testContainer = new Container({
        autoBindInjectable: true,
        skipBaseClassChecks: true
    });

    beforeEach(() => {

        // create a snapshot so each unit test can modify
        // it without breaking other unit tests
        testContainer.snapshot();

    });

    afterEach(() => {

        // Restore to last snapshot so each unit test
        // takes a clean copy of the application container
        testContainer.restore();

    });

    it('check user authenticating', async () => {

        expect(await new UserPrincipal({
            id: 2,
        }).isAuthenticated()).equal(false, 'should not be authenticated');

        expect(await new UserPrincipal({
            id: 2,
            token: 'test-token',
        }).isAuthenticated()).equal(true, 'should be authenticated');
    });

    it('check roles', async () => {
        expect(await new UserPrincipal({
            id: 2,
            roles: ['test-role'],
        }).isInRole('test-role')).equal(true, 'should have role "test-role"');
        expect(await new UserPrincipal({
            id: 2,
            roles: ['test-role'],
        }).isInRole('missing-test-role')).equal(false, 'should have role "missing-test-role"');
    });

    it('check owned resource', async () => {
        expect(await new UserPrincipal({
            id: 2,
            resources: ['test-resource'],
        }).isInRole('test-resource')).equal(false, 'should have resource "test-resource"');

        expect(await new UserPrincipal({
            id: 2,
            resources: ['test-resource'],
        }).isInRole('missing-test-resource')).equal(false, 'should have resource "missing-test-resource"');
    });

    it('test user profile', async () => {
        const userPrincipal = new UserPrincipal({
            id: 2,
            resources: [],
            roles: [],
            token: 'test-token'
        })
        expect(userPrincipal.details.id).to.be.equal(2, 'should be user profile id');
        // expect(anonymousPrincipal.details.resources).to.be.equal([], 'should no resources in anonymous profile');
        // expect(anonymousPrincipal.details.roles).to.be.equals([], 'should no roles in anonymous profile');
        expect(userPrincipal.details.token).to.be.equal('test-token', 'should be user profile token');
    });
});
