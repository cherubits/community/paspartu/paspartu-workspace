import { User } from "../../domain/models/User";
import { REPOSITORY_SYMBOLS } from '../../core/constants/repositories';
import { inject, injectable } from "inversify";
import { Repository } from "typeorm";
import { UserEntity } from "../../domain/entities/UserEntity";
import { provideSingleton } from "../../core/decorators/provideSingleton";

/**
 * Service layer
 */
@injectable()
export class AuthenticationService {

    // @inject($repositories.UserRepository)
    // private readonly userRepository: Repository<User>;

    constructor(
        @inject(REPOSITORY_SYMBOLS.UserRepository)
        private readonly userRepository: Repository<UserEntity>
    ) { }

    async getUserDetails(token: string): Promise<User> {
        const userEntity = await this.userRepository.findOneOrFail({
            where: {
                token: token,
                active: true
            }
        });
        return {
            resources: [],
            roles: [],
            ...userEntity
        };
    }
}
