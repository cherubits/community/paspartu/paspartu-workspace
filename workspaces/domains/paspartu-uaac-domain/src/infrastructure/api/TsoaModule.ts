import { interfaces } from "inversify";
import { BaseModule } from "../../core/BaseModule";

export class TsoaModule extends BaseModule {
    constructor() {
        super(
            {
                logger: Symbol.for('TsoaModuleLogger'),
            },
            async (
                bind: interfaces.Bind,
                unbind: interfaces.Unbind,
                isBound: interfaces.IsBound,
                rebind: interfaces.Rebind,
                unbindAsync: interfaces.UnbindAsync,
                onActivation: interfaces.Container["onActivation"],
                onDeactivation: interfaces.Container["onDeactivation"],
            ) => {

                // const specOptions: ExtendedSpecConfig = {
                //     basePath: "/api",
                //     entryFile: "./api/server.ts",
                //     specVersion: 3,
                //     outputDirectory: "./api/dist",
                //     controllerPathGlobs: ["./routeControllers/**/*Controller.ts"],
                //   };

                //   const routeOptions: ExtendedRoutesConfig = {
                //     basePath: "/api",
                //     entryFile: "./api/server.ts",
                //     routesDir: "./api",
                //   };

                //   await generateSpec(specOptions);

                //   await generateRoutes(routeOptions);


                // onActivation($repositories.UserRepository, (context: interfaces.Context, component: CalendarImp) => {
                //     $logger.info(`${moduleName} activate`, { contextId: context.id });
                //     component.setup();
                //     // // (context, katana) => {
                //     // let handler = {
                //     //     apply: function (target, thisArgument, argumentsList) {
                //     //         console.log(`Starting: ${new Date().getTime()}`);
                //     //         let result = target.apply(thisArgument, argumentsList);
                //     //         console.log(`Finished: ${new Date().getTime()}`);
                //     //         return result;
                //     //     }
                //     // };
                //     // katana.use = new Proxy(katana.use, handler);
                //     return component;
                // });

                // onDeactivation($repositories.UserRepository, (component: CalendarImp) => {
                //     $logger.info(`${moduleName} deactivate`);
                //     component.dispose();
                // })
            });
    }
}
