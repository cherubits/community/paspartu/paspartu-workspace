import { AsyncContainerModule, interfaces } from "inversify";
import {REPOSITORY_SYMBOLS} from "../../core/constants/repositories";
import $logger from './logging';
import { DataSource, Repository } from "typeorm";
import { UserEntity } from "../../domain/entities/UserEntity";
import { applicationDataSource } from './datasource';
import { IndividualEntity } from "../../domain/entities/IndividualEntity";
import { OrganizationEntity } from "../../domain/entities/OrganizationEntity";
import { ProjectEntity } from "../../domain/entities/ProjectEntity";
import { GroupEntity } from "../../domain/entities/GroupEntity";
import { PermissionEntity } from "../../domain/entities/PermissionEntity";
export class PersistenceContainerModule extends AsyncContainerModule {
    constructor() {
        super(async (
            bind: interfaces.Bind,
            unbind: interfaces.Unbind,
            isBound: interfaces.IsBound,
            rebind: interfaces.Rebind,
            unbindAsync: interfaces.UnbindAsync,
            onActivation: interfaces.Container["onActivation"],
            onDeactivation: interfaces.Container["onDeactivation"],
        ) => {


            await applicationDataSource.initialize();
            $logger.info("data-source has been initialized");

            bind<Repository<IndividualEntity>>(REPOSITORY_SYMBOLS.IndividualRepository)
                .toDynamicValue(() => applicationDataSource.getRepository(IndividualEntity))
                .inRequestScope();
            bind<Repository<OrganizationEntity>>(REPOSITORY_SYMBOLS.OrganizationRepository)
                .toDynamicValue(() => applicationDataSource.getRepository(OrganizationEntity))
                .inRequestScope();
            bind<Repository<ProjectEntity>>(REPOSITORY_SYMBOLS.ProjectRepository)
                .toDynamicValue(() => applicationDataSource.getRepository(ProjectEntity))
                .inRequestScope();

            bind<Repository<GroupEntity>>(REPOSITORY_SYMBOLS.GroupRepository)
                .toDynamicValue(() => applicationDataSource.getRepository(GroupEntity))
                .inRequestScope();
            bind<Repository<PermissionEntity>>(REPOSITORY_SYMBOLS.PermissionRepository)
                .toDynamicValue(() => applicationDataSource.getRepository(PermissionEntity))
                .inRequestScope();
            bind<Repository<UserEntity>>(REPOSITORY_SYMBOLS.UserRepository)
                .toDynamicValue(() => applicationDataSource.getRepository(UserEntity))
                .inRequestScope();

            // onActivation($repositories.UserRepository, (context: interfaces.Context, component: CalendarImp) => {
            //     $logger.info(`${moduleName} activate`, { contextId: context.id });
            //     component.setup();
            //     // // (context, katana) => {
            //     // let handler = {
            //     //     apply: function (target, thisArgument, argumentsList) {
            //     //         console.log(`Starting: ${new Date().getTime()}`);
            //     //         let result = target.apply(thisArgument, argumentsList);
            //     //         console.log(`Finished: ${new Date().getTime()}`);
            //     //         return result;
            //     //     }
            //     // };
            //     // katana.use = new Proxy(katana.use, handler);
            //     return component;
            // });

            // onDeactivation($repositories.UserRepository, (component: CalendarImp) => {
            //     $logger.info(`${moduleName} deactivate`);
            //     component.dispose();
            // })
        });
    }
}

export default PersistenceContainerModule;
