import { DataSource } from "typeorm";

export const applicationDataSource = new DataSource({
    type: "postgres",
    host: "localhost",
    port: 5432,
    username: "paspartu",
    password: "paspartu",
    database: "paspartu",
    synchronize: true,
    logging: false,
    logNotifications: true,
    entities: ["**/*Entity.ts"],
    subscribers: ["**/subscribers/*.ts"],
    migrations: ["**/migrations/*.ts"],
    migrationsRun: true,
    migrationsTransactionMode: "each",
    migrationsTableName: "database_changelog",
    applicationName: 'paspartu',
    useUTC: true,
});
