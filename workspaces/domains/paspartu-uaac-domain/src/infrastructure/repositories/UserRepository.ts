import { injectable } from "inversify";
import { getConnection, Repository } from "typeorm";
import { UserEntity } from "../../domain/entities/UserEntity";


@injectable()
export class UserRepository extends Repository<UserEntity> {}
