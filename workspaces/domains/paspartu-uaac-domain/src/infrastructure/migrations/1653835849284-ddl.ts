import { MigrationInterface, QueryRunner } from "typeorm";

export class ddl1653835849284 implements MigrationInterface {
    name = 'ddl1653835849284'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "uaac_permission" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "code" character varying NOT NULL, "description" character varying NOT NULL, CONSTRAINT "PK_74c13935cbaae16f449398968bc" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "uaac_groups" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "description" character varying NOT NULL, CONSTRAINT "PK_e3e7d9acc981aedb3674a7c5f3f" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "organizations" ("id" SERIAL NOT NULL, "displayName" character varying NOT NULL, "description" character varying NOT NULL, CONSTRAINT "PK_6b031fcd0863e3f6b44230163f9" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "individuals" ("id" SERIAL NOT NULL, "lastName" character varying NOT NULL, "firstName" character varying NOT NULL, CONSTRAINT "PK_ebf809180acc8fce381144eb48b" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "projects" ("id" SERIAL NOT NULL, "displayName" character varying NOT NULL, "description" character varying NOT NULL, CONSTRAINT "PK_6271df0a7aed1d6c0691ce6ac50" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "timesheet_entries" ("id" SERIAL NOT NULL, "displayName" character varying NOT NULL, "description" character varying NOT NULL, CONSTRAINT "PK_25a8a9b6a96e72864d598563c56" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "uaac_users" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), "updatedDate" TIMESTAMP NOT NULL DEFAULT now(), "deletedDate" TIMESTAMP, "active" boolean NOT NULL, "token" uuid NOT NULL DEFAULT uuid_generate_v4(), CONSTRAINT "PK_d591939d9600c66549632e45f8e" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "uaac_group_permissions" ("group" integer NOT NULL, "permission" uuid NOT NULL, CONSTRAINT "PK_51280a8852483a2337f1af6e9ce" PRIMARY KEY ("group", "permission"))`);
        await queryRunner.query(`CREATE INDEX "IDX_c4d3cfc296c6e1f9a3f28513eb" ON "uaac_group_permissions" ("group") `);
        await queryRunner.query(`CREATE INDEX "IDX_f73c6c305be2d520015cf368bc" ON "uaac_group_permissions" ("permission") `);
        await queryRunner.query(`CREATE TABLE "uaac_user_permissions" ("user" integer NOT NULL, "permission" uuid NOT NULL, CONSTRAINT "PK_42e3717d6ae5ce05725b4d5b189" PRIMARY KEY ("user", "permission"))`);
        await queryRunner.query(`CREATE INDEX "IDX_e2b741f28e4de9ceb7370ceb2a" ON "uaac_user_permissions" ("user") `);
        await queryRunner.query(`CREATE INDEX "IDX_2d7e4716b4eb9ee0cc966af819" ON "uaac_user_permissions" ("permission") `);
        await queryRunner.query(`ALTER TABLE "uaac_group_permissions" ADD CONSTRAINT "FK_c4d3cfc296c6e1f9a3f28513ebf" FOREIGN KEY ("group") REFERENCES "uaac_groups"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "uaac_group_permissions" ADD CONSTRAINT "FK_f73c6c305be2d520015cf368bca" FOREIGN KEY ("permission") REFERENCES "uaac_permission"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "uaac_user_permissions" ADD CONSTRAINT "FK_e2b741f28e4de9ceb7370ceb2a9" FOREIGN KEY ("user") REFERENCES "uaac_users"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "uaac_user_permissions" ADD CONSTRAINT "FK_2d7e4716b4eb9ee0cc966af8193" FOREIGN KEY ("permission") REFERENCES "uaac_permission"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "uaac_user_permissions" DROP CONSTRAINT "FK_2d7e4716b4eb9ee0cc966af8193"`);
        await queryRunner.query(`ALTER TABLE "uaac_user_permissions" DROP CONSTRAINT "FK_e2b741f28e4de9ceb7370ceb2a9"`);
        await queryRunner.query(`ALTER TABLE "uaac_group_permissions" DROP CONSTRAINT "FK_f73c6c305be2d520015cf368bca"`);
        await queryRunner.query(`ALTER TABLE "uaac_group_permissions" DROP CONSTRAINT "FK_c4d3cfc296c6e1f9a3f28513ebf"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_2d7e4716b4eb9ee0cc966af819"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_e2b741f28e4de9ceb7370ceb2a"`);
        await queryRunner.query(`DROP TABLE "uaac_user_permissions"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_f73c6c305be2d520015cf368bc"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_c4d3cfc296c6e1f9a3f28513eb"`);
        await queryRunner.query(`DROP TABLE "uaac_group_permissions"`);
        await queryRunner.query(`DROP TABLE "uaac_users"`);
        await queryRunner.query(`DROP TABLE "timesheet_entries"`);
        await queryRunner.query(`DROP TABLE "projects"`);
        await queryRunner.query(`DROP TABLE "individuals"`);
        await queryRunner.query(`DROP TABLE "organizations"`);
        await queryRunner.query(`DROP TABLE "uaac_groups"`);
        await queryRunner.query(`DROP TABLE "uaac_permission"`);
    }

}
