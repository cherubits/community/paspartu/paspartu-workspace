import { provideSingleton } from "../../core/decorators/provideSingleton";
import $services from '../../core/constants/services';
import { REPOSITORY_SYMBOLS } from '../../core/constants/repositories';
import { UserRepository } from "../repositories/UserRepository";
import { UserEntity } from "../../domain/entities/UserEntity";
import { inject } from "inversify";
import { Repository } from "typeorm";
import { User } from "../../domain/models/User";
import $logger from '../../logging';

@provideSingleton($services.UserService)
export class UserService {
    constructor(
        @inject(REPOSITORY_SYMBOLS.UserRepository)
        private userRepository: Repository<UserEntity>
    ) { }

    public async create(user: User) {
        const entity = this.userRepository.create({ ...user });
        $logger.info(`created new user with id='${entity.id}'`, { entity: user });
        return entity;
    }


    public async modify(id: number, user: User) {
        const entity = await this.userRepository.findOneByOrFail({ id: id });
        const modifiedUserEntity = Object.assign(entity, user);
        modifiedUserEntity.id = id;
        const result = this.userRepository.save(modifiedUserEntity);
        $logger.info(`modified user with id='${entity.id}'`, { entity: result });
        return result;
    }

    public async erase(id: number) {
        const entity = await this.userRepository.findOneByOrFail({ id: id });
        this.userRepository.remove(entity);
        $logger.info(`created new user with id='${entity.id}'`, { entity: entity });
    }


    public async findOne(id: number) {
        return this.userRepository.findOneByOrFail({ id: id });
    }

    public async find() {
        return this.userRepository.find();
    }

    public async findOneByCredentials(username: string, password: string) {
        return this.userRepository.findOneByOrFail({
            username: username,
            password: password
         });
    }

}
