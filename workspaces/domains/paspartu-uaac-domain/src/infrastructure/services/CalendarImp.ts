import { injectable } from "inversify";
import { Calendar, Disposable } from "../../core/interfaces/Calendar";
import { Day } from "../../core/interfaces/Day";
import $logger from '../../logging';

@injectable()
export class CalendarImp implements Calendar, Disposable {

    dispose(disposing: boolean = true): void {
        $logger.info(`calendar is disposing=${disposing}`);
    }


    setup(): void {

        $logger.info('setup calendar');
    }

    week(index: number): Day[] {
        throw new Error("Method not implemented.");
    }
    month(index: string | number): Day[] {
        throw new Error("Method not implemented.");
    }
    public use() {
        $logger.info("Used Katana!");
    }
}

