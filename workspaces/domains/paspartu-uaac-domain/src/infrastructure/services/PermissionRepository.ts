import { PathLike } from "fs";
import { performance } from "perf_hooks";
import { value } from "../../core/decorators/value";

const measure = (
    target: Object,
    propertyKey: string,
    descriptor: PropertyDescriptor
) => {
    const originalMethod = descriptor.value;

    descriptor.value = function (...args: Array<any>) {
        const start = performance.now();
        const result = originalMethod.apply(this, args);
        const finish = performance.now();
        console.log(`Execution time: ${finish - start} milliseconds`);
        return result;
    };

    return descriptor;
};

const changeValue = (value: any) => (target: Object, propertyKey: string) => {
    Object.defineProperty(target, propertyKey, { value });
};


// const minimumFuel = (fuel: number) => (
//     target: Object,
//     propertyKey: string,
//     descriptor: PropertyDescriptor
// ) => {
//     const originalMethod = descriptor.value;

//     descriptor.value = function (...args: Array<any>) {
//         if (this.fuel > fuel) {
//             originalMethod.apply(this, args);
//         } else {
//             console.log("Not enough fuel!");
//         }
//     };

//     return descriptor;
// };

export interface Permission {
    name: string;
    displayName: string;
    description: string;
}


const classDecorator = (target: Function) => {
    // do something with your class
}

const addFuelToRocket = <T extends { new(...args: any[]): {} }>(constructor: T) => {
    return class extends constructor {
        fuel = 100
    }
}

const myDecorator = (target: Object, propertyKey: string, descriptor: PropertyDescriptor) => {
    // do something with your method
}

const propertyDecorator = (target: Object, propertyKey: string) => {
    // do something with your property
}

export class Filter implements Record<string, any> {
}



export class StorageConfiguration {

    @value('PASPARTU_STORAGE_USER')
    user?: string;

    @value('PASPARTU_STORAGE_GROUP')
    group?: string;

    @value('PASPARTU_TMP_DIR')
    tmp?: PathLike;

    @value('PASPARTU_VAR_DIR')
    var?: PathLike;
}


export class AuthorService {

    public collectTickets(): void {}
}
