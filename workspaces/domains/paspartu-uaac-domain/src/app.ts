

import express, { Application, Response as ExResponse, Request as ExRequest } from "express";
import swaggerUi from "swagger-ui-express";

const app: Application = express();

export default app;
