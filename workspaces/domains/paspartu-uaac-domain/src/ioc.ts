import { Container, decorate, injectable } from "inversify";
import $logger from './logging';
import {Controller} from 'tsoa';
import { buildProviderModule } from "inversify-binding-decorators";
import './application/controllers/UserController';
import I18nModule from "./infrastructure/i18n";
import PersistenceContainerModule from "./infrastructure/persistence";
import AuthenticationModule from "./infrastructure/authentication";

const iocContainer = new Container({
    autoBindInjectable: true,
    skipBaseClassChecks: true,
});

(async () => {

    $logger.info('loading container modules', { component: 'container'});

    await iocContainer.loadAsync(
        new I18nModule(),
        new PersistenceContainerModule(),
        new AuthenticationModule()
    );

    decorate(injectable(), Controller); // Makes tsoa's Controller injectable
    iocContainer.load(buildProviderModule());
    $logger.warn('initialize controllers', { component: 'container'});

    $logger.info('container modules initialized', { component: 'container'});

    return iocContainer;
})();

// export according to convention
export { iocContainer };
