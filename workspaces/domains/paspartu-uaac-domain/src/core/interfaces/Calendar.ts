import { Day } from "./Day";

export interface Calendar {

    week(index: number): Array<Day>;
    month(index: number|string): Array<Day>;
    setup(): void;
}


export interface Disposable {
    dispose(disposing: boolean): void;
}

export interface Initializer {
    setup(): void;
}

export interface Runnable extends Initializer, Disposable, Function {

}

export interface Command<DataType> {

    readonly canFulfill: boolean;

    execute(subject: DataType): void;
}
