export const REPOSITORY_SYMBOLS = {
    IndividualRepository: Symbol("IndividualRepository"),
    OrganizationRepository: Symbol("OrganizationRepository"),
    ProjectRepository: Symbol("ProjectRepository"),
    GroupRepository: Symbol("GroupRepository"),
    UserRepository: Symbol("UserRepository"),
    PermissionRepository: Symbol("PermissionRepository"),
};

export default REPOSITORY_SYMBOLS;
