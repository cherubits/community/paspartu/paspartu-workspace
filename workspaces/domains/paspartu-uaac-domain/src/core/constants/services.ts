export const SERVICE_IDENTIFIER = {
    CALENDAR: Symbol.for("Calendar"),
    WARRIOR: Symbol.for("Warrior"),
    WEAPON: Symbol.for("Weapon"),
    AuthenticationService: Symbol.for("AuthenticationService"),
    UserService: Symbol.for("UserService"),
};

export default SERVICE_IDENTIFIER;
