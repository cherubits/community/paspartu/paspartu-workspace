import { AsyncContainerModule, interfaces } from "inversify";
import { Logger } from "winston";
import { createModuleLogger } from './createModuleLogger';
import { Intl } from '../infrastructure/i18n/intl';

export interface BasicModuleSymbols {
    logger?: interfaces.ServiceIdentifier<Logger>;
    intl?: interfaces.ServiceIdentifier<Intl>;
}

export class BaseModule extends AsyncContainerModule {
    constructor($symbols: BasicModuleSymbols, registry: interfaces.AsyncContainerModuleCallBack) {
        super(async (
            bind: interfaces.Bind,
            unbind: interfaces.Unbind,
            isBound: interfaces.IsBound,
            rebind: interfaces.Rebind,
            unbindAsync: interfaces.UnbindAsync,
            onActivation: interfaces.Container["onActivation"],
            onDeactivation: interfaces.Container["onDeactivation"],
        ) => {

            if ($symbols.logger) {
                const $logger: Logger = $symbols.logger as Logger;
                bind<Logger>($symbols.logger)
                    .toDynamicValue(() => createModuleLogger($logger))
                    .inSingletonScope();
            }

            registry(bind, unbind, isBound, rebind, unbindAsync, onActivation, onDeactivation);

            // onActivation($repositories.UserRepository, (context: interfaces.Context, component: CalendarImp) => {
            //     $logger.info(`${moduleName} activate`, { contextId: context.id });
            //     component.setup();
            //     // // (context, katana) => {
            //     // let handler = {
            //     //     apply: function (target, thisArgument, argumentsList) {
            //     //         console.log(`Starting: ${new Date().getTime()}`);
            //     //         let result = target.apply(thisArgument, argumentsList);
            //     //         console.log(`Finished: ${new Date().getTime()}`);
            //     //         return result;
            //     //     }
            //     // };
            //     // katana.use = new Proxy(katana.use, handler);
            //     return component;
            // });

            // onDeactivation($repositories.UserRepository, (component: CalendarImp) => {
            //     $logger.info(`${moduleName} deactivate`);
            //     component.dispose();
            // })
        });
    }
}
