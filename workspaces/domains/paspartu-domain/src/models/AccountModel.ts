
export interface AccountModel {
    username: string;
    email: string;
}
