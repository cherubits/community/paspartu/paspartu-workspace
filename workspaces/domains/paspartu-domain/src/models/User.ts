/**
 * User objects allow you to associate actions performed in the system with the user that performed them.
 * The User object contains common information across every user in the system regardless of status and role.
 *
 *
 * @example {
 *  "id": 1,
 *  "token": "52907745-7672-470e-a803-a2f8feb52944"
 * }
 */
export interface User {

    /**
     * User id
     */
    id?: number;

    /**
     * Authorization token
     */
    token?: string;

    email?: string;

    username?: string;

    password?: string;

    resources?: Array<unknown>;

    roles?: Array<string>;
}
