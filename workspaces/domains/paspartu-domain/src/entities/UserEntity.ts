import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    JoinTable,
    ManyToMany,
    Generated
} from "typeorm";
import { PermissionEntity } from "./PermissionEntity";

@Entity({
    name: 'uaac_users'
})
export class UserEntity {

    @PrimaryGeneratedColumn()
    public id!: number;

    @Column()
    public title!: string;

    @CreateDateColumn()
    public createdDate!: Date;

    @UpdateDateColumn()
    public updatedDate!: Date;

    @DeleteDateColumn()
    public deletedDate!: Date;

    @ManyToMany((type) => PermissionEntity)
    @JoinTable({
        name: 'uaac_user_permissions',
        joinColumn: {
            name: "user",
            referencedColumnName: "id",
        },
        inverseJoinColumn: {
            name: "permission",
            referencedColumnName: "id",
        },
    })
    public permissions?: Array<PermissionEntity>;

    @Column({
        // default: () => true
    })
    public active!: boolean;

    @Column({
        // default: () => true
    })
    @Generated("uuid")
    public token!: string;

    @Column()
    public username!: string;

    @Column()
    public email!: string;


    @Column()
    public password!: string;
}
