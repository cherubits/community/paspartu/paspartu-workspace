import {
    Entity,
    PrimaryGeneratedColumn,
    Column
} from "typeorm";

@Entity({
    name: 'organizations'
})
export class OrganizationEntity {

    @PrimaryGeneratedColumn()
    public id!: number;

    @Column()
    public displayName!: string;

    @Column()
    public description!: string;

}
