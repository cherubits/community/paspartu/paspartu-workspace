export const $repositories = {
    IndividualRepository: Symbol("IndividualRepository"),
    OrganizationRepository: Symbol("OrganizationRepository"),
    ProjectRepository: Symbol("ProjectRepository"),
    GroupRepository: Symbol("GroupRepository"),
    UserRepository: Symbol("UserRepository"),
    PermissionRepository: Symbol("PermissionRepository"),
    TimesheetRepository: Symbol("TimesheetRepository"),
    UserDetailsRepository: Symbol("UserDetailsRepository"),
};
