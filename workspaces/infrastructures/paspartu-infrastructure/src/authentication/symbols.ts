export const $middlewares = {
    AuthenticationMiddleware: Symbol.for("AuthenticationMiddleware")
};

export const $providers = {
    AuthenticationProvider: Symbol.for("AuthenticationProvider")
};

export const $services = {
    AuthenticationService: Symbol.for('AuthenticationService')
}
