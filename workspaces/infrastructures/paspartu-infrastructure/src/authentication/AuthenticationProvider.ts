import { inject, injectable } from "inversify";
import { interfaces } from "inversify-express-utils";
import { Request, Response, NextFunction } from 'express';
import { $providers, $services } from "./symbols";

import { AuthenticationService } from "./AuthenticationService";
import { UserPrincipal } from "./UserPrincipal";
import { AnonymousPrincipal } from "./AnonymousPrincipal";
import $logger from './logging';
import { provideSingleton } from "@lordoftheflies/paspartu-core/src/decorators";

@provideSingleton($providers.AuthenticationProvider)
export class AuthenticationProvider implements interfaces.AuthProvider {

    @inject($services.AuthenticationService)
    private authenticationService!: AuthenticationService;

    public async getUser(
        req: Request,
        res: Response,
        next: NextFunction
    ): Promise<interfaces.Principal> {
        const token = req.headers["x-auth-token"] as string; // x-access-token
        if (token) {
            const user = await this.authenticationService.getUserDetails(token);
            // res.setHeader('', '')
            $logger.debug(`provide user '${user.id}'`, { component: $providers.AuthenticationProvider });
            return new UserPrincipal(user);
        } else {
            return new AnonymousPrincipal();
        }
    }

}


