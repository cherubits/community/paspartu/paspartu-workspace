import { interfaces } from "inversify-express-utils";
import { User } from "@lordoftheflies/paspartu-domain/src/models";
import $logger from './logging';


export class UserPrincipal implements interfaces.Principal {
    public details: User;
    public constructor(details: User) {
        this.details = details;
    }
    public isAuthenticated(): Promise<boolean> {
        $logger.debug(`check: not authenticated`, { 'security': 'anonymous' });
        return Promise.resolve(!!this.details.token);
    }
    public isResourceOwner(resourceId: unknown): Promise<boolean> {
        $logger.debug(`check: not own resource '${resourceId}'`, { 'security': 'anonymous' });
        return Promise.resolve((this.details.resources) ? this.details.resources.includes(resourceId) : false);
    }
    public isInRole(role: string): Promise<boolean> {
        $logger.debug(`check: not member of role '${role}'`, { 'security': 'anonymous' });
        return Promise.resolve(this.details.roles ? this.details.roles.includes(role) : false);
    }
}
