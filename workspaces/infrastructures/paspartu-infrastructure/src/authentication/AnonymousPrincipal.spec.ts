import { describe, it } from "mocha";
import { expect } from 'chai';
import { AnonymousPrincipal } from './AnonymousPrincipal';

describe('Unit Test: AnonymousPrincipal', () => {

    let anonymousPrincipal: AnonymousPrincipal = new AnonymousPrincipal();

    it('check anonymous is not authenticated', async () => {
        expect(await anonymousPrincipal.isAuthenticated()).equal(false, 'should not be authenticated');
    });

    it('check anonymous has no roles', async () => {
        expect(await anonymousPrincipal.isInRole('test-role')).equal(false, 'should not be in any role');
    });

    it('check anonymous has no owned resource', async () => {
        expect(await anonymousPrincipal.isResourceOwner('test-resource')).equal(false, 'should not own any resource');
    });

    it('test anonymous profile', async () => {
        expect(anonymousPrincipal.details.id).to.be.equal(1, 'should be anonymous profile id');
        // expect(anonymousPrincipal.details.resources).to.be.equal([], 'should no resources in anonymous profile');
        // expect(anonymousPrincipal.details.roles).to.be.equals([], 'should no roles in anonymous profile');
        expect(anonymousPrincipal.details.token).to.be.equal('anonymous', 'should be anonymous profile token');
    });
});
