import { inject, injectable } from "inversify";
import { Repository } from "typeorm";
import { User } from "@lordoftheflies/paspartu-domain/src/models";
import { UserEntity } from "@lordoftheflies/paspartu-domain/src/entities";
import { $repositories } from '@lordoftheflies/paspartu-domain/src/symbols';
import { $services } from './symbols';
import { provideSingleton } from "@lordoftheflies/paspartu-core/src/decorators";

/**
 * Service layer
 */
@provideSingleton($services.AuthenticationService)
export class AuthenticationService {

    // @inject($repositories.UserRepository)
    // private readonly userRepository: Repository<User>;

    constructor(
        @inject($repositories.UserDetailsRepository)
        private readonly userRepository: Repository<UserEntity>
    ) { }

    async getUserDetails(token: string): Promise<User> {
        const userEntity = await this.userRepository.findOneOrFail({
            where: {
                token: token,
                active: true
            }
        });
        return {
            resources: [],
            roles: [],
            ...userEntity
        };
    }
}
