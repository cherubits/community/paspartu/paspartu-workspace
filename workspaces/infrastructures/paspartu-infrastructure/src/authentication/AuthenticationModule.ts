

import { injectable, Container, AsyncContainerModule, interfaces, ContainerModule } from "inversify";
import { provide, buildProviderModule } from "inversify-binding-decorators";
import {
    $middlewares,
    $providers,
    $services,
} from './symbols';
import { $repositories } from '@lordoftheflies/paspartu-domain/src/symbols'

import $logger from './logging';
import { AuthenticationMiddleware } from "./AuthenticationMiddleware";
import { AuthenticationService } from "./AuthenticationService";
import { AuthenticationProvider } from "./AuthenticationProvider";
import { UserEntity } from "@lordoftheflies/paspartu-domain/src/entities";

export class AuthenticationModule extends AsyncContainerModule {

    constructor() {
        super(async (
            bind: interfaces.Bind,
            unbind: interfaces.Unbind,
            isBound: interfaces.IsBound,
            rebind: interfaces.Rebind,
            unbindAsync: interfaces.UnbindAsync,
            onActivation: interfaces.Container["onActivation"],
            onDeactivation: interfaces.Container["onDeactivation"],
        ) => {
            $logger.info('initialize module');
            try {
                // bind<Repository<UserEntity>>($repositories.UserRepository)
                //     .toDynamicValue(() => dataSource.getRepository(entityClass))
                //     .inRequestScope();
                // bind<AuthenticationService>($services.AuthenticationService)
                //     .to(AuthenticationService)
                //     .inSingletonScope();
                // bind<AuthenticationProvider>($providers.AuthenticationProvider)
                //     .to(AuthenticationProvider)
                //     .inSingletonScope();
                // bind<AuthenticationMiddleware>($middlewares.AuthenticationMiddleware)
                //     .to(AuthenticationMiddleware)
                //     .inSingletonScope();
                $logger.info(`loaded authentication middleware`);
            } catch (exception) {
                $logger.warn(`loaded authentication middleware failed`, exception);
            }
            $logger.info('module initialized');
        });
    }
}

export default AuthenticationModule;

