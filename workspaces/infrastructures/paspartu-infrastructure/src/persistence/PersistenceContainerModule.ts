import { AsyncContainerModule, interfaces } from "inversify";
import { $repositories } from "./symbols";
import $logger from './logging';
import { DataSource, EntityTarget, Repository } from "typeorm";
import { applicationDataSource } from './datasource';
import {
    IndividualEntity,
    GroupEntity,
    OrganizationEntity,
    PermissionEntity,
    ProjectEntity,
    TimesheetEntryEntity,
    UserEntity,
} from '@lordoftheflies/paspartu-domain/src/entities';
export class PersistenceContainerModule extends AsyncContainerModule {
    constructor() {
        super(async (
            bind: interfaces.Bind,
            unbind: interfaces.Unbind,
            isBound: interfaces.IsBound,
            rebind: interfaces.Rebind,
            unbindAsync: interfaces.UnbindAsync,
            onActivation: interfaces.Container["onActivation"],
            onDeactivation: interfaces.Container["onDeactivation"],
        ) => {


            await applicationDataSource.initialize();
            $logger.info("data-source has been initialized");

            this.registerRepository(bind, applicationDataSource, $repositories.IndividualRepository, IndividualEntity);
            this.registerRepository(bind, applicationDataSource, $repositories.OrganizationRepository, OrganizationEntity);
            this.registerRepository(bind, applicationDataSource, $repositories.ProjectRepository, ProjectEntity);
            this.registerRepository(bind, applicationDataSource, $repositories.GroupRepository, GroupEntity);
            this.registerRepository(bind, applicationDataSource, $repositories.UserRepository, UserEntity);
            this.registerRepository(bind, applicationDataSource, $repositories.PermissionRepository, PermissionEntity);
            this.registerRepository(bind, applicationDataSource, $repositories.TimesheetRepository, TimesheetEntryEntity);

            bind<Repository<UserEntity>>($repositories.UserDetailsRepository)
                .toDynamicValue(() => applicationDataSource.getRepository(UserEntity))
                .inSingletonScope();

            // onActivation($repositories.UserRepository, (context: interfaces.Context, component: CalendarImp) => {
            //     $logger.info(`${moduleName} activate`, { contextId: context.id });
            //     component.setup();
            //     // // (context, katana) => {
            //     // let handler = {
            //     //     apply: function (target, thisArgument, argumentsList) {
            //     //         console.log(`Starting: ${new Date().getTime()}`);
            //     //         let result = target.apply(thisArgument, argumentsList);
            //     //         console.log(`Finished: ${new Date().getTime()}`);
            //     //         return result;
            //     //     }
            //     // };
            //     // katana.use = new Proxy(katana.use, handler);
            //     return component;
            // });

            // onDeactivation($repositories.UserRepository, (component: CalendarImp) => {
            //     $logger.info(`${moduleName} deactivate`);
            //     component.dispose();
            // })
        });
    }

    registerRepository<T>(bind: interfaces.Bind, dataSource: DataSource, serviceIdentifier: interfaces.ServiceIdentifier<Repository<T>>, entityClass: EntityTarget<T>) {
        bind<Repository<T>>(serviceIdentifier)
            .toDynamicValue(() => dataSource.getRepository(entityClass))
            .inRequestScope();
    }
}

export default PersistenceContainerModule;
