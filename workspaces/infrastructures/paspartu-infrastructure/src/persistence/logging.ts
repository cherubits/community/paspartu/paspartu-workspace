import {
    createLogger,
    format,
    transports
} from 'winston';

const logger = createLogger({
    level: 'info',
    format: format.json(),
    defaultMeta: { component: 'persistence' },
    transports: [
        //
        // - Write all logs with importance level of `error` or less to `error.log`
        // - Write all logs with importance level of `info` or less to `combined.log`
        //
        new transports.File({ filename: 'error.log', level: 'error' }),
        new transports.File({ filename: 'combined.log' }),
    ],
});

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
if (process.env.NODE_ENV !== 'production') {
    logger.add(new transports.Console({
        format: format.combine(
            format.align(),
            format.timestamp(),
            format.colorize(),
            format.simple(),
            // format.prettyPrint(),
            // format.logstash()
            // format.printf(info => `${info.timestamp} [${info.level}]: ${info.message}`),
          ),
    }));
}

export default logger;
