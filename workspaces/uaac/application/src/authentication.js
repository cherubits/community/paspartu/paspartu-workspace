"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.expressAuthentication = void 0;
const jwt = __importStar(require("jsonwebtoken"));
const container_1 = require("./container");
const symbols_1 = require("@lordoftheflies/paspartu-uaac-infrastructure/src/authentication/symbols");
const logger_1 = __importDefault(require("./logger"));
async function expressAuthentication(request, securityName, scopes) {
    const service = container_1.iocContainer.get(symbols_1.SERVICE_SYMBOLS.AuthenticationService);
    logger_1.default.warn(`read security tokens`, { middleware: 'express', security: securityName, scopes: scopes });
    if (securityName === "api_key") {
        let token;
        if (request.query && request.query.access_token) {
            token = request.query.access_token;
        }
        if (token) {
            try {
                logger_1.default.debug(`get API key ${token}`);
                return service.getUserDetails(token.toString());
            }
            catch (ex) {
                logger_1.default.warn(`read API key failed`, { error: ex });
                return Promise.reject({});
            }
        }
        else {
            logger_1.default.warn(`no API key provided`);
            return Promise.reject({});
        }
    }
    else if (securityName === "jwt") {
        const token = request.body.token ||
            request.query.access_token ||
            request.headers["x-access-token"];
        return new Promise((resolve, reject) => {
            if (!token) {
                logger_1.default.warn(`No JWT token provided`);
                reject(new Error("No token provided"));
            }
            else {
                jwt.verify(token, "secret", function (err, decoded) {
                    if (err) {
                        logger_1.default.warn(`Invalid JWT token provided`);
                        reject(err);
                    }
                    else {
                        if (scopes) {
                            // Check if JWT contains all required scopes
                            for (let scope of scopes) {
                                if (!decoded.scopes.includes(scope)) {
                                    reject(new Error("JWT does not contain required scope."));
                                }
                            }
                            logger_1.default.warn(`get JWT token '${token}'`);
                            resolve(decoded);
                        }
                    }
                });
            }
        });
    }
    else {
        logger_1.default.warn(`unknown security method ${securityName}`);
        return Promise.reject({});
    }
}
exports.expressAuthentication = expressAuthentication;
//# sourceMappingURL=authentication.js.map