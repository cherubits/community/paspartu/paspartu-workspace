import * as express from "express";
import { inject } from "inversify";
import $services from "@lordoftheflies/paspartu-uaac-domain/dist/symbols/services";
import { UserEntity } from "@lordoftheflies/paspartu-uaac-domain/dist/entities/UserEntity";

import {
    Body,
    Controller,
    Get,
    Path,
    Post,
    Query,
    Route,
    SuccessResponse,
    OperationId,
    Response,
    Request,
    Tags,
    Security,
} from "tsoa";
import { ErrorResponseModel } from "@lordoftheflies/paspartu-core/src/errors/ErrorResponseModel";
import { ValidateErrorJSON } from "@lordoftheflies/paspartu-core/src/errors/ValidateErrorJSON";
// import { UserResponseModel } from "../../domain/models/UserResponseModel";
import { provideSingleton } from "@lordoftheflies/paspartu-core/src/decorators/provideSingleton";
import { UserService } from "@lordoftheflies/paspartu-uaac-domain/dist/services/UserService";
import { CredentialsModel } from "@lordoftheflies/paspartu-uaac-domain/src/models/CredentialsModel";
import { RegistrationModel } from "@lordoftheflies/paspartu-uaac-domain/src/models/RegistrationModel";

import * as jwt from "jsonwebtoken";
import { httpPost } from "inversify-express-utils";
import { UserModel } from "@lordoftheflies/paspartu-uaac-domain/src/models/UserModel";

@Route("account")
@Tags("UAAC")
@provideSingleton(AccountController)
export class AccountController extends Controller {

    @inject($services.UserService)
    private userService!: UserService;

    /**
     * Login user
     * @summary Login
     * @param credentials Id of user
     * @param request Modified user
     * @returns
     */
    @SuccessResponse("200", "User logged in")
    @Post("login")
    @OperationId("login")
    public async login(
        @Body() credentials: CredentialsModel,
        @Request() request: any,
        // @Response() response: any
    ): Promise<UserModel> {
        const user = await this.userService.findOneByCredentials(credentials.login, credentials.password);
        const token = jwt.sign({
            id: 1,
            username: user.username,
            email: user.email,
            scopes: ["user:create", "account:profile-read"]
        }, 'secret');
        return { id: user.id, resources: ["account:profile-read"], roles: ["user.create"], token: token };
    }

    /**
     * Register user
     * @summary Register
     * @param registration Id of user
     * @returns
     */
    @SuccessResponse("200", "User registered")
    @Post("register")
    @OperationId("register")
    public async register(
        @Body() registration: RegistrationModel,
        // @Request() request: any,
        // @Response() response: any
    ): Promise<UserModel> {
        if (!registration.password || !registration.passwordAgain || (registration.password !== registration.passwordAgain)) {
            throw new Error('invalid');
        } else {
            const password = registration.password;
            const user = await this.userService.create({
                email: registration.email,
                password: password,
                username: registration.login,
            });
            const token = jwt.sign({
                id: user.id,
                username: user.username,
                email: user.email,
                scopes: [
                    "user:create",
                    "account:profile-read"
                ]
            }, user.password);
            this.setHeader('x-access-token', `${token}`);
            this.setHeader('Set-Cookie', `x-access-token=${token}`);
            return {
                id: user.id,
                resources: ["account:profile-read"],
                roles: ["user.create"],
                token: token
            };

        }
    }

    /**
     * Get user info
     * @param request fre
     * @returns sf
     */
    @Response<ErrorResponseModel>(500, "Unexpected error")
    @Response<ValidateErrorJSON>(422, "Validation failed")
    @Security("jwt", ["account:profile-read"])
    @Get("info")
    @OperationId("userInfo")
    public async userInfo(@Request() request: any): Promise<UserModel> {
        await this.userService.findOne(request.user.id);
        return Promise.resolve(request.user);
    }
}
