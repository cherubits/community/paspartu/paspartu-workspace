import { inject } from "inversify";
import { controller, BaseHttpController, httpGet } from "inversify-express-utils";
import { AuthenticationService } from "@lordoftheflies/paspartu-uaac-infrastructure/dist/authentication/AuthenticationService";
import {SERVICE_SYMBOLS as $services} from '@lordoftheflies/paspartu-uaac-infrastructure/dist/authentication/symbols';
@controller("/")
export class UserDetailsController extends BaseHttpController {

    @inject($services.AuthenticationService)
    private authService!: AuthenticationService;

    @httpGet("/")
    public async getUserDetails() {
        if (await this.httpContext.user.isAuthenticated()) {
            return this.authService.getUserDetails(this.httpContext.user.details.id);
        } else {
            throw new Error();
        }
    }
}
