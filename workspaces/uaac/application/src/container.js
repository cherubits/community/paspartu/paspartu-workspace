"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.iocContainer = void 0;
const inversify_1 = require("inversify");
const logger_1 = __importDefault(require("./logger"));
const tsoa_1 = require("tsoa");
const inversify_binding_decorators_1 = require("inversify-binding-decorators");
require("./application/controllers/UserController");
const i18n_1 = __importDefault(require("@lordoftheflies/paspartu-uaac-domain/src/i18n"));
const persistence_1 = __importDefault(require("@lordoftheflies/paspartu-uaac-infrastructure/src/persistence"));
const index_1 = require("@lordoftheflies/paspartu-uaac-infrastructure/src/authentication/index");
const iocContainer = new inversify_1.Container({
    autoBindInjectable: true,
    skipBaseClassChecks: true,
});
exports.iocContainer = iocContainer;
(async () => {
    logger_1.default.info('loading container modules', { component: 'container' });
    await iocContainer.loadAsync(new i18n_1.default(), new persistence_1.default(), new index_1.AuthenticationModule());
    (0, inversify_1.decorate)((0, inversify_1.injectable)(), tsoa_1.Controller); // Makes tsoa's Controller injectable
    iocContainer.load((0, inversify_binding_decorators_1.buildProviderModule)());
    logger_1.default.warn('initialize controllers', { component: 'container' });
    logger_1.default.info('container modules initialized', { component: 'container' });
    return iocContainer;
})();
//# sourceMappingURL=container.js.map