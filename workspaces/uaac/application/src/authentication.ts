import * as express from "express";
import * as jwt from "jsonwebtoken";
import { iocContainer } from './container';
import { AuthenticationService } from '@lordoftheflies/paspartu-uaac-infrastructure/src/authentication/AuthenticationService';
import {
    PROVIDER_SYMBOLS as $providers,
    SERVICE_SYMBOLS as $services
} from "@lordoftheflies/paspartu-uaac-infrastructure/src/authentication/symbols";
import $logger from './logger';

export async function expressAuthentication(
    request: express.Request,
    securityName: string,
    scopes?: string[]
): Promise<any> {
    const service: AuthenticationService = iocContainer.get($services.AuthenticationService);
    $logger.warn(`read security tokens`, { middleware: 'express', security: securityName, scopes: scopes });
    if (securityName === "api_key") {
        let token;
        if (request.query && request.query.access_token) {
            token = request.query.access_token;
        }

        if (token) {
            try {
                $logger.debug(`get API key ${token}`);
                return service.getUserDetails(token.toString());
            } catch (ex) {
                $logger.warn(`read API key failed`, { error: ex });
                return Promise.reject({});
            }
        } else {
            $logger.warn(`no API key provided`);
            return Promise.reject({});
        }
    } else if (securityName === "jwt") {
        const token =
            request.body.token ||
            request.query.access_token ||
            request.headers["x-access-token"];

        return new Promise((resolve, reject) => {
            if (!token) {
                $logger.warn(`No JWT token provided`);
                reject(new Error("No token provided"));
            } else {
                jwt.verify(token, "secret", function (err: any, decoded: any) {
                    if (err) {
                        $logger.warn(`Invalid JWT token provided`);
                        reject(err);
                    } else {
                        if (scopes) {
                            // Check if JWT contains all required scopes
                            for (let scope of scopes) {
                                if (!decoded.scopes.includes(scope)) {
                                    reject(new Error("JWT does not contain required scope."));
                                }
                            }
                            $logger.warn(`get JWT token '${token}'`);
                            resolve(decoded);
                        }
                    }
                });
            }
        });
    } else {
        $logger.warn(`unknown security method ${securityName}`);
        return Promise.reject({});
    }
}
