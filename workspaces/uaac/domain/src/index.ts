import { AccountEntity } from "@/entities/AccountEntity";
import { InvitationEntity } from "./entities/InvitationEntity";
import { RegistrationEntity } from "./entities/RegistrationEntity";

export default {
    AccountEntity,
    InvitationEntity,
    RegistrationEntity
}
