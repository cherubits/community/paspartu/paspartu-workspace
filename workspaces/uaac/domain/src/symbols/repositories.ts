export default {
    AccountRepository: Symbol("AccountRepository"),
    InvitationRepository: Symbol("InvitationRepository"),
    RegistrationRepository: Symbol("RegistrationRepository"),
    GroupRepository: Symbol("GroupRepository"),
    UserRepository: Symbol("UserRepository"),
    PermissionRepository: Symbol("PermissionRepository"),
};

