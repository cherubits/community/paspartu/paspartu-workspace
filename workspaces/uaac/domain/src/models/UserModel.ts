export interface UserModel {
    id?: number;
    login?: string;
    encryptedPassword?: string;
    email?: string;
    resources?: string[];
    roles?: string[];
    token?: string;
}
