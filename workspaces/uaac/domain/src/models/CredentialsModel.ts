export interface CredentialsModel {
    login: string;
    password: string;
    rememberMe: boolean
}
