import { RegistrationStatus } from "@/enums/RegistrationStatus";

export class RegistrationModel {
    lastName?: string;
    firstName?: string;
    email?: string;
    login?: string;
    password?: string;
    passwordAgain?: string;
    status?: RegistrationStatus;
}
