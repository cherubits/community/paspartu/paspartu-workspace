import { InvitationStatus } from "@/enums/InvitationStatus";

export class InvitationModel {
    email?: string;
    createdDate?: Date;
    createdBy?: string;
    status?: InvitationStatus;
}
