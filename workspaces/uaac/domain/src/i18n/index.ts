

import { injectable, Container, AsyncContainerModule, interfaces, ContainerModule } from "inversify";
import { provide, buildProviderModule } from "inversify-binding-decorators";
import $intl from './symbols';
import $logger from './logging';
import { ErrorIntl } from "./intl";

export class I18nModule extends AsyncContainerModule {

    constructor() {
        super(async (
            bind: interfaces.Bind,
            unbind: interfaces.Unbind,
            isBound: interfaces.IsBound,
            rebind: interfaces.Rebind,
            unbindAsync: interfaces.UnbindAsync,
            onActivation: interfaces.Container["onActivation"],
            onDeactivation: interfaces.Container["onDeactivation"],
        ) => {
            $logger.debug('initialize module', {module: 'persistence'});
            this.loadLocale(bind, "en");
            this.loadLocale(bind, "hu");
            $logger.debug('module initialized');
        });
    }

    private async loadLocale(bind: interfaces.Bind, locale: string) {
        try {
            bind<ErrorIntl>($intl.ErrorIntl)
                .toConstantValue((await import(`./locales/${locale}.json`)) as ErrorIntl)
                .whenTargetNamed(locale);
            $logger.debug(`loaded locale '${locale}'`)
        } catch (exception) {
            $logger.warn(`loading locale '${locale}' failed`, exception);
        }
    }
}

export default I18nModule;
