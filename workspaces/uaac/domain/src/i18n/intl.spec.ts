import { expect } from "chai";
import { Container } from "inversify";
import $intl from './symbols';
import I18nModule from ".";
import { ErrorIntl } from "./intl";

// application container is shared by all unit tests
const testContainer = new Container({
    autoBindInjectable: true,
    skipBaseClassChecks: true
});

testContainer.load(new I18nModule());

describe("Test locales", () => {

    beforeEach(() => {

        // create a snapshot so each unit test can modify
        // it without breaking other unit tests
        testContainer.snapshot();

    });

    afterEach(() => {

        // Restore to last snapshot so each unit test
        // takes a clean copy of the application container
        testContainer.restore();

    });

    // each test is executed with a snapshot of the container

    it("Test error message translation 'en'", () => {
        let en: ErrorIntl = testContainer.getNamed<ErrorIntl>($intl.ErrorIntl, "en");
        expect(en.missingEnvironmentVariableErrorMessage).to.eql("Missing environment variable");
        expect(en.missingLocaleErrorMessage).to.eql("Missing locale");
        expect(en.unknownErrorMessage).to.eql("Unknown error");

    });

    it("Test error message translation 'hu'", () => {
        let hu: ErrorIntl = testContainer.getNamed<ErrorIntl>($intl.ErrorIntl, "hu");
        expect(hu.missingEnvironmentVariableErrorMessage).to.eql("Hiányzó környezeti változó");
        expect(hu.missingLocaleErrorMessage).to.eql("Hiányzó fordítás");
        expect(hu.unknownErrorMessage).to.eql("Ismeretlen hiba");
    });

    it("Test unhappy error translation", () => {
        expect(() => testContainer.getNamed<ErrorIntl>($intl.ErrorIntl, "unknown"))
            .to.throw("No matching bindings found for serviceIdentifier: Symbol(ErrorIntl)");
    });

});
