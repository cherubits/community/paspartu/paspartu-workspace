import { provideSingleton } from "@lordoftheflies/paspartu-core/src/decorators/provideSingleton";
import $services from '../symbols/services';
import $repositories from '../symbols/repositories';

import { UserEntity } from "@/entities/UserEntity";
import { inject } from "inversify";
import { Repository } from "typeorm";
import { UserModel } from "@/models/UserModel";
import $logger from '../logger';

@provideSingleton($services.UserService)
export class UserService {
    constructor(
        @inject($repositories.UserRepository)
        private userRepository: Repository<UserEntity>
    ) { }

    public async create(user: UserModel) {
        const entity = this.userRepository.create({ ...user });
        $logger.info(`created new user with id='${entity.id}'`, { entity: user });
        return entity;
    }


    public async modify(id: number, user: UserModel) {
        const entity = await this.userRepository.findOneByOrFail({ id: id });
        const modifiedUserEntity = Object.assign(entity, user);
        modifiedUserEntity.id = id;
        const result = this.userRepository.save(modifiedUserEntity);
        $logger.info(`modified user with id='${entity.id}'`, { entity: result });
        return result;
    }

    public async erase(id: number) {
        const entity = await this.userRepository.findOneByOrFail({ id: id });
        this.userRepository.remove(entity);
        $logger.info(`created new user with id='${entity.id}'`, { entity: entity });
    }


    public async findOne(id: number) {
        return this.userRepository.findOneByOrFail({ id: id });
    }

    public async find() {
        return this.userRepository.find();
    }

    public async findOneByCredentials(username: string, password: string) {
        return this.userRepository.findOneByOrFail({
            login: username,
            encryptedPassword: password
         });
    }

}
