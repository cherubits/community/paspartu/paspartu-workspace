import { InvitationStatus } from "@/enums/InvitationStatus";
import { UserEntity } from "@/entities/UserEntity";

export class InvitationEntity {
    email?: string;
    createdDate?: Date;
    createdBy?: UserEntity;
    status?: InvitationStatus;
}
