export class UserEntity {
    id?: number;
    login?: string;
    encryptedPassword?: string;
    email?: string;
    token?: string;
    active?: boolean;
    resources?: string[];
    roles?: string[];
}
