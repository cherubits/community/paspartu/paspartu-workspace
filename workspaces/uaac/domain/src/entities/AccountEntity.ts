import { UserEntity } from "@/entities/UserEntity";

export class AccountEntity {
    createdDate?: Date;
    createdBy?: UserEntity;
}
