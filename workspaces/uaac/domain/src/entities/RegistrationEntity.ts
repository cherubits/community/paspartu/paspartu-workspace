import { RegistrationStatus } from "@/enums/RegistrationStatus";

export class RegistrationEntity {
    lastName?: string;
    firstName?: string;
    email?: string;
    password?: string;
    status?: RegistrationStatus;
}
