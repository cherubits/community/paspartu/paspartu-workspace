export enum RegistrationStatus {
    PENDING,
    CLOSED,
    ERROR,
}
