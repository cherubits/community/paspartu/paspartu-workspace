export enum InvitationStatus {
    PENDING,
    CLOSED,
    ERROR,
}
