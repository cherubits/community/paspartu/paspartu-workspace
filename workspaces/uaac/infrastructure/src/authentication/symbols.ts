export const MIDDLEWARE_SYMBOLS = {
    AuthenticationMiddleware: Symbol.for("AuthenticationMiddleware")
};

export const PROVIDER_SYMBOLS = {
    AuthenticationProvider: Symbol.for("AuthenticationProvider")
};

export const SERVICE_SYMBOLS = {
    AuthenticationService: Symbol.for("AuthenticationService")
};

export default MIDDLEWARE_SYMBOLS;
