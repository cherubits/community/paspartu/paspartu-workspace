import { interfaces } from "inversify-express-utils";
import { UserEntity } from "@lordoftheflies/paspartu-uaac-domain/src/entities/UserEntity";
import $logger from './logger';

/**
 * Principal for anonymous users
 */
export class AnonymousPrincipal implements interfaces.Principal {
    public details: UserEntity;
    public constructor() {
        this.details = {
            id: 1,
            token: 'anonymous',
            resources: [],
            roles: []
        };
    }
    public isAuthenticated(): Promise<boolean> {
        $logger.debug(`check: not authenticated`, { 'security': 'anonymous' });
        return Promise.resolve(false);
    }
    public isResourceOwner(resourceId: unknown): Promise<boolean> {
        $logger.debug(`check: not own resource '${resourceId}'`, { 'security': 'anonymous' });
        return Promise.resolve(false);
    }
    public isInRole(role: string): Promise<boolean> {
        $logger.debug(`check: not member of role '${role}'`, { 'security': 'anonymous' });
        return Promise.resolve(false);
    }
}
