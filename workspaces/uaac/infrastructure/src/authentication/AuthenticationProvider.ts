import { inject, injectable } from "inversify";
import { interfaces } from "inversify-express-utils";
import { Request, Response, NextFunction } from 'express';
import { SERVICE_IDENTIFIER } from '../../core/constants/services';
import { PROVIDER_SYMBOLS as $symbols } from "./symbols";

import { AuthenticationService } from "./AuthenticationService";
import { UserPrincipal } from "./UserPrincipal";
import { AnonymousPrincipal } from "./AnonymousPrincipal";
import $logger from './logger';

@injectable()
export class AuthenticationProvider implements interfaces.AuthProvider {

    @inject(SERVICE_IDENTIFIER.AuthenticationService)
    private authenticationService!: AuthenticationService;

    public async getUser(
        req: Request,
        res: Response,
        next: NextFunction
    ): Promise<interfaces.Principal> {
        const token = req.headers["x-auth-token"] as string; // x-access-token
        if (token) {
            const user = await this.authenticationService.getUserDetails(token);
            // res.setHeader('', '')
            $logger.debug(`provide user '${user.id}'`, { component: $symbols.AuthenticationProvider });
            return new UserPrincipal(user);
        } else {
            return new AnonymousPrincipal();
        }
    }

}


