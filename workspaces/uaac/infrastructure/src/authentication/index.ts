

import { injectable, Container, AsyncContainerModule, interfaces, ContainerModule } from "inversify";
import { provide, buildProviderModule } from "inversify-binding-decorators";
import {
    MIDDLEWARE_SYMBOLS as $middlewares,
    PROVIDER_SYMBOLS as $providers,
    SERVICE_SYMBOLS as $services,
} from './symbols';

import $logger from './logger';
import { AuthenticationMiddleware } from "./AuthenticationMiddleware";
import { AuthenticationService } from "./AuthenticationService";
import { AuthenticationProvider } from "./AuthenticationProvider";

export class AuthenticationModule extends AsyncContainerModule {

    constructor() {
        super(async (
            bind: interfaces.Bind,
            unbind: interfaces.Unbind,
            isBound: interfaces.IsBound,
            rebind: interfaces.Rebind,
            unbindAsync: interfaces.UnbindAsync,
            onActivation: interfaces.Container["onActivation"],
            onDeactivation: interfaces.Container["onDeactivation"],
        ) => {
            $logger.info('initialize module');
            try {
                bind<AuthenticationService>($services.AuthenticationService)
                    .to(AuthenticationService)
                    .inSingletonScope();
                bind<AuthenticationProvider>($providers.AuthenticationProvider)
                    .to(AuthenticationProvider)
                    .inSingletonScope();
                bind<AuthenticationMiddleware>($middlewares.AuthenticationMiddleware)
                    .to(AuthenticationMiddleware)
                    .inSingletonScope();
                $logger.info(`loaded authentication middleware`);
            } catch (exception) {
                $logger.warn(`loaded authentication middleware failed`, exception);
            }
            $logger.info('module initialized');
        });
    }
}

export default AuthenticationModule;

