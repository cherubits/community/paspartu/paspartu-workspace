import { interfaces } from "inversify-express-utils";
import { UserEntity } from "@lordoftheflies/paspartu-uaac-domain/src/entities/UserEntity";
import $logger from './logger';


export class UserPrincipal implements interfaces.Principal {
    public details: UserEntity;
    public constructor(details: UserEntity) {
        this.details = details;
    }
    public isAuthenticated(): Promise<boolean> {
        $logger.debug(`check: not authenticated`, { 'security': 'anonymous' });
        return Promise.resolve(!!this.details.token);
    }
    public isResourceOwner(resourceId: string): Promise<boolean> {
        $logger.debug(`check: not own resource '${resourceId}'`, { 'security': 'anonymous' });
        return Promise.resolve((this.details.resources) ? this.details.resources.includes(resourceId) : false);
    }
    public isInRole(role: string): Promise<boolean> {
        $logger.debug(`check: not member of role '${role}'`, { 'security': 'anonymous' });
        return Promise.resolve(this.details.roles ? this.details.roles.includes(role) : false);
    }
}
