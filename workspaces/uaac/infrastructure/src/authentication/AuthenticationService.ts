import { UserModel } from "@lordoftheflies/paspartu-uaac-domain/src/models/UserModel";
import $repositories from '@lordoftheflies/paspartu-uaac-domain/src/symbols/repositories';
import { inject, injectable } from "inversify";
import { Repository } from "typeorm";
import { UserEntity } from "@lordoftheflies/paspartu-uaac-domain/src/entities/UserEntity";

/**
 * Service layer
 */
@injectable()
export class AuthenticationService {

    constructor(
        @inject($repositories.UserRepository)
        private readonly userRepository: Repository<UserEntity>
    ) { }

    async getUserDetails(token: string): Promise<UserModel> {
        const userEntity = await this.userRepository.findOneOrFail({
            where: {
                token: token,
                active: true
            }
        });
        return {
            resources: [],
            roles: [],
            ...userEntity
        };
    }
}
