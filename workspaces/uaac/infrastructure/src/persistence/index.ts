import { AsyncContainerModule, interfaces } from "inversify";
import { applicationDataSource } from './datasource';
import $repositories from "@lordoftheflies/paspartu-uaac-domain/src/symbols/repositories";
import $logger from './logging';
import { DataSource, Repository } from "typeorm";
import { AccountEntity } from "@lordoftheflies/paspartu-uaac-domain/src/entities/AccountEntity";
import { InvitationEntity } from "@lordoftheflies/paspartu-uaac-domain/src/entities/InvitationEntity";
import { RegistrationEntity } from "@lordoftheflies/paspartu-uaac-domain/src/entities/RegistrationEntity";
import { UserEntity } from "@lordoftheflies/paspartu-uaac-domain/src/entities/UserEntity";
export class PersistenceContainerModule extends AsyncContainerModule {
    constructor() {
        super(async (
            bind: interfaces.Bind,
            unbind: interfaces.Unbind,
            isBound: interfaces.IsBound,
            rebind: interfaces.Rebind,
            unbindAsync: interfaces.UnbindAsync,
            onActivation: interfaces.Container["onActivation"],
            onDeactivation: interfaces.Container["onDeactivation"],
        ) => {


            await applicationDataSource.initialize();
            $logger.info("data-source has been initialized");

            bind<Repository<UserEntity>>($repositories.UserRepository)
                .toDynamicValue(() => applicationDataSource.getRepository(UserEntity))
                .inRequestScope();
            bind<Repository<AccountEntity>>($repositories.AccountRepository)
                .toDynamicValue(() => applicationDataSource.getRepository(AccountEntity))
                .inRequestScope();
            bind<Repository<InvitationEntity>>($repositories.InvitationRepository)
                .toDynamicValue(() => applicationDataSource.getRepository(InvitationEntity))
                .inRequestScope();

            bind<Repository<RegistrationEntity>>($repositories.RegistrationRepository)
                .toDynamicValue(() => applicationDataSource.getRepository(RegistrationEntity))
                .inRequestScope();

            // onActivation($repositories.UserRepository, (context: interfaces.Context, component: CalendarImp) => {
            //     $logger.info(`${moduleName} activate`, { contextId: context.id });
            //     component.setup();
            //     // // (context, katana) => {
            //     // let handler = {
            //     //     apply: function (target, thisArgument, argumentsList) {
            //     //         console.log(`Starting: ${new Date().getTime()}`);
            //     //         let result = target.apply(thisArgument, argumentsList);
            //     //         console.log(`Finished: ${new Date().getTime()}`);
            //     //         return result;
            //     //     }
            //     // };
            //     // katana.use = new Proxy(katana.use, handler);
            //     return component;
            // });

            // onDeactivation($repositories.UserRepository, (component: CalendarImp) => {
            //     $logger.info(`${moduleName} deactivate`);
            //     component.dispose();
            // })
        });
    }
}

export default PersistenceContainerModule;
