# Paspartu Workspace

[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lerna.js.org/)

Monorepo for Paspartu project.

## Technologies

* IoC container: [Inversify JS](https://inversify.io/)
* ORM: [TypeORM](https://typeorm.io/)

## Architecture

