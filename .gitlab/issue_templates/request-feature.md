---
name: Component feature request
about: Create frondend component
title: "[COMPONENT-REQUEST] "
labels: feature-request, component
assignees: ${assignee1}
---

## Instructions

* Create new React component.
* Create unit tests
* Create stories
* Create documentation


**Is your component request related to a problem? Please describe.**
A clear and concise description of what the component is. How should look like, what states are managed?

**Describe the solution you'd like**
A clear and concise description of what you want from the component to happen.

**Describe alternatives you've considered**
A clear and concise description of any alternative solutions or features you've considered.

**Additional context**
Add any other context or screenshots about the feature request here.

/estimate 1d
/spent 1h
/label ~feature
/assign @lordoftheflies